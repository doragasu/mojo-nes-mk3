# MOJO-NES MK3

## Introduction

![MOJO-NES MK3 cartridge](doc/img/cart-ra.png)

MOJO-NES MK3 is a cartridge system designed to cover all the needs of Nintendo NES game developers. Basically it consists of a NES cartridge, a cartridge programmer (to burn ROMs and mappers to the cartridge) and the support software and firmware. The cartridge has the following specifications:

* 8 MiB (64 megabit) of CHR ROM (for graphics).
* 8 MiB (64 megabit) of PRG ROM (for code, sound, etc.).
* 8 KiB of additional battery-backed work RAM.
* 128 macroblock CPLD to implement mappers such as MMC3.
* CIC implementation (using Avrciczz by Krikkzz).

You can see the programmer and cart in action in the following video:

<div align="center">
  <a href="https://www.youtube.com/watch?v=EjLsbCM4A3k"><img src="https://img.youtube.com/vi/EjLsbCM4A3k/0.jpg" alt="MOJO-NES MK3 demo"></a>
</div>

As happened with previous iterations of MOJO-NES, the cartridge was designed with the intention of holding NES games from the infamous [The Mojon Twins](http://www.mojontwins.com/) (hence the name). But do not be afraid about using it with any project you wish!

## What can I find in here?

This repository contains all the CAD design and source files required to build and use a MOJO-NES MK3 system. The repository layout is as follows:

* src/mk3-prog: Programmer client for PC. Run this to burn ROM files and mappers to the cartridge.
* src/mk3-prog-fw: Programmer firmware.
* src/mk3-prog-bl: Programmer bootloader, with DFU software upgrade capability. It is not required, but comes in handy to upgrade the programmer firmware.
* src/mapper: Code (VHDL) for the different supported mappers.
* sch/cart: NES cartridge design (schematics and PCB).
* sch/prog: Programmer design (schematics and PCB) that allows to burn ROMs and mapper bitfiles to the cartridge.
* sch/lib: Additional symbols and modules required for the cartridge and programmer design, not in KiCad standard libraries.
* sch/enclosure/prog: Enclosure design files for the programmer (FreeCAD and STL files). 3D printing optimized.
* doc: Additional documentation.

I have not designed a 3D printable cartridge shell, but it is easy to find some pretty good models, [like this one](https://www.thingiverse.com/thing:2006281).

# Using the cartridge

## How to start developing

If you are a NES developer, you will need a MOJO-NES MK3 cartridge, a MOJO-NES MK3 programmer, and the MOJO-NES MK3 programmer client. Grab your cartridge, insert it into the programmer, plug the programmer to your computer, and learn how to use it by reading [the readme file of the client software](src/mk3-prog/README.md). It contains instructions on how to build and use the program. The client can be used in GUI mode (with a Qt graphical interface) and with a command line interface (great for batch programming!). You can write ROMs, read them back, burn the Avrciczz firmware, program mapper bitfiles to the CPLD, etc.

The files in all the other subdirectories are only needed if you want to assemble the hardware on your own. The projects under src/mapper might also be useful as a place to start if you want to code your own mappers.

# Mapper support

Currently, the following mappers are supported:

* [0: NROM](https://wiki.nesdev.com/w/index.php/NROM)
* [3: CNROM](https://wiki.nesdev.com/w/index.php/INES_Mapper_003)
* [4: MMC3](https://wiki.nesdev.com/w/index.php/MMC3)
* [113](http://wiki.nesdev.com/w/index.php/INES_Mapper_113)
* 404: NFROM. This mapper is experimental. I have a fork of Mesen emulator supporting it [here](https://github.com/doragasu/Mesen).

As previously stated, mapper implementations are found under `src/mapper`. Directory names with `-ghdl` suffix, contain the mapper code along with the files needed to simulate the maper behavior with GHDL software. Directory names with `-lpf` suffix, contain the mapper code along with the project files to synthesize the code using Lattice Diamond suite.

The CPLD used to implement mappers, has plenty space (128 macrocells) to implement any mapper (excepting maybe the ones with additional sound hardware, but NES carts do not support sound chips anyway). Developing and burning mappers to the CPLD requires to install additional software (free as in beer) from Lattice Semiconductor. Have a look to the [programmer client readme file](src/mk3-prog/README.md) for details.

# License

All the contents in this repository are provided with NO WARRANTY. As the sources and design files of every subproject have different natures, two different licenses are used in this repository:

* Source files (`.c, .cpp, .h`) are licensed under the GPLv3 license.
* HDL files (`.vhd`) and CAD files (schematics, PCBs, enclosure design) are licensed under the CERN-OHL-S v2 license.

# Author

All the schematic files, CAD designs, source files and documentation in this repository have been created by Jesús Alonso (@doragasu).

# Contributions

Contributions are welcome. If you find a bug please open an issue. If you have implemented a cool feature/improvement, please send a pull request.

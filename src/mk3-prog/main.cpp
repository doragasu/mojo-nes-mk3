/************************************************************************//**
 * \file
 * \brief Command Line Interface for the mojo-nes-mk3 programmer.
 *
 * \defgroup main main
 * \{
 * \brief Command Line Interface for the mojo-nes-mk3 programmer.
 *
 * This utility allows to manage mojo-nes-mk3 cartridges, usina a
 * mojo-nes-mk3 programmer. The utility allows to program and read flash
 * and RAM chips. A driver system allows to support several mapper chip
 * implementations.
 *
 * \author Jesus Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#include "util.h"
#ifdef __OS_WIN
#include <windows.h>
#else
#include <sys/ioctl.h>
#include <signal.h>
#endif

#if (defined(__OS_WIN) && defined(QT_STATIC))
// Windows static builds need to import Windows Integration plugin
#include <QtPlugin>
Q_IMPORT_PLUGIN(QWindowsIntegrationPlugin)
#endif

#ifdef QT
#include <QtWidgets/QApplication>
#include <QtWidgets/QMessageBox>
#include "flashdlg.h"
#endif

#include <getopt.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include <glib.h>

#include "progbar.h"
#include "cmd.h"
#include "avrflash.h"
#include "latticeflash.h"
#include "ines.h"
#include "version.h"

/// Maximum length of a memory range.
#define MAX_MEM_RANGE	24

/// Maximum file length
#define MAX_FILELEN		255

/** \addtogroup ProgChips
 *  \brief Programmable flash chips
 *  \{ */
/// Character ROM chip (CHR ROM)
#define PROG_CHIP_CHR	0
/// Program ROM chip (PRG ROM)
#define PROG_CHIP_PRG	1
/// Last value for programmable chips
#define PROG_CHIP_MAX	PROG_CHIP_PRG
/** \} */

/// SRAM base address
//#define PROG_SRAM_BASE	0x6000
/// SRAM length
#define PROG_SRAM_LEN	(8 * 1024)

/// Default path for configuration file
#ifdef __OS_WIN
#define CFG_PATH_DEFAULT "mk3-prog.cfg"
#else
#define CFG_PATH_DEFAULT "/etc/mk3-prog.cfg"
#endif

/// Definition of the chip of the programmer (AT90USB646)
#define AVR_CHIP_MCU	"usb646"
///Definition of the CIC chip (ATTINY13)
#define AVR_CHIP_CIC	"t13"
/// avrdude binary path
#define AVR_PATH			"/usr/bin/avrdude"
/// Configuration file of the programmer to use
#define AVR_PROG_CFG		"/usr/share/mk3-prog/mk3-avr.conf"
/// MCU programmer defined in mk3-avr.conf
#define AVR_PROG_MCU		"mk3prog-mcu"
/// CIC programmer defined in mk3prog.conf
#define AVR_PROG_CIC		"mk3prog-cic"
/// Default Vendor ID for the programmer
#define VID_DEFAULT         0x03EB
/// Default Peripheral ID for the programmer
#define PID_DEFAULT         0x206C

/// Path to the FPGA programmer program/script
#define LATT_PROG_PATH		"/usr/local/diamond/3.10_x64/bin/lin64/pgrcmd"

/// Printf-like macro that prints only if f.verbose is TRUE.
#define PrintVerb(...)			\
    do{if(f.verbose) printf(__VA_ARGS__);fflush(stdout);}while(0)

/// Supported option flags
typedef union {
    uint32_t all;				///< Access to all flags
    struct {
        uint8_t fwVer:1;		///< Get firmware version
        uint8_t verify:1;		///< Verify flashed files
        uint8_t verbose:1;		///< Verbose operation
        uint8_t flashId:1;		///< Get flash chip IDs
        uint8_t chrErase:1;		///< Erase CHR flash
        uint8_t prgErase:1;		///< Erase PRG flash
        uint8_t dry:1;			///< Dry run
        uint8_t bootloader:1;   ///< Enter bootloader mode
        uint8_t autoErase:1;    ///< Automatically erase flash
    };
} Flags;

/*
 * Global variables.
 */

/// Options supported by the client
static const struct option opt[] = {
    {"qt-gui",	    no_argument,		NULL,	'Q'},
    {"firm-ver",	no_argument,		NULL,	'f'},
    {"flash-nes",   required_argument,  NULL,   'n'},
    {"flash-chr",   required_argument,  NULL,   'c'},
    {"flash-prg",   required_argument,  NULL,   'p'},
    {"read-chr",    required_argument,  NULL,   'C'},
    {"read-prg",    required_argument,  NULL,   'P'},
    {"erase-chr",   no_argument,        NULL,   'e'},
    {"erase-prg",   no_argument,        NULL,   'E'},
    {"esect-chr",   required_argument,  NULL,   's'},
    {"esect-prg",   required_argument,  NULL,   'S'},
    {"erange-chr",  required_argument,  NULL,   'g'},
    {"erange-prg",  required_argument,  NULL,   'G'},
    {"erase-auto",  no_argument,        NULL,   'A'},
    {"verify",      no_argument,        NULL,   'V'},
    {"flash-id",    no_argument,        NULL,   'i'},
    {"read-ram",	required_argument,  NULL,	'R'},
    {"write-ram",	required_argument,  NULL,	'W'},
    {"fpga-flash",	required_argument,	NULL,	'b'},
    {"cic-flash",	required_argument,	NULL,	'a'},
    {"firm-flash",  required_argument,  NULL,   'F'},
    {"mapper",      required_argument,  NULL,   'M'},
    {"bootloader",  no_argument,		NULL,   'B'},
    {"dry-run",     no_argument,		NULL,   'd'},
    {"version",     no_argument,        NULL,   'r'},
    {"verbose",     no_argument,        NULL,   'v'},
    {"help",        no_argument,        NULL,   'h'},
    {NULL,          0,                  NULL,    0 }
};

/// Descriptions of the supported options
static const char *description[] = {
    "Launch Qt GUI",
    "Get programmer firmware version",
    "Flash iNES (*.nes) ROM",
    "Flash file to CHR ROM",
    "Flash file to PRG ROM",
    "Read CHR ROM to file",
    "Read PRG ROM to file",
    "Erase CHR Flash",
    "Erase PRG Flash",
    "Erase CHR flash sector",
    "Erase PRG flash sector",
    "Erase CHR flash memory range",
    "Erase PRG flash memory range",
    "Automatically erase required flash range",
    "Verify flash after writing file",
    "Obtain flash chips identifiers",
    "Read data from RAM chip",
    "Write data to RAM chip",
    "Upload bitfile to FPGA, using .xcf file",
    "AVR CIC firmware flash",
    "Flash programmer firmware",
    "Set mapper: 0-NOROM, 4-MMC3, 404-NFROM",
    "Enter bootloader mode",
    "Dry run: don't actually do anything",
    "Show program version",
    "Show additional information",
    "Print help screen and exit"
};

/*
 * PRIVATE FUNCTIONS
 */

#ifndef __OS_WIN
/************************************************************************//**
 * Signal handler that restores cursor and aborts program.
 * 
 * \param[in] sig Received signal causing abortion.
 ****************************************************************************/
static void Terminate(int sig) {
    PrintErr("Caught signal %d, aborting...\n", sig);
    // Restore default cursor
    printf("\e[?25h");
    exit(1);
}
#endif

/************************************************************************//**
 * Print program version.
 * 
 * \param[in] prgName Program name to print along with program version.
 ****************************************************************************/
static void PrintVersion(char *prgName) {
    printf("%s version %d.%d, doragasu 2016~2018.\n", prgName,
            VERSION_MAJOR, VERSION_MINOR);
}

/************************************************************************//**
 * Print help message.
 * 
 * \param[in] prgName Program name to print along with help message.
 ****************************************************************************/
static void PrintHelp(char *prgName) {
    int i;

    PrintVersion(prgName);
    printf("Usage: %s [OPTIONS [OPTION_ARG]]\nSupported options:\n\n", prgName);
    for (i = 0; opt[i].name; i++) {
        printf(" -%c, --%s%s: %s.\n", opt[i].val, opt[i].name,
                opt[i].has_arg == required_argument?" <arg>":"",
                description[i]);
    }
    // Print additional info
    printf("For file arguments, it is possible to specify start address and "
            "file length to read/write in bytes, with the following format:\n"
            "    file_name:memory_address:file_length\n\n"
            "Examples:\n"
            "TODO\n"
            "\nNOTES:\n"
            "\t- To flash CIC and programmer firmware, avrdude must be "
            "installed, with corresponding configuration file.\n"
            "\t- Uploading bitfiles to FPGA, requires Lattice Diamond or \n"
            "\t  Programmer Standalone to be installed.\n");
}

/************************************************************************//**
 * Receives a MemImage pointer with full info in file name (e.g.
 * m->file = "rom.bin:6000:1"). Removes from m->file information other
 * than the file name, and fills the remaining structure fields if info
 * is provided (e.g. info in previous example would cause m = {"rom.bin",
 * 0x6000, 1}).
 *
 * \param[inout] m Pointer to memory image argument to parse.
 *
 * \return 0 if success, non-zero on error.
 ****************************************************************************/
static int ParseMemArgument(MemImage *m) {
    int i;
    char *addr = NULL;
    char *len  = NULL;
    char *endPtr;

    // Set address and length to default values
    m->len = m->addr = 0;

    // First argument is name. Find where it ends
    for (i = 0; i < (MAX_FILELEN + 1) && m->file[i] != '\0' &&
            m->file[i] != ':'; i++);
    // Check if end reached without finding end of string
    if (i == MAX_FILELEN + 1) return 1;
    if (m->file[i] == '\0') return 0;

    // End of token marker, search address
    m->file[i++] = '\0';
    addr = m->file + i;
    for (; i < (MAX_FILELEN + 1) && m->file[i] != '\0' && m->file[i] != ':';
            i++);
    // Check if end reached without finding end of string
    if (i == MAX_FILELEN + 1) return 1;
    // If end of token marker, search length
    if (m->file[i] == ':') {
        m->file[i++] = '\0';
        len = m->file + i;
        // Verify there's an end of string
        for (; i < (MAX_FILELEN + 1) && m->file[i] != '\0'; i++);
        if (m->file[i] != '\0') return 1;
    }
    // Convert strings to numbers and return
    if (addr && *addr) m->addr = strtol(addr, &endPtr, 0);
    if (m->addr == 0 && addr == endPtr) return 2;
    if (len  && *len)  m->len  = strtol(len, &endPtr, 0);
    if (m->len  == 0 && len  == endPtr) return 3;

    return 0;
}

/************************************************************************//**
 * Print a memory image structure.
 * 
 * \param[in] m Pointer to memory image argument to print.
 ****************************************************************************/
static void PrintMemImage(MemImage *m) {
    printf("%s", m->file);
    if (m->addr) printf(" at address 0x%06X", m->addr);
    if (m->len ) printf(" (%d bytes)", m->len);
}

/************************************************************************//**
 * Parses an input string containing a memory range, obtaining the address
 * and length. If any of these parameters is not present, they are set to
 * 0. Parameters are separated by colon character.
 *
 * \param[in]  inStr Input string containing the memory range.
 * \param[out] range Parsed memory range
 *
 * \return 0 if OK, 1 if error.
 ****************************************************************************/
int ParseMemRange(char inStr[], MemRange *range) {
	int32_t i;
	char *saddr, *endPtr;
	char scratch;
	long val;

	// Seek end of string or field separator (:)
	for (i = 0; (i < (MAX_MEM_RANGE + 1)) && (inStr[i] != '\0') &&
			(inStr[i] != ':'); i++);
	
	if (i == (MAX_MEM_RANGE + 1)) return 1;
	// Store end of string or separator, and ensure proper end of string
	scratch = inStr[i];
	inStr[i++] = '\0';
	// Convert to long
	val = strtol(inStr, &endPtr, 0);
	if (*endPtr != '\0' || val < 0) return 1;
	range->addr = val;
	// If we had field separator, repeat scan for length
	if (scratch == '\0') return 0;
	saddr = inStr + i;
	for (; (i < (MAX_MEM_RANGE + 1)) && (inStr[i] != '\0'); i++);
	if (i == (MAX_MEM_RANGE + 1)) return 1;
	val = strtol(saddr, &endPtr, 0);
	if (*endPtr != '\0' || val < 0) return 1;
	range->len = val;
	return 0;
}

/************************************************************************//**
 * Print a memory range error message corresponding to input code.
 * 
 * \param[in] code Error code to print.
 ****************************************************************************/
static void PrintMemError(int code) {
    switch (code) {
        case 0: printf("Memory range OK.\n"); break;
        case 1: PrintErr("Invalid memory range string.\n"); break;
        case 2: PrintErr("Invalid memory address.\n"); break;
        case 3: PrintErr("Invalid memory length.\n"); break;
        default: PrintErr("Unknown memory specification error.\n");
    }
}

/************************************************************************//**
 * \brief Flashes to the specified chip, the input data.
 *
 * \param[in] chip      Flash chip to program.
 * \param[in] data      Pointer to the data to flash.
 * \param[in] range     Memory range to flash
 * \param[in] autoErase When TRUE, automatically erase memory before
 *                      programming
 * \param[in] cols      Number of columns of the terminal, used to draw the
 *                      status bar.
 *
 * \return 0 on success, less than 0 on error.
 ****************************************************************************/
static int Flash(uint8_t chip, const uint8_t *data, MemRange range,
        int autoErase, unsigned int cols)
{
    uint32_t addr;
    uint32_t i;
    int toWrite;
    // Address string, e.g.: 0x123456
    char addrStr[9];

    if (chip > PROG_CHIP_MAX) return -1;

    // If requested, perform auto-erase
    if (autoErase) {
        printf("Auto-erasing %s range 0x%06X:%06X... ", chip?"PRG":"CHR",
                range.addr, range.len);
        fflush(stdout);
        if (CmdRangeErase((CmdChip)chip, range.addr, range.len)) {
            PrintErr("Auto-erase failed!\n");
            return -1;
        }
        printf("OK!\n");
    }

    printf("Flashing %s ROM, starting at 0x%06X...\n", chip?"PRG":"CHR",
            range.addr);

    // Send flash command to programmer
    for (i = 0, addr = range.addr; i < range.len;) {
        toWrite = MIN(32768, range.len - i);
        if (CmdWrite((CmdChip)chip, data + i, addr, toWrite) != CMD_OK) {

            PrintErr("Couldn't write to cart!\n");
            return -1;
        }
        // Update vars and draw progress bar
        i += toWrite;
        addr += toWrite;
        sprintf(addrStr, "0x%06X", addr);
        ProgBarDraw(i, range.len, cols, addrStr);
    }
    putchar('\n');

    return 0;
}

/************************************************************************//**
 * Allocates a RAM buffer, and reads range specified in MemImage input
 * from the specified Flash chip to the allocated buffer.
 *
 * \param[in] chip Flash chip to read.
 * \param[in] f    Memory image with the range to read.
 * \param[in] cols Number of columns of the terminal, used to draw the
 *                 status bar.
 *
 * \return Pointer to the raw data of the allocated and read image file,
 *         or NULL if error occurred.
 *
 * \warning Buffer must be externally deallocated when no longer needed,
 *          using free().
 ****************************************************************************/
uint8_t *AllocAndRead(uint8_t chip, MemImage *f, unsigned int cols) {
    uint8_t *readBuf;
    int toRead;
    uint32_t addr;
    uint32_t i;
    // Address string, e.g.: 0x123456
    char addrStr[9];

    if (chip > PROG_CHIP_MAX) return NULL;

    readBuf = (uint8_t*)malloc(f->len);
    if (!readBuf) {
        perror("Allocating read buffer");
        return NULL;
    }
    printf("Reading %s ROM starting at 0x%06X...\n", chip?"PRG":"CHR",
            f->addr);

    fflush(stdout);
    for (i = 0, addr = f->addr; i < f->len;) {
        toRead = MIN(32768, f->len - i);
        if (CmdRead((CmdChip)chip, readBuf + i, addr, toRead) != CMD_OK) {
            PrintErr("Couldn't read from cart!\n");
            free(readBuf);
            return NULL;
        }
        fflush(stdout);
        // Update vars and draw progress bar
        i += toRead;
        addr += toRead;
        sprintf(addrStr, "0x%06X", addr);
        ProgBarDraw(i, f->len, cols, addrStr);
    }
    putchar('\n');
    return readBuf;
}

/************************************************************************//**
 * Allocates a RAM buffer, reads the specified MemImage file, and writes it
 * to the in-cart RAM chip.
 *
 * \param[in] f    Memory image with the range to write and file to read.
 *
 * \return Pointer to the raw data of the allocated and read image file,
 *         or NULL if error occurred.
 *
 * \warning Buffer must be externally deallocated when no longer needed,
 *          using free().
 ****************************************************************************/
uint8_t *AllocAndRamWrite(MemImage *f) {
    FILE *ram;
    uint8_t *writeBuf;

    if (!(ram = fopen(f->file, "rb"))) {
        perror(f->file);
        return NULL;
    }

    // Obtain length if not specified
    if (!f->len) {
        fseek(ram, 0, SEEK_END);
        f->len = ftell(ram);
        fseek(ram, 0, SEEK_SET);
    }

    // Check address and length are OK
    if ((f->addr + f->len) > PROG_SRAM_LEN) {
        PrintErr("Wrong RAM write address:length combination!\n");
        fclose(ram);
        return NULL;
    }

    writeBuf = (uint8_t*)malloc(f->len);
    if (!writeBuf) {
        perror("Allocating write buffer RAM");
        fclose(ram);
        return NULL;
    }
    // Read the RAM file and close it.
    if (1 > fread(writeBuf, f->len, 1, ram)) {
        fclose(ram);
        free(writeBuf);
        PrintErr("Error reading RAM file!\n");
        return NULL;
    }
    fclose(ram);

    printf("Writing SRAM %s starting at 0x%04X... ", f->file, f->addr);

    // Send RAM write command
    if (CmdWrite(CMD_CHIP_PRG_RAM, writeBuf, f->addr, f->len) != CMD_OK) {
        free(writeBuf);
        PrintErr("Couldn't write to cart!\n");
        return NULL;
    }
    printf("OK!\n");
    return writeBuf;
}

/************************************************************************//**
 * Allocates a RAM buffer, and reads range specified in MemImage input
 * from the in-cart RAM chip.
 *
 * \param[in] f    Memory image with the range to read.
 *
 * \return Pointer to the raw data of the allocated and read image file,
 *         or NULL if error occurred.
 *
 * \warning Buffer must be externally deallocated when no longer needed,
 *          using free().
 ****************************************************************************/
uint8_t *AllocAndRamRead(MemImage *f) {
    uint8_t *readBuf;

    // Check address and length are OK
    if ((f->addr + f->len) > PROG_SRAM_LEN) {
        PrintErr("Wrong RAM read address:length combination!\n");
        return NULL;
    }

    readBuf = (uint8_t*)malloc(f->len);
    if (!readBuf) {
        perror("Allocating RAM read buffer");
        return NULL;
    }
    printf("Reading cart starting at 0x%06X... ", f->addr);
    fflush(stdout);

    // Send RAM read command	
    if (CmdRead(CMD_CHIP_PRG_RAM, readBuf, f->addr, f->len) != CMD_OK) {
        PrintErr("Couldn't read from cart!\n");
        free(readBuf);
        return NULL;
    }
    printf("OK!\n");
    return readBuf;
}

/************************************************************************//**
 * Evaluate an expression. If less than 0, print the specified error. Else
 * do nothing.
 *
 * \param[in] code  Code to evaluate.
 * \param[in] error Error message to print if code < 0.
 ****************************************************************************/
#define try(code, error)	\
    do{if ((code) < 0) {PrintErr(error);errCode = -1; \
        goto dealloc_exit;}}while(0)

/************************************************************************//**
 * Program entry point. Parses input parameters and perform requested
 * actions.
 *
 * \param[in] argc Number of input parameters.
 * \param[in] argv Array of input parameters strings to be parsed.
 *
 * \return 0 if OK, less than 0 if error.
 ****************************************************************************/
int main(int argc, char **argv){
    // Command-line flags
    Flags f;
    // iNES file to flash
    char *inesFile = NULL;
    // Number of columns of the terminal
    unsigned int cols = 80;
    // Mapper to use
    uint16_t mapper = UINT16_MAX;
    // CHR sector erase address. Set to UINT32_MAX for none
    uint32_t chrSectErase = UINT32_MAX;
    // PRG sector erase address. Set to UINT32_MAX for none
    uint32_t prgSectErase = UINT32_MAX;
    // Rom file to write to CHR ROM
    MemImage fCWr = {NULL, 0, 0};
    // Rom file to read from CHR ROM (default read length: 256 KiB)
    MemImage fCRd = {NULL, 0, 256*1024};
    // Rom file to write to PRG ROM
    MemImage fPWr = {NULL, 0, 0};
    // Rom file to read from PRG ROM (default read length: 512 KiB)
    MemImage fPRd = {NULL, 0, 512*1024};
    // Binary blob to flash to the FPGA
    MemImage fFpga = {NULL, 0, 0};
    // Binary blob to flash to the AVR CIC microcontroller
    MemImage fCic = {NULL, 0, 0};
    // Binary blob to flash to the programmer microcontroller
    MemImage fFw = {NULL, 0, 0};
    // File to read from cartridge SRAM (8 KiB default).
    MemImage fRRd = {NULL, 0, 8 * 1024};
    // File to write to cartridge SRAM.
    MemImage fRWr = {NULL, 0, 0};
    // CHR memory range to erase
    MemRange mrChr = {0, 0};
    // PRG memory range to erase
    MemRange mrPrg = {0, 0};
    // Error code for function calls
    int errCode = 0;
    // Buffer for writing data to CHR flash
    uint8_t *chrWrBuf = NULL;
    // Buffer for writing data to PRG flash
    uint8_t *prgWrBuf = NULL;
    // Buffer for reading CHR cart data
    uint8_t *chrRdBuf = NULL;
    // Buffer for reading PRG cart data
    uint8_t *prgRdBuf = NULL;
    // Buffer for RAM writes
    uint8_t *ramWrBuf = NULL;
    // Buffer for RAM reads
    uint8_t *ramRdBuf = NULL;
    // Programmer VID and PID
    uint16_t vid = VID_DEFAULT;
    uint16_t pid = PID_DEFAULT;
    // Key file for the configuration
    GKeyFile *gkf = NULL;
    // Configuration file path
    const char cfgFile[] = CFG_PATH_DEFAULT;
    // Temporal char pointer
    char *tmpChr = NULL;
    // Path for the Lattice Programmer software
    const char *latPath = LATT_PROG_PATH;
    // Path of avrdude binary
    const char *avrPath = AVR_PATH;
    // Path for the avrdude configuration
    const char *avrDConf = AVR_PROG_CFG;
    // Programmer configuration for MCU
    const char *progMcu  = AVR_PROG_MCU;
    // Programmer configuration for CIC
    const char *progCic = AVR_PROG_CIC;
    // MCU chip
    const char *chipMcu = AVR_CHIP_MCU;
    // CIC chip
    const char *chipCic = AVR_CHIP_CIC;
    // Flags AvrFlash function
    union avrFlags af;
    MemRange chrFlashRange;
    MemRange prgFlashRange;
    struct ines_read_flags inf;
    struct ines_rom_data ines;
    char *teststr;
    CmdFlashId id[2];
	// Use QT GUI flag
	uint8_t useQt = FALSE;
    // Mapper data
    struct CmdMapperData *md = NULL;
    gboolean *map_variant = NULL;
    gint *map_number = NULL;
    gchar **map_name = NULL;
    gsize num_mappers = 0;
    gsize num_map_nums = 0;
    gsize num_map_variants = 0;
    
    uint16_t fwVer = 0;
    memset(&chrFlashRange, 0, sizeof(chrFlashRange));
    memset(&prgFlashRange, 0, sizeof(prgFlashRange));
    memset(&inf,  0, sizeof(inf));
    memset(&ines, 0, sizeof(ines));

    // Just for loop iteration
    uint32_t i;

    // Set all flags to FALSE
    f.all = 0;
    // Reads console arguments
    if(argc > 1)
    {
        /// Option index, for command line options parsing
        int opIdx = 0;
        /// Character returned by getopt_long()
        int c;

        // Open configuration file
        gkf = g_key_file_new();
        if (g_key_file_load_from_file(gkf, cfgFile, G_KEY_FILE_NONE, NULL)) {
            // Read config data
            map_name = g_key_file_get_string_list(gkf, "MAPPERS", "names",
                            &num_mappers, NULL);
            map_number = g_key_file_get_integer_list(gkf, "MAPPERS",
                            "numbers", &num_map_nums, NULL);
            map_variant = g_key_file_get_boolean_list(gkf, "MAPPERS",
                    "mirror_variants", &num_map_variants, NULL);
            if (!map_name || !map_number || !map_variant || num_mappers !=
                    num_map_nums || num_mappers != num_map_variants) {
                PrintErr("Incorrect mapper configuration\n");
                goto dealloc_exit;
            }
            if ((tmpChr = g_key_file_get_string(gkf, "LATTICE_PROGRAMMER",
                            "path", NULL))) {
                latPath = tmpChr;
                LatticeFlashInit(latPath);
            } else puts("WARNING: Failed to load Lattice Programmer path "
                    "from config file.");
            if ((tmpChr = g_key_file_get_string(gkf, "AVRDUDE",
                            "path", NULL))) {
                avrPath = tmpChr;
            } else puts("WARNING: Failed to load avrdude configuration file.");
            if ((tmpChr = g_key_file_get_string(gkf, "AVRDUDE",
                            "conf", NULL))) {
                avrDConf = tmpChr;
            } else puts("WARNING: Failed to load avrdude configuration file.");
            if ((tmpChr = g_key_file_get_string(gkf, "AVRDUDE",
                            "prog_mcu", NULL))) {
                progMcu = tmpChr;
            } else
                puts("WARNING: Failed to load programmer chip configuration.");
            if ((tmpChr = g_key_file_get_string(gkf, "AVRDUDE",
                            "prog_cic", NULL))) {
                progCic = tmpChr;
            } else puts("WARNING: Failed to load avrdude CIC chip "
                    "configuration.");
            if ((tmpChr = g_key_file_get_string(gkf, "AVRDUDE",
                            "chip_mcu", NULL))) {
                chipMcu = tmpChr;
            } else puts("WARNING: Failed to load programmer chip model");
            if ((tmpChr = g_key_file_get_string(gkf, "AVRDUDE",
                            "chip_cic", NULL))) {
                chipCic = tmpChr;
            } else puts("WARNING: Failed to load CIC chip model");
            if ((vid = g_key_file_get_int64(gkf, "USB", "vid", NULL))
                    <=  0) {
                vid = VID_DEFAULT;
                puts("WARNING: Failed to load programmer VID.");
            }
            if ((pid =  g_key_file_get_int64(gkf, "USB", "pid", NULL))
                    <=  0) {
                pid = PID_DEFAULT;
                puts("WARNING: Failed to load programmer PID.");
            }
        } else printf("WARNING: could not open configuration file \"%s\"\n", cfgFile);

        while ((c = getopt_long(argc, argv, "Qfn:c:p:C:P:eEs:S:g:G:AViR:W:b:a:F:M:Bdrvh", opt, &opIdx)) != -1)
        {
            // Parse command-line options
            switch (c)
            {
                case 'Q':
                    useQt = TRUE;
                    break;

                case 'f':
                    f.fwVer = TRUE;
                    break;

                case 'n':
                    inesFile = optarg;
                    break;

                case 'c': // Write CHR flash
                    fCWr.file = optarg;
                    if ((errCode = ParseMemArgument(&fCWr))) {
                        PrintErr("Error: On CHR Flash file argument: ");
                        PrintMemError(errCode);
                        return 1;
                    }
                    break;

                case 'p': // Write PRG flash
                    fPWr.file = optarg;
                    if ((errCode = ParseMemArgument(&fPWr))) {
                        PrintErr("Error: On PRG Flash file argument: ");
                        PrintMemError(errCode);
                        return 1;
                    }
                    break;

                case 'C': // Read CHR flash
                    fCRd.file = optarg;
                    if ((errCode = ParseMemArgument(&fCRd))) {
                        PrintErr("Error: On CHR ROM read argument: ");
                        PrintMemError(errCode);
                        return 1;
                    }
                    break;

                case 'P': // Read PRG flash
                    fPRd.file = optarg;
                    if ((errCode = ParseMemArgument(&fPRd))) {
                        PrintErr("Error: On PRG ROM read argument: ");
                        PrintMemError(errCode);
                        return 1;
                    }
                    break;

                case 'e': // Erase entire CHR flash
                    f.chrErase = TRUE;
                    break;

                case 'E': // Erase entire PRG flash
                    f.prgErase = TRUE;
                    break;

                case 's': // Erase CHR flash sector
                    chrSectErase = strtol( optarg, NULL, 16 );
                    break;

                case 'S': // Erase CHR flash sector
                    prgSectErase = strtol( optarg, NULL, 16 );
                    break;

                case 'g': // Erase CHR flash range
					if (ParseMemRange(optarg, &mrChr) || (0 == mrChr.len)) {
						PrintErr("Error: Invalid Flash erase range argument: %s\n", optarg);
						return 1;
					}
                    break;

                case 'G': // Erase PRG flash range
					if (ParseMemRange(optarg, &mrPrg) || (0 == mrPrg.len)) {
						PrintErr("Error: Invalid Flash erase range argument: %s\n", optarg);
						return 1;
					}
                    break;

                case 'A': // Automatically erase flash
                    f.autoErase = TRUE;
                    break;

                case 'V': // Verify flash write
                    f.verify = TRUE;
                    break;

                case 'i': // Flash id
                    f.flashId = TRUE;
                    break;

                case 'R': // RAM read
                    fRRd.file = optarg;
                    if ((errCode = ParseMemArgument(&fRRd))) {
                        PrintErr("Error: On RAM read argument: ");
                        PrintMemError(errCode);
                        return 1;
                    }
                    break;


                case 'W': // RAM write
                    fRWr.file = optarg;
                    if ((errCode = ParseMemArgument(&fRWr))) {
                        PrintErr("Error: On RAM write argument: ");
                        PrintMemError(errCode);
                        return 1;
                    }
                    break;

                case 'b': // Flash FPGA bitfile
                    fFpga.file = optarg;
                    if ((errCode = ParseMemArgument(&fFpga))) {
                        PrintErr("Error: On FPGA bitfile argument. ");
                        PrintMemError(errCode);
                        return 1;
                    }
                    break;

                case 'a': // Flash AVR CIC firmware
                    fCic.file = optarg;
                    if ((errCode = ParseMemArgument(&fCic))) {
                        PrintErr("Error: On AVR CIC firmware argument. ");
                        PrintMemError(errCode);
                        return 1;
                    }
                    break;

                case 'F': // Flash programmer firmware
                    fFw.file = optarg;
                    if ((errCode = ParseMemArgument(&fFw))) {
                        PrintErr("Error: On programmer firmware argument. ");
                        PrintMemError(errCode);
                        return 1;
                    }
                    break;

                case 'M':
                    mapper = strtol(optarg, &teststr, 10);
                    if ((0 == mapper) && (optarg == teststr)) {
                        PrintErr("Invalid mapper %s requested!\n", optarg);
                        return 1;
                    }
                    break;

                case 'B': // Enter bootloader mode
                    f.bootloader = TRUE;
                    break;

                case 'd': // Dry run
                    f.dry = TRUE;
                    break;

                case 'r': // Version
                    PrintVersion(argv[0]);
                    return 0;

                case 'v': // Verbose
                    f.verbose = TRUE;
                    break;

                case 'h': // Help
                    PrintHelp(argv[0]);
                    return 0;

                case '?':       // Unknown switch
                    putchar('\n');
                    PrintHelp(argv[0]);
                    return 1;
            }
        }
    }
    else
    {
        printf("Nothing to do!\n");
        PrintHelp(argv[0]);
        return 0;
    }

    if (optind < argc) {
        PrintErr("Unsupported parameter:");
        for (i = optind; i < (uint32_t)argc; i++) PrintErr(" %s", argv[i]);
        PrintErr("\n\n");
        PrintHelp(argv[0]);
        return -1;
    }

    if (inesFile && (fPWr.file || fCWr.file)) {
        puts("NES file cannot be specified along with PRG/CHR ROM");
        return -1;
    }
    /// \todo A lot more sanity checks

    // Create mapper configuration structure
    md = (CmdMapperData*)calloc(num_mappers, sizeof(struct CmdMapperData));
    if (!md) {
        PrintErr("Mapper data memory allocation failed!\n");
        goto dealloc_exit;
    }
    for (i = 0; i < num_mappers; i++) {
        md[i].name = map_name[i];
        md[i].number = map_number[i];
        md[i].mirror_variant = map_variant[i];
    }

	// Try launching QT GUI if requested
	if (useQt) {
#ifdef QT
		QApplication app (argc, argv);

		// Try initialising USB device
		if (CmdInit((((uint32_t)vid)<<16) | pid, num_mappers, md)) {
			QMessageBox::critical(NULL, "MK3 PROGRAMMER ERROR",
					"Could not find MOJO-NES MK3 programmer!\n"
					"Please make sure MOJO-NES MK3 programmer is\n"
					"plugged and drivers/permissions are OK.");
			return -1;
		}
	
		FlashDialog dlg;
		dlg.show();
		return app.exec();
#else
		PrintErr("Requested Qt GUI, but MDMA has not been compiled with Qt support!\n");
		return -1;
#endif
	}

    if (f.verbose) {
        printf("Using configuration:\n");
       	printf(" - Lattice Programmer path: %s\n", latPath);
        printf(" - avrdude path: %s\n", avrPath);
        printf(" - avrdude configuration file: %s\n", avrDConf);
        printf(" - Programmer MCU: %s\n", progMcu);
        printf(" - CIC MCU: %s\n", progCic);
        printf(" - MCU programmer model: %s\n", chipMcu);
        printf(" - CIC programmer model: %s\n", chipCic);
        printf(" - USB VID:PID: %04X:%04X\n", vid, pid);
        printf("\nUsing VID:PID: %04X:%04X\n\n", vid, pid);
        printf("The following actions will%s be performed (in order):\n",
                f.dry?" NOT":"");
        printf("==================================================%s\n\n",
                f.dry?"====":"");
        if (fFpga.file) {
            printf(" - Upload FPGA bitfile ");
            PrintMemImage(&fFpga); putchar('\n');
        }
        if (fCic.file) {
            printf(" - Upload AVR CIC firmware ");
            PrintMemImage(&fFpga); putchar('\n');
        }
        if (fFw.file) {
            printf(" - Upload programmer firmware ");
            PrintMemImage(&fFw); putchar('\n');
        }
        if (mapper != UINT16_MAX) {
            printf(" - Set mapper to %d.\n", mapper);
        }
        CondPrintf(f.fwVer, " - Get programmer board firmware version.\n");
        CondPrintf(f.flashId, " - Show Flash chip identification.\n");
        if (fRWr.file) {
            printf(" - Write RAM %s", f.verify?"and verify ":"");
            PrintMemImage(&fRWr); putchar('\n');
        }
        if (fRRd.file) {
            printf(" - Read RAM to ");
            PrintMemImage(&fRRd); putchar('\n');
        }
        if (f.chrErase) printf(" - Erase CHR Flash.\n");
        else if (chrSectErase != UINT32_MAX) {
            printf(" - Erase CHR sector at 0x%X.\n", chrSectErase);
        } else if (mrChr.len) {
            printf (" - Erase CHR Flash range 0x%06X:%06X.\n", mrChr.addr,
                    mrChr.len);
        } else if (f.autoErase && (inesFile || fCWr.file)) {
            printf (" - Auto-erase CHR Flash\n");
        }
        if (f.prgErase) printf(" - Erase PRG Flash.\n");
        else if (prgSectErase != UINT32_MAX) {
            printf(" - Erase PRG sector at 0x%X.\n", prgSectErase);
        } else if (mrPrg.len) {
            printf (" - Erase PRG Flash range 0x%06X:%06X.\n", mrPrg.addr,
                    mrPrg.len);
        } else if (f.autoErase && (inesFile || fPWr.file)) {
            printf (" - Auto-erase PRG Flash\n");
        }
        if (inesFile) {
            printf(" - Flash %s%s ROM file\n", f.verify?"and verify ":"",
                    inesFile);
        }
        if (fCWr.file) {
            printf(" - Flash CHR %s", f.verify?"and verify ":"");
            PrintMemImage(&fCWr); putchar('\n');
        }
        if (fCRd.file) {
            printf(" - Read CHR ROM to ");
            PrintMemImage(&fCRd); putchar('\n');
        }
        if (fPWr.file) {
            printf(" - Flash PRG %s", f.verify?"and verify ":"");
            PrintMemImage(&fPWr); putchar('\n');
        }
        if (fPRd.file) {
            printf(" - Read PRG ROM to ");
            PrintMemImage(&fPRd); putchar('\n');
        }
        if (f.bootloader) {
            printf(" - Enter bootloader mode\n");
        }
        printf("\n");
    }

    if (f.dry) return 0;

    /*
     * COMMAND LINE PARSING END,
     * PROGRAM STARTS HERE.
     */
    // Detect number of columns (for progress bar drawing).
#ifdef __OS_WIN
    CONSOLE_SCREEN_BUFFER_INFO csbi;

    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    cols = csbi.srWindow.Right - csbi.srWindow.Left;
#else
    struct winsize max;
    ioctl(0, TIOCGWINSZ , &max);
    cols = max.ws_col;

    // Catch SIGTERM to restore cursor before exiting
    if ((signal(SIGTERM, Terminate) == SIG_ERR) ||
            (signal(SIGINT, Terminate) == SIG_ERR)){
        PrintErr("Could not catch signals.\n");
        return 1;
    }

    // Bonus: set transparent cursor
    printf("\e[?25l");
#endif

    /* First run commands related to MCU/FPGA flashing */
    // Flash FPGA bitfile
    if (fFpga.file) {
        printf("%s, %s\n", latPath, fFpga.file);
        if (LatticeFlash(fFpga.file)) {
            // WORKAROUND: At least in my machine, lattice programmer tends
            // to segfault after flashing the file, so ignore the return
            // value and assume it went OK
//            PrintErr("Programming bitfile failed!\n"
//                    "Please verify the board is connected, jumpers are OK "
//                    "and try again.\n");
//            errCode = 1;
//            goto dealloc_exit;
        }
    }
    // Flash CIC firmware blob
    if (fCic.file) {
        // File must be flashed using ADBUS interface. Prior to flashing,
        // BCBUS0 ping (FT_PSEL) must be set to '1'.
        af.flags = TRUE;
        af.hfuse = TRUE;
        af.lfuse = TRUE;
        if (AvrFlash(avrPath, avrDConf, chipCic, fCic.file, progCic, af)) {
            PrintErr("Flashing CIC failed!\n"
                    "Please verify the board is connected, jumpers are OK "
                    "and try again.\n");
            errCode = 1;
            goto dealloc_exit;
        }
    }
    // Flash programmer firmware blob
    if (fFw.file) {
        // File must be flashed using BDBUS interface. Prior to flashing,
        // user must short jumper JP1, or flashing will fail.
        af.flags = TRUE;
        af.hfuse = FALSE;
        af.lfuse = FALSE;
        if (AvrFlash(avrPath, avrDConf, chipMcu, fFw.file, progMcu, af)) {
            PrintErr("Flashing MCU failed!\n"
                    "Please verify the board is connected and JP1 is "
                    "shorted, and try again.\n");
            errCode = 1;
            goto dealloc_exit;
        }
    }

    // Initialize command layer
    printf("Initializing programmer... ");
    if (CmdInit((((uint32_t)vid)<<16) | pid, num_mappers, md)) {
        errCode = 1;
        goto dealloc_exit;
    }
    printf("OK!\n");

    if (f.fwVer) {
        try(CmdFwGet(&fwVer), "Couldn't get programmer firmware!\n");
        printf("MOJO-NES programmer version %d.%d\n", fwVer>>8, fwVer & 0xFF);
    }

    // iNES file flash
    if (inesFile) {
        inf.fill_chr_8k_blocks = 0;
        inf.fill_prg_16k_blocks = INES_FILL_PRG_KB / 16;
        inf.verbose = f.verbose;
        if (ines_load(inesFile, &inf, &ines) != INES_STAT_OK) {
            errCode = 1;
            goto dealloc_exit;
        }
        chrWrBuf = ines.chr_rom;
        chrFlashRange.addr = 0;
        chrFlashRange.len = ines.chr_rom_8k_blocks * 8 * 1024;
        prgWrBuf = ines.prg_rom;
        prgFlashRange.addr = 0;
        prgFlashRange.len = ines.prg_rom_16k_blocks * 16 * 1024;
        mapper = ines.mapper_num;
    }
    // Configure mapper
    if (mapper != UINT16_MAX) {
        if (CMD_OK != CmdMapperCfg(CmdMapperNumToType(mapper))) {
            PrintErr("Setting mapper %d failed!\n", mapper);
        } else {
            printf("Configured mapper %d\n", mapper);
        }
    }

    if (f.flashId) {
        try(CmdFIdGet(id), "Couldn't get flash ID\n");
        printf("CHR --> ManID: 0x%02X. DevID: 0x%02X:%02X:%02X\n", id[0].manId,
                id[0].devId[0], id[0].devId[1], id[0].devId[2]);
        printf("PRG --> ManID: 0x%02X. DevID: 0x%02X:%02X:%02X\n", id[1].manId,
                id[1].devId[0], id[1].devId[1], id[1].devId[2]);
    }
    // RAM write
    if (fRWr.file) {
        ramWrBuf = AllocAndRamWrite(&fRWr);
        if (!ramWrBuf) {
            errCode = 1;
            goto dealloc_exit;
        }
    }
    // RAM read/verify
    if (fRRd.file || (ramWrBuf && f.verify)) {
        // If verify is set, ignore addr and length set in command line.
        if (f.verify) {
            fRRd.addr = fRWr.addr;
            fRRd.len  = fRWr.len;
        }
        ramRdBuf = AllocAndRamRead(&fRRd);
        if (!ramRdBuf) {
            errCode = 1;
            goto dealloc_exit;
        }
        // Verify
        if (f.verify) {
            for (i = 0; i < fRWr.len; i++) {
                if (ramWrBuf[i] != ramRdBuf[i]) {
                    break;
                }
            }
            if (i == fRWr.len)
                printf("RAM Verify OK!\n");
            else {
                printf("RAM Verify failed at addr 0x%06X!\n", i + fRWr.addr);
                printf("RAM Wrote: 0x%02X; Read: 0x%02X\n", ramWrBuf[i],
                        ramRdBuf[i]);
                // Set error, but do not exit yet, because user might want
                // to write readed data to a file!
                errCode = 1;
            }
        }
        // Write output file
        if (fRRd.file) {
            FILE *dump = fopen(fRRd.file, "wb");
            if (!dump) {
                perror(fRRd.file);
                errCode = 1;
                goto dealloc_exit;
            }
            fwrite(ramRdBuf, fRRd.len, 1, dump);
            fclose(dump);
            printf("Wrote RAM file %s.\n", fRRd.file);
        }
        // Exit if we had a previous error (e.g. on verify stage).
        if (errCode) goto dealloc_exit;
    }
    if (f.chrErase) {
        printf("Erasing CHR Flash... "); fflush(stdout);
        try(CmdFlashErase(CMD_CHIP_CHR_FLASH, CMD_ERASE_FULL),
                "CHR chip erase ERROR!\n");
        printf("OK!\n");
    } else if (chrSectErase != UINT32_MAX) {
        printf("Erasing CHR sector at 0x%06X... ", chrSectErase);
        fflush(stdout);
        try(CmdFlashErase(CMD_CHIP_CHR_FLASH,
                    chrSectErase), "CHR sector erase ERROR!\n");
        printf("OK!\n");
    } else if (mrChr.len) {
        printf("Erasing CHR range 0x%06X:%06X... ", mrChr.addr, mrChr.len);
        fflush(stdout);
        try(CmdRangeErase(CMD_CHIP_CHR_FLASH, mrChr.addr, mrChr.len),
                "CHR range erase ERROR!\n");
        printf("OK!\n");
    }

    if (f.prgErase) {
        printf("Erasing PRG Flash... "); fflush(stdout);
        try(CmdFlashErase(CMD_CHIP_PRG_FLASH, CMD_ERASE_FULL),
                "PRG chip erase ERROR!\n");
        printf("OK!\n");
    } else if (prgSectErase != UINT32_MAX) {
        printf("Erasing PRG sector at 0x%06X... ", prgSectErase);
        fflush(stdout);
        try(CmdFlashErase(CMD_CHIP_PRG_FLASH,
                    prgSectErase), "PRG sector erase ERROR!\n");
        printf("OK!\n");
    } else if (mrPrg.len) {
        printf("Erasing PRG range 0x%06X:%06X... ", mrPrg.addr, mrPrg.len);
        fflush(stdout);
        try(CmdRangeErase(CMD_CHIP_PRG_FLASH, mrPrg.addr, mrPrg.len),
                "PRG range erase ERROR!\n");
        printf("OK!\n");
    }

    // CHR Flash program
    if (fCWr.file) {
        chrWrBuf = Alloc(&fCWr);
        if (!chrWrBuf) {
            errCode = 1;
            goto dealloc_exit;
        }
        chrFlashRange.addr = fCWr.addr;
        chrFlashRange.len = fCWr.len;
    }
    if (chrWrBuf) {
        if (Flash(PROG_CHIP_CHR, chrWrBuf, chrFlashRange, f.autoErase, cols)
                != 0) {
            errCode = 1;
            goto dealloc_exit;
        }
    }
    // CHR Flash read/verify
    if (fCRd.file || (chrWrBuf && f.verify)) {
        // If verify is set, ignore addr and length set in command line.
        if (f.verify) {
            fCRd.addr = chrFlashRange.addr;
            fCRd.len  = chrFlashRange.len;
        }
        chrRdBuf = AllocAndRead(PROG_CHIP_CHR, &fCRd, cols);
        if (!chrRdBuf) {
            errCode = 1;
            goto dealloc_exit;
        }
        // Verify
        if (f.verify) {
            for (i = 0; i < fCWr.len; i++) {
                if (chrWrBuf[i] != chrRdBuf[i]) {
                    break;
                }
            }
            if (i == fCWr.len)
                printf("CHR Verify OK!\n");
            else {
                printf("CHR Verify failed at addr 0x%07X!\n", i + fCWr.addr);
                printf("CHR Wrote: 0x%04X; Read: 0x%04X\n", chrWrBuf[i],
                        chrRdBuf[i]);
                // Set error, but do not exit yet, because user might want
                // to write readed data to a file!
                errCode = 1;
            }
        }
        // Write output file
        if (fCRd.file) {
            FILE *dump = fopen(fCRd.file, "wb");
            if (!dump) {
                perror(fCRd.file);
                errCode = 1;
                goto dealloc_exit;
            }
            fwrite(chrRdBuf, fCRd.len, 1, dump);
            fclose(dump);
            printf("Wrote CHR file %s.\n", fCRd.file);
        }
        // Exit if we had a previous error (e.g. on verify stage).
        if (errCode) goto dealloc_exit;
    }
    // PRG Flash program
    if (fPWr.file) {
        prgWrBuf = Alloc(&fPWr);
        if (!prgWrBuf) {
            errCode = 1;
            goto dealloc_exit;
        }
        prgFlashRange.addr = fPWr.addr;
        prgFlashRange.len = fPWr.len;
    }
    if (prgWrBuf) {
        if (Flash(PROG_CHIP_PRG, prgWrBuf, prgFlashRange, f.autoErase, cols)
                != 0) {
            errCode = 1;
            goto dealloc_exit;
        }
    }
    // PRG Flash read/verify
    if (fPRd.file || (prgWrBuf && f.verify)) {
        // If verify is set, ignore addr and length set in command line.
        if (f.verify) {
            fPRd.addr = prgFlashRange.addr;
            fPRd.len  = prgFlashRange.len;
        }
        prgRdBuf = AllocAndRead(PROG_CHIP_PRG, &fPRd, cols);
        if (!prgRdBuf) {
            errCode = 1;
            goto dealloc_exit;
        }
        // Verify
        if (f.verify) {
            for (i = 0; i < fPWr.len; i++) {
                if (prgWrBuf[i] != prgRdBuf[i]) {
                    break;
                }
            }
            if (i == fPWr.len)
                printf("PRG Verify OK!\n");
            else {
                printf("PRG Verify failed at addr 0x%07X!\n", i + fPWr.addr);
                printf("PRG Wrote: 0x%04X; Read: 0x%04X\n", prgWrBuf[i],
                        prgRdBuf[i]);
                // Set error, but do not exit yet, because user might want
                // to write readed data to a file!
                errCode = 1;
            }
        }
        // Write output file
        if (fPRd.file) {
            FILE *dump = fopen(fPRd.file, "wb");
            if (!dump) {
                perror(fPRd.file);
                errCode = 1;
                goto dealloc_exit;
            }
            fwrite(prgRdBuf, fPRd.len, 1, dump);
            fclose(dump);
            printf("Wrote PRG file %s.\n", fPRd.file);
        }
        // Exit if we had a previous error (e.g. on verify stage).
        if (errCode) goto dealloc_exit;
    }
    if (f.bootloader) {
        printf("Entering bootloader... ");
        if (CmdBootloader() < 0) {
            PrintErr("could not enter bootloader!\n");
            errCode = 1;
            goto dealloc_exit;
        }
        printf("DONE!\n");
    }

dealloc_exit:
    CmdClose();
    if (md) free(md);
    if (gkf) g_key_file_free(gkf);
    if (ramWrBuf) free(ramWrBuf);
    if (ramRdBuf) free(ramRdBuf);
    if (chrWrBuf) free(chrWrBuf);
    if (prgWrBuf) free(prgWrBuf);
    if (chrRdBuf) free(chrRdBuf);
    if (prgRdBuf) free(prgRdBuf);
#ifndef __OS_WIN
    // Restore cursor
    printf("\e[?25h");
#endif
    return errCode;
}

/** \} */


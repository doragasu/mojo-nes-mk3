/************************************************************************//**
 * \file
 * \brief Program version definition
 *
 * \defgroup version version
 * \{
 * \brief Program version definition
 *
 * \author Jesus Alonso (doragasu)
 * \date   2018
 ****************************************************************************/
#ifndef _VERSION_H_
#define _VERSION_H_

/// Major version of the program
#define VERSION_MAJOR	0x00
/// Minor version of the program
#define VERSION_MINOR	0x06

#endif /*_VERSION_H_*/


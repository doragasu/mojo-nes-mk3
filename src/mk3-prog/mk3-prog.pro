######################################################################
# mk3-prog qmake project file.
#
# doragasu, 2018
######################################################################

TEMPLATE = app
TARGET = mk3-prog
INCLUDEPATH += .

QT += widgets

DEFINES += QT_DEPRECATED_WARNINGS

# Uncomment following three lines for Windows static builds
#CONFIG+=static
#CONFIG+=no_smart_library_merge
#QTPLUGIN+=qwindows

GLIBINC = $$system(pkg-config --cflags glib-2.0)
GLIBLIB = $$system(pkg-config --libs glib-2.0)
XML2INC = $$system(xml2-config --cflags)
XML2LIB = $$system(xml2-config --libs)

# Extra headers
QMAKE_CXXFLAGS += $$GLIBINC $$XML2INC
QMAKE_CFLAGS += $$GLIBINC $$XML2INC
# Extra libraries
LIBS += $$GLIBLIB $$XML2LIB -lusb-1.0 -lutil

DEFINES += QT

# Input files
HEADERS = avrflash.h  cmd.h  flashdlg.h  flash_man.h  ines.h  latticeflash.h  progbar.h  pspawn.h  util.h version.h
SOURCES += avrflash.c  cmd.c  flashdlg.cpp  flash_man.cpp  ines.c  latticeflash.c  main.cpp  progbar.c  pspawn.c util.c

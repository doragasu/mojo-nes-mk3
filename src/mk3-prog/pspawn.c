/************************************************************************//**
 * \file 
 * \brief Spawns a child process using a pseudo terminal.
 *
 * Spawns a child process using a pseudo-terminal to avoid input/output
 * buffering. The standard output of the child process is redirected to
 * the standard output of the parent process.
 *
 * \author Jesus Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#include "util.h"
#ifdef __OS_WIN
#include <windows.h>
#else
#include <unistd.h>
#include <pty.h>
#include <sys/wait.h>
#endif
#include <stdio.h>
#include <errno.h>
#include "pspawn.h"

/// Lenght of the line buffer
#define PSPAWN_LINE_BUF_LEN		256

int pspawn(const char *file, char *const arg[])
{
#ifdef __OS_WIN
    int cmdlen;
    int i;
    char *cmd;
    FILE *f;
    char buffer[PSPAWN_LINE_BUF_LEN];
    size_t readed;
    int err;

    cmdlen = 1 + strlen(file);
    for (i = 1; arg[i] != NULL; i++) cmdlen += 1 + strlen(arg[i]);

    cmd = (char*) malloc(cmdlen);
    if (!cmd) return -1;

    strcpy(cmd, file);
    cmdlen = strlen(file);
    cmd[cmdlen++] = ' ';

    for (i = 1; arg[i] != NULL; i++) {
        strcpy(cmd + cmdlen, arg[i]);
        cmdlen += strlen(arg[i]);
        cmd[cmdlen++] = ' ';
    }
    cmd[cmdlen] = '\0';

    f = popen(cmd, "r");
    if (!f) {
        free(cmd);
        return -1;
    }
    while ((readed = fread(buffer, 1, PSPAWN_LINE_BUF_LEN, f)) > 0) {
        printf("%s", buffer);
    }
    err = pclose(f);
    free(cmd);
    return err;
#else
	char lineBuf[PSPAWN_LINE_BUF_LEN];
	int fd, stat;
	ssize_t readed;

	// Use forkpty instead of fork/exec or popen, to avoid buffering
    switch(forkpty(&fd, NULL, NULL, NULL)) {
        case -1:    // forkpty failed
            perror("pspawn: ");
            return -1;

        case 0:     // Child process spawn process
            execvp(file, arg);
			// execvp should never return unless error occurs
			perror("pspawn child: ");
            _exit(-1);

        default:    // Parent process
			// read from fd, print readed data and end when EOF
			while ((readed = read(fd, lineBuf, PSPAWN_LINE_BUF_LEN)) > 0) {
				lineBuf[readed] = '\0';
				fputs(lineBuf, stdout);
			}
			// Wait for child to exit
			wait(&stat);
			if (WIFEXITED(stat)) return WEXITSTATUS(stat);
			else return -2;
			break;
    } // switch(forkpty(...))

	return 0;
#endif /*__OS_WIN*/
}


/************************************************************************//** * \file
 *
 * \brief Flash manager dialog class implementation.
 *
 * \defgroup flashdlg flashdlg
 * \{
 * \brief Flash manager dialog class implementation.
 *
 * Uses a dialog with a QTabWidget. Each tab is implemented in a separate
 * class.
 *
 * \author doragasu
 * \date   2017
 ****************************************************************************/

#ifndef _FLASHDLG_H_
#define _FLASHDLG_H_

#include <QtWidgets/QDialog>
#include <QtWidgets/QWidget>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLayout>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QComboBox>

#include "flash_man.h"

/************************************************************************//**
 * Main dialog class
 ****************************************************************************/
class FlashDialog : public QDialog {
	Q_OBJECT

public:
	/// Tabs class handler
	QTabWidget *tabs;
	/// Status label
	QLabel *statusLab;
	/// Pointer to progress bar from parent
	QProgressBar *progBar;
	/// Pointer to exit button
	QPushButton *btnQuit;

	/********************************************************************//**
	 * Default constructor.
	 ************************************************************************/
	FlashDialog(void);

	/********************************************************************//**
	 * Initialize the combo box with available mappers.
     *
     * \param[in] cbox Combo box to fill with mapper data.
	 ************************************************************************/
    void FillMapperComboBox(QComboBox *cbox);

private:
	/********************************************************************//**
	 * Initialize the dialog with the tabs.
	 ************************************************************************/
	void InitUI(void);
};

/************************************************************************//**
 * Widget class handling the INFO tab
 ****************************************************************************/
class FlashInfoTab : public QWidget {
	Q_OBJECT
public:
	/********************************************************************//**
	 * Constructor
	 *
	 * \param[in] dlg Pointer to the dialog owning this class
	 ************************************************************************/
	FlashInfoTab(FlashDialog *dlg);

public slots:
	/********************************************************************//**
	 * This slot shall be run when the tab of the owner class changes. If
	 * selected tab is the INFO tab, updates the programmer information
	 *
	 * \param[in] index Selected tab index.
	 ************************************************************************/
	void TabChange(int index);

	/********************************************************************//**
	 * Enter DFU bootloader mode.
	 ************************************************************************/
	void DfuEnter(void);

private:
	/// Parent dialog
	FlashDialog *dlg;
	/// Programmer version label
	QLabel *progVer;
	/// Manufacturer ID label
	QLabel *chrId;
	/// Device ID label
	QLabel *prgId;

	/********************************************************************//**
	 * Initialize the tab interface
	 ************************************************************************/
	void InitUI(void);

	/********************************************************************//**
	 * Do not allow using default constructor
	 ************************************************************************/
	FlashInfoTab(void);
};

/************************************************************************//**
 * Widget class handling the ERASE tab
 ****************************************************************************/
class FlashEraseTab : public QWidget {
	Q_OBJECT
public:
	/********************************************************************//**
	 * Constructor
	 *
	 * \param[in] dlg Pointer to the dialog owning this class
	 ************************************************************************/
	FlashEraseTab(FlashDialog *dlg);

public slots:
	/********************************************************************//**
	 * Erases flash as specified in dialog data.
	 ************************************************************************/
	void Erase(void);

	/********************************************************************//**
	 * Hides the erase memory range input, depending on the check status of
	 * the fullCb checkbox.
	 *
	 * \param[in] state Checked state of the fullCb checkbox.
	 ************************************************************************/
	void ToggleFull(int state);

private:
	/// Parent dialog
	FlashDialog *dlg;
    /// Radio button for CHR ROM file
    QRadioButton *chrFile;
    /// Radio button for PRG ROM file
    QRadioButton *prgFile;
	/// Start of the erase range
	QLineEdit *startLe;
	/// Length of the erase range
	QLineEdit *lengthLe;
    /// Mapper combo box
    QComboBox *mapperCombo;
	/// Full erase checkbox
	QCheckBox *fullCb;
	/// Widget holding the frame range controls
	QWidget *rangeFrame;

	/********************************************************************//**
	 * Initialize the tab interface
	 ************************************************************************/
	void InitUI(void);

	/********************************************************************//**
	 * Do not allow using default constructor
	 ************************************************************************/
	FlashEraseTab(void);
};

/************************************************************************//**
 * Widget class handling the READ tab
 ****************************************************************************/
class FlashReadTab : public QWidget {
	Q_OBJECT
public:
	/********************************************************************//**
	 * Constructor
	 *
	 * \param[in] dlg Pointer to the dialog owning this class
	 ************************************************************************/
	FlashReadTab(FlashDialog *dlg);

public slots:
	/********************************************************************//**
	 * Reads a segment of the flash chip, depending on dialog data
	 ************************************************************************/
	void Read(void);

	/********************************************************************//**
	 * Opens a file dialog, for the user to select the file to write to.
	 ************************************************************************/
	void ShowFileDialog(void);

	/********************************************************************//**
	 * Updates dialog data when RAM radio button is toggled.
     *
     * \param[in] checked True if RAM radio button is checked.
	 ************************************************************************/
    void RamToggle(bool checked);

private:
	/// Parent dialog
	FlashDialog *dlg;
	/// File to read
	QLineEdit *fileLe;
    /// Radio button for CHR ROM file
    QRadioButton *chrFile;
    /// Radio button for PRG ROM file
    QRadioButton *prgFile;
    /// Radio button for RAM file
    QRadioButton *ramFile;
    /// Mapper combo box
    QComboBox *mapperCombo;
	/// Cartridge address to start reading
	QLineEdit *startLe;
	/// Read length
	QLineEdit *lengthLe;

	/********************************************************************//**
	 * Initialize the tab interface
	 ************************************************************************/
	void InitUI(void);

	/********************************************************************//**
	 * Do not allow using default constructor
	 ************************************************************************/
	FlashReadTab(void);
};

/************************************************************************//**
 * Widget class handling the WRITE tab
 ****************************************************************************/
class FlashWriteTab : public QWidget {
	Q_OBJECT
public:
	/********************************************************************//**
	 * Constructor
	 *
	 * \param[in] dlg Pointer to the dialog owning this class
	 ************************************************************************/
	FlashWriteTab(FlashDialog *dlg);

public slots:
	/********************************************************************//**
	 * Programs a file to the flash chip, depending on dialog input.
	 ************************************************************************/
    void Flash(void);

	/********************************************************************//**
	 * Opens the file dialog for the user to select the file to program.
	 ************************************************************************/
	void ShowFileDialog(void);

	/********************************************************************//**
	 * Updates dialog data when RAM radio button is toggled.
     *
     * \param[in] checked True if RAM radio button is checked.
	 ************************************************************************/
    void RamToggle(bool checked);

	/********************************************************************//**
	 * Updates dialog data when NES (iNES) radio button is toggled.
     *
     * \param[in] checked True if NES radio button is checked.
	 ************************************************************************/
    void NesToggle(bool checked);

	/********************************************************************//**
	 * Updates dialog data when Program Mapper checkbox is toggled.
     *
     * \param[in] checked True if Program Mapper checkbox is checked.
	 ************************************************************************/
    void MapperBurnToggle(bool checked);

	/********************************************************************//**
	 * Updates dialog data when the index in the mapper combo box changes.
     *
     * \param[in] index Index of the selected entry in the mapper combo box.
	 ************************************************************************/
    void MapperComboChange(int index);

private:
    typedef struct {
        uint8_t *data;
        uint32_t addr;
        uint32_t len;
    } MemBuf;
    int FlashWrite(uint8_t *buf, uint32_t addr, uint32_t len);
	/// Parent dialog
	FlashDialog *dlg;
    /// Radio button for NES file
    QRadioButton *nesFile;
    /// Radio button for CHR ROM file
    QRadioButton *chrFile;
    /// Radio button for PRG ROM file
    QRadioButton *prgFile;
    /// Radio button for RAM file
    QRadioButton *ramFile;
	/// File to flash
	QLineEdit *fileLe;
    /// Mapper combo box
    QComboBox *mapperCombo;
    /// Mapper program check box
    QCheckBox *mapperProgCb;
    /// Horizontal mirroring radio button
    QRadioButton *horizMirror;
    /// Vertical mirroring radio button
    QRadioButton *vertMirror;
	/// Auto-erase checkbox
	QCheckBox *autoCb;
	/// Verify checkbox
	QCheckBox *verifyCb;

	/********************************************************************//**
	 * Initialize the tab interface
	 ************************************************************************/
	void InitUI(void);

	/********************************************************************//**
	 * Do not allow using default constructor
	 ************************************************************************/
	FlashWriteTab(void);

	/********************************************************************//**
	 * Updates dialog items status depending on the selected mapper and the
     * program mapper to FPGA check box.
     *
     * \param[in] index Mapper index in the combo box.
     * \param[in] burn  True when it is requested to burn the mapper to FPGA.
	 ************************************************************************/
    void MapperMirrorEnable(int index, bool burn);
};

#endif /*_FLASHDLG_H_*/

/** \} */


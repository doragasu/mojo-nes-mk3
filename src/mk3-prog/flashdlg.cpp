/************************************************************************//**
 * \file
 *
 * \brief Flash manager dialog class implementation.
 *
 * Uses a dialog with a QTabWidget. Each tab is implemented in a separate
 * class.
 *
 * \author doragasu
 * \date   2017
 ****************************************************************************/
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QGroupBox>
#include <string.h>

#include "flashdlg.h"

#include "ines.h"
#include "version.h"
#include "util.h"

FlashInfoTab::FlashInfoTab(FlashDialog *dlg)
{
	this->dlg = dlg;
	InitUI();
}

void FlashInfoTab::InitUI(void)
{
	QLabel *mk3VerCaption = new QLabel("MK3 Programmer Version:");
	QLabel *mk3Ver = new QLabel(QString::asprintf("%d.%d", VERSION_MAJOR,
				VERSION_MINOR));
	mk3Ver->setFrameStyle(QFrame::Panel | QFrame::Sunken);
	QLabel *progVerCapion = new QLabel("Programmer firmware version:");
	progVer = new QLabel("N/A");
	progVer->setFrameStyle(QFrame::Panel | QFrame::Sunken);
	QLabel *chrIdCaption = new QLabel("CHR Flash IDs:");
	chrId = new QLabel("N/A");
	chrId->setFrameStyle(QFrame::Panel | QFrame::Sunken);
	QLabel *prgIdCaption = new QLabel("PRG Flash IDs:");
	prgId = new QLabel("N/A");
	prgId->setFrameStyle(QFrame::Panel | QFrame::Sunken);
	QLabel *about = new QLabel("MOJO-NES MK3 Programmer,\n"
			"by doragasu, 2018");
	QPushButton *bootBtn = new QPushButton("Bootloader\nmode");

	// Connect signals and slots
	connect(dlg->tabs, SIGNAL(currentChanged(int)), this,
			SLOT(TabChange(int)));
	connect(bootBtn, SIGNAL(clicked()), this, SLOT(DfuEnter()));

	// Set layout
	QHBoxLayout *aboutLayout = new QHBoxLayout;
	aboutLayout->addWidget(about);
	aboutLayout->addStretch(1);
	aboutLayout->addWidget(bootBtn);

	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(mk3VerCaption);
	mainLayout->addWidget(mk3Ver);
	mainLayout->addWidget(progVerCapion);
	mainLayout->addWidget(progVer);
	mainLayout->addWidget(chrIdCaption);
	mainLayout->addWidget(chrId);
	mainLayout->addWidget(prgIdCaption);
	mainLayout->addWidget(prgId);
	mainLayout->addLayout(aboutLayout);
	mainLayout->setAlignment(Qt::AlignTop);

	setLayout(mainLayout);
}

void FlashInfoTab::DfuEnter(void)
{
	if (QMessageBox::warning(this, "Warning", "Programmer will enter DFU "
			"bootloader mode, and then it will exit. Once in bootloader "
			"mode, use a DFU programmer utility such as Atmel Flip to update "
			"the programmer firmware.\n\nProceed?", QMessageBox::Ok |
			QMessageBox::Cancel) == QMessageBox::Ok) {
		FlashMan fm;
		fm.DfuBootloader();
		dlg->close();
	}
}

void FlashInfoTab::TabChange(int index)
{
	uint16_t err;
    CmdFlashId id[2];
    uint16_t fwVer;
    FlashMan fm;

	// If tab is the info tab, update fields
	if (index != 3) return;

    err = fm.ProgFwGet(&fwVer);    
	if (err) QMessageBox::warning(this, "Error",
            "Could not get Programmer firmware version!");
    else {
        this->progVer->setText(QString::asprintf("%d.%d", fwVer>>8,
                    fwVer & 0xFF));
    }
	err = fm.FlashIdsGet(id);
	if (err) QMessageBox::warning(this, "Error", "Could not get IDs!");
	else {
		this->chrId->setText(QString::asprintf("%02X:%02X:%02X:%02X",
                id[0].manId, id[0].devId[0], id[0].devId[1], id[0].devId[2]));
		this->prgId->setText(QString::asprintf("%02X:%02X:%02X:%02X", 
                id[1].manId, id[1].devId[0], id[1].devId[1], id[1].devId[2]));
	}
}

FlashEraseTab::FlashEraseTab(FlashDialog *dlg)
{
	this->dlg = dlg;

	InitUI();
}

void FlashEraseTab::InitUI(void)
{
	// Create widgets
    QGroupBox *fileGroup = new QGroupBox("Chip to erase:");
    chrFile = new QRadioButton("CHR ROM");
    prgFile = new QRadioButton("PRG ROM");
    chrFile->setChecked(true);
    QLabel *mapperLab = new QLabel("Mapper:");
    mapperCombo = new QComboBox();
	fullCb = new QCheckBox("Full erase");
	QLabel *rangeLb = new QLabel("Range to erase (bytes):");
	QLabel *startLb = new QLabel("Start: ");
	startLe = new QLineEdit("0x000000");
	QLabel *lengthLb = new QLabel("Length: ");
	lengthLe = new QLineEdit(QString::asprintf("0x%06X",
                FlashMan::FM_FLASH_LENGTH));
	QPushButton *eraseBtn = new QPushButton("Erase!");
	
    dlg->FillMapperComboBox(mapperCombo);

	// Connect signals and slots
	connect(eraseBtn, SIGNAL(clicked()), this, SLOT(Erase()));
	connect(fullCb, SIGNAL(stateChanged(int)), this, SLOT(ToggleFull(int)));

	// Set layout
    QVBoxLayout *radioLayout = new QVBoxLayout;
    radioLayout->addWidget(chrFile);
    radioLayout->addWidget(prgFile);
    fileGroup->setLayout(radioLayout);

    QHBoxLayout *mapperLayout = new QHBoxLayout;
    mapperLayout->addWidget(mapperLab);
    mapperLayout->addWidget(mapperCombo);

	QHBoxLayout *rangeLayout = new QHBoxLayout;
	rangeLayout->addWidget(startLb);
	rangeLayout->addWidget(startLe);
	rangeLayout->addWidget(lengthLb);
	rangeLayout->addWidget(lengthLe);

	QVBoxLayout *rangeVLayout = new QVBoxLayout;
	rangeVLayout->addWidget(rangeLb);
	rangeVLayout->addLayout(rangeLayout);
	rangeFrame = new QWidget;
	rangeFrame->setLayout(rangeVLayout);

	QHBoxLayout *statLayout = new QHBoxLayout;
	statLayout->addWidget(eraseBtn);
	statLayout->setAlignment(Qt::AlignRight);

	QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(fileGroup);
    mainLayout->addLayout(mapperLayout);
	mainLayout->addWidget(fullCb);
	mainLayout->addWidget(rangeFrame);
	mainLayout->addStretch(1);
	mainLayout->addLayout(statLayout);

	setLayout(mainLayout);
}

void FlashEraseTab::ToggleFull(int state)
{
	if (Qt::Checked == state) rangeFrame->hide();
	else rangeFrame->show();
}

void FlashEraseTab::Erase(void)
{
	int start, len;
	int status;
	bool ok;
	FlashMan fm;
    CmdChip chip;

    if (chrFile->isChecked()) {
        chip = CMD_CHIP_CHR_FLASH;
    } else {
        chip = CMD_CHIP_PRG_FLASH;
    }

    // Set mapper
    if (CMD_OK != fm.MapperSet(mapperCombo->currentIndex())) {
		QMessageBox::critical(this, "Mapper failed",
				"Could not set mapper configuration!");
        return;
    }

	dlg->tabs->setEnabled(false);
	dlg->btnQuit->setVisible(false);

	if (fullCb->isChecked()) {
		dlg->statusLab->setText("Erasing...");
		dlg->repaint();
		status = fm.FullErase(chip);
	} else {
		start = startLe->text().toInt(&ok, 0);
		if (ok) len = lengthLe->text().toInt(&ok, 0);
		if (!ok || ((start + len) > FlashMan::FM_FLASH_LENGTH)) {
			QMessageBox::warning(this, "MK3 Programmer",
                    "Invalid erase range!");
			return;
		}
		dlg->statusLab->setText("Erasing...");
		dlg->repaint();
		// Partial erase, with word based range
		status = fm.RangeErase(chip, start, len);
	}
	if (status) QMessageBox::warning(this, "Error", "Erase failed!");

	dlg->tabs->setEnabled(true);
	dlg->btnQuit->setVisible(true);
	dlg->statusLab->setText("Done!");
}


FlashReadTab::FlashReadTab(FlashDialog *dlg)
{
	this->dlg = dlg;
	InitUI();
}

void FlashReadTab::RamToggle(bool checked)
{
    if (checked) {
        lengthLe->setText(QString::asprintf("0x%06X",
                    FlashMan::FM_RAM_LENGTH));
    } else {
        lengthLe->setText(QString::asprintf("0x%06X",
                    FlashMan::FM_FLASH_LENGTH));
    }
}

void FlashReadTab::InitUI(void)
{
	// Create widgets
	QLabel *romLab = new QLabel("Write data to file:");	
	fileLe = new QLineEdit;
	QPushButton *fOpenBtn = new QPushButton("...");
	fOpenBtn->setFixedWidth(30);
    QGroupBox *fileGroup = new QGroupBox("ROM to read:");
    chrFile = new QRadioButton("CHR ROM");
    prgFile = new QRadioButton("PRG ROM");
    ramFile = new QRadioButton("RAM");
    chrFile->setChecked(true);
    QLabel *mapperLab = new QLabel("Mapper:");
    mapperCombo = new QComboBox();
	QLabel *rangeLb = new QLabel("Range to read (bytes):");
	QLabel *startLb = new QLabel("Start: ");
	startLe = new QLineEdit("0x000000");
	QLabel *lengthLb = new QLabel("Length: ");
	lengthLe = new QLineEdit(QString::asprintf("0x%06X",
                FlashMan::FM_FLASH_LENGTH));
	QPushButton *readBtn = new QPushButton("Read!");

    dlg->FillMapperComboBox(mapperCombo);

	// Connect signals to slots
	connect(fOpenBtn, SIGNAL(clicked()), this, SLOT(ShowFileDialog()));
	connect(readBtn, SIGNAL(clicked()), this, SLOT(Read()));
    connect(ramFile, SIGNAL(toggled(bool)), this, SLOT(RamToggle(bool)));

	// Configure layout
    QVBoxLayout *radioLayout = new QVBoxLayout;
    radioLayout->addWidget(chrFile);
    radioLayout->addWidget(prgFile);
    radioLayout->addWidget(ramFile);
    fileGroup->setLayout(radioLayout);

	QHBoxLayout *fileLayout = new QHBoxLayout;
	fileLayout->addWidget(fileLe);
	fileLayout->addWidget(fOpenBtn);

    QHBoxLayout *mapperLayout = new QHBoxLayout;
    mapperLayout->addWidget(mapperLab);
    mapperLayout->addWidget(mapperCombo);

	QHBoxLayout *rangeLayout = new QHBoxLayout;
	rangeLayout->addWidget(startLb);
	rangeLayout->addWidget(startLe);
	rangeLayout->addWidget(lengthLb);
	rangeLayout->addWidget(lengthLe);

	QHBoxLayout *statLayout = new QHBoxLayout;
	statLayout->addWidget(readBtn);
	statLayout->setAlignment(Qt::AlignRight);

	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(romLab);
	mainLayout->addLayout(fileLayout);
    mainLayout->addWidget(fileGroup);
    mainLayout->addLayout(mapperLayout);
	mainLayout->addWidget(rangeLb);
	mainLayout->addLayout(rangeLayout);
	mainLayout->addStretch(1);
	mainLayout->addLayout(statLayout);

	setLayout(mainLayout);
}

void FlashReadTab::ShowFileDialog(void)
{
	QString fileName;

	fileName = QFileDialog::getSaveFileName(this, tr("Write to ROM file"),
			NULL, tr("ROM Files (*.bin);;All files (*)"));
	if (!fileName.isEmpty()) fileLe->setText(fileName);
}

void FlashReadTab::Read(void)
{
	uint8_t *rdBuf = NULL;
	int start, len;
	bool ok;
    CmdChip chip;

	if (fileLe->text().isEmpty()) {
		QMessageBox::warning(this, "MK3 Programmer", "No file selected!");
		return;
	}
	start = startLe->text().toInt(&ok, 0);
	if (ok) len = lengthLe->text().toInt(&ok, 0);
	if (!ok || ((start + len) > FlashMan::FM_FLASH_LENGTH)) {
		QMessageBox::warning(this, "MK3 Programmer", "Invalid read range!");
		return;
	}

    // Configure chip to read
    if (chrFile->isChecked()) {
        chip = CMD_CHIP_CHR_FLASH;
    } else if (prgFile->isChecked()) {
        chip = CMD_CHIP_PRG_FLASH;
    } else {
        chip = CMD_CHIP_PRG_RAM;
    }

	dlg->tabs->setDisabled(true);
	dlg->btnQuit->setVisible(false);
	dlg->progBar->setVisible(true);
	// Create Flash Manager and connect signals to UI control slots
	FlashMan fm;
	connect(&fm, &FlashMan::RangeChanged, dlg->progBar,
			&QProgressBar::setRange);
	connect(&fm, &FlashMan::ValueChanged, dlg->progBar,
			&QProgressBar::setValue);
	connect(&fm, &FlashMan::StatusChanged, dlg->statusLab, &QLabel::setText);

    // Set mapper
    if (CMD_OK != fm.MapperSet(mapperCombo->currentIndex())) {
		QMessageBox::critical(this, "Mapper failed",
				"Could not set mapper configuration!");
        goto out;
    }
	// Start reading
	rdBuf = fm.Read(chip, start, len);
	if (!rdBuf) {
		QMessageBox::warning(this, "Read failed",
				"Cannot allocate buffer!");
	}
	// Cart readed, write to file
	if (rdBuf) {
		FILE *f;
		if (!(f = fopen(fileLe->text().toStdString().c_str(), "wb")))
			QMessageBox::warning(this, "ERROR", "Could not open file!");
		else {
            // Write file
	        if (fwrite(rdBuf, len, 1, f) <= 0)
				QMessageBox::warning(this, "ERROR", "Could not write to file!");
	        fclose(f);
		}
	}
out:
	// Cleanup
	if (rdBuf) fm.BufFree(rdBuf);
	dlg->progBar->setVisible(false);
	dlg->btnQuit->setVisible(true);
	dlg->tabs->setDisabled(false);
	dlg->statusLab->setText("Done!");
	disconnect(this, 0, 0, 0);
}

FlashWriteTab::FlashWriteTab(FlashDialog *dlg)
{
	this->dlg = dlg;
	InitUI();
}

void FlashWriteTab::RamToggle(bool checked)
{
    autoCb->setEnabled(!checked);
    autoCb->setChecked(!checked);
}

void FlashWriteTab::NesToggle(bool checked)
{
    mapperCombo->setDisabled(checked);
}

void FlashWriteTab::MapperBurnToggle(bool checked)
{
    MapperMirrorEnable(mapperCombo->currentIndex(), checked);
}

void FlashWriteTab::MapperComboChange(int index)
{
    MapperMirrorEnable(index, mapperProgCb->isChecked());
}

void FlashWriteTab::InitUI(void)
{
	// Create widgets
	QLabel *romLab = new QLabel("File to write:");	
	fileLe = new QLineEdit;
	QPushButton *fOpenBtn = new QPushButton("...");
	fOpenBtn->setFixedWidth(30);
    QGroupBox *fileGroup = new QGroupBox("File type:");
    nesFile = new QRadioButton("NES (iNES) ROM");
    chrFile = new QRadioButton("CHR ROM");
    prgFile = new QRadioButton("PRG ROM");
    ramFile = new QRadioButton("RAM");
    nesFile->setChecked(true);
    QGroupBox *mapperGroup = new QGroupBox("Mapper:");
    QLabel *mapperLab = new QLabel("Type:");
    mapperProgCb = new QCheckBox("Program to FPGA");
    mapperCombo = new QComboBox();
    mapperCombo->setDisabled(true);
    horizMirror = new QRadioButton("Horizontal mirroring");
    vertMirror  = new QRadioButton("Vertical mirroring");
    horizMirror->setChecked(true);
    horizMirror->setDisabled(true);
    vertMirror->setDisabled(true);
	autoCb = new QCheckBox("Auto-erase");
	autoCb->setCheckState(Qt::Checked);
	verifyCb = new QCheckBox("Verify");
	verifyCb->setCheckState(Qt::Unchecked);
	QPushButton *flashBtn = new QPushButton("Write!");

    dlg->FillMapperComboBox(mapperCombo);

	// Connect signals to slots
	connect(fOpenBtn, SIGNAL(clicked()), this, SLOT(ShowFileDialog()));
	connect(flashBtn, SIGNAL(clicked()), this, SLOT(Flash()));
    connect(ramFile, SIGNAL(toggled(bool)), this, SLOT(RamToggle(bool)));
    connect(nesFile, SIGNAL(toggled(bool)), this, SLOT(NesToggle(bool)));
    connect(mapperProgCb, SIGNAL(toggled(bool)), this,
            SLOT(MapperBurnToggle(bool)));
    connect(mapperCombo, SIGNAL(currentIndexChanged(int)), this,
            SLOT(MapperComboChange(int)));

	// Configure layout
    QVBoxLayout *radioLayout = new QVBoxLayout;
    radioLayout->addWidget(nesFile);
    radioLayout->addWidget(chrFile);
    radioLayout->addWidget(prgFile);
    radioLayout->addWidget(ramFile);
    fileGroup->setLayout(radioLayout);

	QHBoxLayout *fileLayout = new QHBoxLayout;
	fileLayout->addWidget(fileLe);
	fileLayout->addWidget(fOpenBtn);

	QHBoxLayout *statLayout = new QHBoxLayout;
	statLayout->addWidget(flashBtn);
	statLayout->setAlignment(Qt::AlignRight);

    QHBoxLayout *mapSelLayout = new QHBoxLayout;
    mapSelLayout->addWidget(mapperLab);
    mapSelLayout->addWidget(mapperCombo);
    mapSelLayout->addWidget(mapperProgCb);
    QVBoxLayout *mapperLayout = new QVBoxLayout;
    mapperLayout->addLayout(mapSelLayout);
    mapperLayout->addWidget(horizMirror);
    mapperLayout->addWidget(vertMirror);
    mapperGroup->setLayout(mapperLayout);

	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(romLab);
	mainLayout->addLayout(fileLayout);
    mainLayout->addWidget(fileGroup);
    mainLayout->addWidget(mapperGroup);
	mainLayout->addWidget(autoCb);
	mainLayout->addWidget(verifyCb);
	mainLayout->addStretch(1);
	mainLayout->addLayout(statLayout);

	setLayout(mainLayout);
}

void FlashWriteTab::ShowFileDialog(void)
{
	QString fileName;
    const char *fileFilter;

    if (nesFile->isChecked()) {
        fileFilter = "NES files (*.nes);;All files (*)";
    } else {
        fileFilter = "Binary files (*.bin);;All files (*)";
    }

   // Change file type depending on selected file type
	fileName = QFileDialog::getOpenFileName(this, tr("Open file"),
			NULL, tr(fileFilter));
	if (!fileName.isEmpty()) {
        fileLe->setText(fileName);
    }
}

void FlashWriteTab::Flash(void) {
    MemBuf chr = {NULL, 0, 0};
    MemBuf prg = {NULL, 0, 0};
    MemBuf ram = {NULL, 0, 0};
    uint8_t *tmp;
	uint8_t *rdBuf = NULL;
    struct ines_read_flags flags;
    struct ines_rom_data data;
    MemImage mem = {NULL, 0, 0};
    uint32_t offset;
	bool autoErase;
    int mapper;
    int vMirror = false;

	if (fileLe->text().isEmpty()) return;
    // Check operations to perform and read data from file
    if (nesFile->isChecked()) {
        memset(&data, 0, sizeof(data));
        flags.fill_chr_8k_blocks = 0;
        flags.fill_prg_16k_blocks = INES_FILL_PRG_KB / 16;
        flags.verbose = FALSE;
        if (ines_load(fileLe->text().toStdString().c_str(), &flags, &data) !=
                INES_STAT_OK) {
            QMessageBox::critical(this, "Flash error",
                    "Could not read NES file!");
            return;
        }
        chr.data = data.chr_rom;
        chr.addr = 0;
        chr.len = data.chr_rom_8k_blocks * 8 * 1024;
        prg.data = data.prg_rom;
        prg.addr = 0;
        prg.len = data.prg_rom_16k_blocks * 16 * 1024;
        mapper = CmdMapperNumToType(data.mapper_num);
        vMirror = data.header.mirror_vert;
    } else {
        mapper = mapperCombo->currentIndex();
        vMirror = vertMirror->isChecked();
        mem.file = strdup(fileLe->text().toStdString().c_str());
        tmp = Alloc(&mem);
        free(mem.file);
        mem.file = NULL;
        if (!tmp) {
            QMessageBox::critical(this, "Flash error",
                    "Could not read ROM file!");
            return;
        }
        if (chrFile->isChecked()) {
            chr.data = tmp;
            chr.len = mem.len;
        } else if (prgFile->isChecked()) {
            prg.data = tmp;
            prg.len = mem.len;
        } else {
            ram.data = tmp;
            ram.len = mem.len;
        }
    }
#ifndef __OS_WIN
    // ftdi_sio interferes with programmer, so check if it is loaded
    if (mapperProgCb->isChecked() && ModuleLoaded("ftdi_sio")) {
        if (QMessageBox::warning(this, "ftdi_sio loaded", "ftdi_sio module has "
                "been detected. This module interferes with the FPGA "
                "programmer. You should manually unload it before continuing, "
                "otherwise mapper programming might silently fail.\n\n"
                "Continue?", QMessageBox::Ok | QMessageBox::Cancel) ==
                QMessageBox::Cancel) {
            return;
        }
    }
#endif //__OS_WIN
    if (mapper >= CmdMapperNumMappers())  {
        if (QMessageBox::warning(this, "Unsupported mapper", "Mapper is not "
                "supported. Continuing may cause errors. Continue?",
                QMessageBox::Ok | QMessageBox::Cancel) ==
                QMessageBox::Cancel) {
            return;
        }
    }

	// Create Flash Manager and connect signals to UI control slots
	FlashMan fm;
	dlg->tabs->setDisabled(true);
	dlg->btnQuit->setVisible(false);
	dlg->progBar->setVisible(true);
	connect(&fm, &FlashMan::RangeChanged, dlg->progBar,
			&QProgressBar::setRange);
	connect(&fm, &FlashMan::ValueChanged, dlg->progBar,
			&QProgressBar::setValue);
	connect(&fm, &FlashMan::StatusChanged, dlg->statusLab, &QLabel::setText);

	autoErase = autoCb->isChecked();

    // Burn mapper to FPGA
    if (mapperProgCb->isChecked() && (mapper < CmdMapperNumMappers())) {
        if (fm.MapperProgram(mapper, vMirror)) {
    		QMessageBox::critical(this, "Burn mapper failed",
    				"Could not configure mapper in the FPGA!");
            goto out;
        }
    }
    // Set mapper
    if (CMD_OK != fm.MapperSet(mapper)) {
		QMessageBox::critical(this, "Mapper failed",
				"Could not set mapper configuration!");
        goto out;
    }

    // Flash
    if (chr.data) {
	    if (fm.Write(CMD_CHIP_CHR_FLASH, chr.data, autoErase,
                    chr.addr, chr.len)) {
	    	QMessageBox::warning(this, "Program failed",
	    		"CHR ROM programming failed!");
	    	goto out;
        }
        if (verifyCb->isChecked()) {
            rdBuf = fm.Read(CMD_CHIP_CHR_FLASH, chr.addr, chr.len);
            if (!rdBuf) {
    	    	QMessageBox::warning(this, "Program failed",
    	    		"CHR ROM read failed!");
    	    	goto out;
            }
            offset = MemCompare(chr.data, rdBuf, chr.len);
            if (offset != chr.len) {
				QString str;
				str.sprintf("CHR verify failed at pos: 0x%X!", offset);
				QMessageBox::warning(this, "Verify failed", str);
                goto out;
            }
            free(rdBuf);
        }
    }

    if (prg.data) {
	    if (fm.Write(CMD_CHIP_PRG_FLASH, prg.data, autoErase,
                    prg.addr, prg.len)) {
	    	QMessageBox::warning(this, "Program failed",
	    		"PRG ROM programming failed!");
	    	goto out;
        }
        if (verifyCb->isChecked()) {
            rdBuf = fm.Read(CMD_CHIP_PRG_FLASH, prg.addr, prg.len);
            if (!rdBuf) {
    	    	QMessageBox::warning(this, "Program failed",
    	    		"PRG ROM read failed!");
    	    	goto out;
            }
            offset = MemCompare(prg.data, rdBuf, prg.len);
            if (offset != prg.len) {
				QString str;
				str.sprintf("PRG verify failed at pos: 0x%X!", offset);
				QMessageBox::warning(this, "Verify failed", str);
                goto out;
            }
            free(rdBuf);
        }
    }

    if (ram.data) {
	    if (fm.Write(CMD_CHIP_PRG_RAM, ram.data, autoErase,
                    ram.addr, ram.len)) {
	    	QMessageBox::warning(this, "Program failed",
	    		"RAM write failed!");
	    	goto out;
        }
        if (verifyCb->isChecked()) {
            rdBuf = fm.Read(CMD_CHIP_PRG_RAM, ram.addr, ram.len);
            if (!rdBuf) {
    	    	QMessageBox::warning(this, "Program failed",
    	    		"RAM read failed!");
    	    	goto out;
            }
            offset = MemCompare(ram.data, rdBuf, ram.len);
            if (offset != ram.len) {
				QString str;
				str.sprintf("RAM verify failed at pos: 0x%X!", offset);
				QMessageBox::warning(this, "Verify failed", str);
                goto out;
            }
            free(rdBuf);
        }
    }

out: 
    if (chr.data) free(chr.data);
    if (prg.data) free(prg.data);
	if (ram.data) free(ram.data);
	dlg->progBar->setVisible(false);
	dlg->btnQuit->setVisible(true);
	dlg->tabs->setDisabled(false);
   	dlg->statusLab->setText("Done!");
	disconnect(this, 0, 0, 0);
}

void FlashDialog::FillMapperComboBox(QComboBox *cbox)
{
    int i;

    for (i = 0; i < CmdMapperNumMappers(); i++) {
        cbox->insertItem(i, CmdMapperName(i));
    }
    // Default to MMC3
    cbox->setCurrentIndex(CmdMapperNumToType(4));
}

FlashDialog::FlashDialog(void)
{
	InitUI();
}

void FlashDialog::InitUI(void)
{
	tabs = new QTabWidget;
	tabs->addTab(new FlashWriteTab(this), tr("WRITE"));
	tabs->addTab(new FlashReadTab(this),  tr("READ"));
	tabs->addTab(new FlashEraseTab(this), tr("ERASE"));
	tabs->addTab(new FlashInfoTab(this),  tr("INFO"));

	statusLab = new QLabel("Ready!");
	statusLab->setFixedWidth(80);
	progBar = new QProgressBar;
	progBar->setVisible(false);
	btnQuit = new QPushButton("Exit");
	btnQuit->setDefault(true);

	connect(btnQuit, SIGNAL(clicked()), this, SLOT(close()));

	QHBoxLayout *statLayout = new QHBoxLayout;
	statLayout->addWidget(statusLab);
	statLayout->addWidget(progBar);
	statLayout->addWidget(btnQuit);

	QVBoxLayout *mainLayout = new QVBoxLayout;
	mainLayout->addWidget(tabs);
	mainLayout->addLayout(statLayout);

	setLayout(mainLayout);

	resize(350, 300);
	setWindowTitle("MOJO-NES MK3 Programmer");
}

void FlashWriteTab::MapperMirrorEnable(int index, bool burn)
{
    // Mapper variant can only be selected when not burning an iNES file
    // and the mapper has mirroring variant
    if (burn && !nesFile->isChecked() &&
            CmdMapperHasMirroringVariants(index)) {
        horizMirror->setEnabled(true);
        vertMirror->setEnabled(true);
    } else {
        horizMirror->setEnabled(false);
        vertMirror->setEnabled(false);
    }
}


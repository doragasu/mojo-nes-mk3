/************************************************************************//**
 * \file
 *
 * \brief Flash Manager.
 *
 * \defgroup flash_man flash_man
 * \{
 * \brief Flash Manager
 *
 * Handles basic operations on flash chips (Program/Read/Erase) using MDMA
 * interface.
 *
 * \author doragasu
 * \date   2017
 ****************************************************************************/

#ifndef _FLASH_MAN_H_
#define _FLASH_MAN_H_

#include <QObject>
#include <stdint.h>
#include "cmd.h"

/************************************************************************//**
 * Flash Manager class.
 ****************************************************************************/
class FlashMan : public QObject {
	Q_OBJECT

public:
    /// Chip length in bytes
    static constexpr int FM_FLASH_LENGTH = 8 * 1024 * 1024;
    /// RAM length in bytes
    static constexpr int FM_RAM_LENGTH = 8 * 1024;

	/********************************************************************//**
	 * Write a file to a chip.
	 *
	 * \param[in] chip      Chip to write to.
	 * \param[in] buf       Buffer to write.
	 * \param[in] autoErase Erase the flash range where the file will be
	 *            flashed. Only supported by Flash chips.
	 * \param[in] start     Word memory addressh where the file will be
	 *            programmed.
	 * \param[in] len       Number of words to write to the flash.
	 *
	 * \return A pointer to a buffer containing the file to flash (byte
	 * swapped) or NULL if the operation fails.
	 *
	 * \warning The user is responsible of freeing the buffer calling
	 * BufFree() when its contents are not needed anymore.
	 ************************************************************************/
	int Write(CmdChip chip, uint8_t *buf, bool autoErase,
			uint32_t start, uint32_t len);

	/********************************************************************//**
	 * Read a memory range from the flash chip.
	 *
	 * \param[in] chip  Chip to read from.
	 * \param[in] start Word memory address to start reading from.
	 * \param[in] len   Number of words to read from flash.
	 *
	 * \return A pointer to the buffer containing the data read from the
	 * flash, or NULL if the read operation has failed.
	 *
	 * \warning The user is responsible of freeing the buffer calling
	 * BufFree() when its contents are not needed anymore.
	 ************************************************************************/
	uint8_t *Read(CmdChip chip, uint32_t start, uint32_t len);

	/********************************************************************//**
	 * Erases a memory range from the specified flash chip.
	 *
	 * \param[in] chip  Chip to erase range from (only Flash chips supported).
	 * \param[in] start Word memory address of the beginning of the range
	 *            to erase.
	 * \param[in] len   Length (in words) of the range to erase.
	 *
	 * \return 0 on success, non-zero if erase operation fails.
	 ************************************************************************/
	int RangeErase(CmdChip chip, uint32_t start, uint32_t len);

	/********************************************************************//**
	 * Issues a complete chip erase command to the flash chip.
	 *
	 * \param[in] chip  Chip to erase (only Flash chips supported).
     *
	 * \return 0 on success, non-zero if erase operation fails.
	 ************************************************************************/
	int FullErase(CmdChip chip);

	/********************************************************************//**
	 * Frees a buffer previously allocated by Program() or Read().
	 *
	 * \param[in] buf The address of the buffer to free.
	 ************************************************************************/
	void BufFree(uint8_t *buf);

	/********************************************************************//**
	 * Obtains the manufacturer and device identifiers fo the flash chips.
	 *
	 * \param[out] id The identifiers of the chips: 4 for the CHR flash and
     *             4 for the PRG flash. Order is manID:devID1:devID2:devID3.
	 *
	 * \return 0 on success, non-zero on error.
	 ************************************************************************/
	uint8_t FlashIdsGet(CmdFlashId id[2]);

	/********************************************************************//**
	 * Enters DFU bootloader mode.
	 *
	 * \return 0 on success, non-zero on error.
	 ************************************************************************/
	int DfuBootloader(void);

    /********************************************************************//**
     * Obtain programmer firmware version:
     *
     * \param[out] fwVer Firmware version: major version on the upper byte and
     *             minor version on the lower byte.
     *
     * \return 0 on success, less than 0 on error.
     ************************************************************************/
    int ProgFwGet(uint16_t *fwVer);

    /********************************************************************//**
     * Set the mapper used by the programmer.
     *
     * \param[in] type Mapper type to use.
     *
     * \return 0 on success, less than 0 on error.
     ************************************************************************/
    int MapperSet(int type);

    /********************************************************************//**
     * Write mapper with specified variant (if applicable) to the FPGA.
     *
     * \param[in] type    Mapper type to use.
     * \param[in] vMirror Vertical mirroring variant when true
     *
     * \return 0 on success, less than 0 on error.
     ************************************************************************/
    int MapperProgram(int type, int vMirror);

signals:
	/********************************************************************//**
	 * RangeChanged signal. It is emitted by the Flash() and Read() methods
	 * when the length of the range ro flash/read is determinde.
	 *
	 * \param[in] min Lower value of the range.
	 * \param[in] max Higher value of the range.
	 ************************************************************************/
	void RangeChanged(int min, int max);

	/********************************************************************//**
	 * StatusChanged signal. It is emitted by the Flash() and Read() methods
	 * when their internal ostatus changes, to change the text of the
	 * control showing the operation status
	 *
	 * \param[in] status Status string.
	 ************************************************************************/
	void StatusChanged(const QString &status);

	/********************************************************************//**
	 * ValueChanged signal. It is emitted by the Flash() and Read() methods
	 * when the position of the flash/read cursor changes.
	 *
	 * \param[in] value Position of the flash/read cursor.
	 ************************************************************************/
	void ValueChanged(int value);

};

#endif /*_FLASH_MAN_H_*/

/** \} */


/************************************************************************//**
 * \file
 * \brief General purpose utiities.
 *
 * \defgroup util util
 * \{
 * \brief General purpose utilities.
 * 
 * \author Jesus Alonso (doragasu)
 * \date   2015
 ****************************************************************************/
#ifndef _UTIL_H_
#define _UTIL_H_

// Try to detect if we are building for Windows, and define __OS_WIN if true.
#if defined(_WIN32) || defined(WIN32) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__BORLANDC__)
/// Windows build system detected.
#define __OS_WIN
#include <windows.h>
#else
#include <unistd.h>
#endif

#include <stdint.h>

#ifndef TRUE
/// TRUE value definition for logic comparisons.
#define TRUE 1
#endif
#ifndef FALSE
/// FALSE value definition for logic comparisons.
#define FALSE 0
#endif

// Common macros to obtain maximum/minimum values.
#ifndef MAX
/// Return the maximum between two numbers.
#define MAX(a,b)	((a)>(b)?(a):(b))
#endif
#ifndef MIN
/// Return the minimum between two numbers.
#define MIN(a,b)	((a)<(b)?(a):(b))
#endif

/// printf-like macro that writes to stderr instead of stdout
#define PrintErr(...)	do{fflush(stdout);fprintf(stderr, __VA_ARGS__);}while(0)

/// Printf-like macro that prints only if condition is TRUE.
#define CondPrintf(cond, ...)	do{if(cond) printf(__VA_ARGS__);}while(0)

// Delay ms function, compatible with both Windows and Unix
#ifdef __OS_WIN
/// Delay the specified amount of milliseconds.
#define DelayMs(ms) Sleep(ms)
#else
/// Delay the specified amount of milliseconds.
#define DelayMs(ms) usleep((ms)*1000)
#endif

/// Definition of a file representing a memory image.
typedef struct {
    char *file;		///< Image file name
    uint32_t addr;	///< Memory address of the image
    uint32_t len;	///< Length of the memory image
} MemImage;

/// Memory range: base addres + length
typedef struct {
    uint32_t addr;  ///< Memory base address
    uint32_t len;   ///< Memory range length
} MemRange;

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************//**
 * \brief Allocates a RAM buffer and reads the specified MemImage file to
 * that buffer.
 *
 * \param[inout] f Memory image to program to specified chip.
 *
 * \return Pointer to the raw data of the allocated file or NULL if error.
 *
 * \note If f->len is 0, it is updated with the length of the input file.
 * \warning Buffer must be externally deallocated when no longer needed,
 *          using free().
 ****************************************************************************/
uint8_t *Alloc(MemImage *f);

/************************************************************************//**
 * \brief Compares two memory regions.
 *
 * \param[in] m1  First memory region to compare.
 * \param[in] m2  Second memory region to compare.
 * \param[in] len Length of the regions to compare.
 *
 * \return len if zones are equal, the failing index otherwise.
 ****************************************************************************/
uint32_t MemCompare(const void *m1, const void *m2, uint32_t len);

/// 
/************************************************************************//**
 * \brief Copy a string.
 *
 * \param[out] dst Destination of the copy.
 * \param[in]  org Origin to copy from.
 *
 * \return A pointer to the termination character of dst (the '\0').
 ****************************************************************************/
char *StrCopy(char *dst, const char *org);

#ifndef __OS_WIN
/************************************************************************//**
 * \brief Returns true if specified kernel module is loaded
 *
 * \param[in] module_name Name of the kernel module to search.
 *
 * \return True if the module is loaded, false otherwise.
 *
 * \warning This does not make sense for macos, an alternative must be coded.
 ****************************************************************************/
int ModuleLoaded(const char *module_name);
#endif

#ifdef __cplusplus
}
#endif

#endif //_UTIL_H_

/** \} */


/************************************************************************//**
 * \file
 * \brief Allows sending commands to the programmer, and receiving results.
 *
 * \author Jesus Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __OS_WIN
#include <windows.h>
#define SEPARATOR '\\'
#else
#include <limits.h>
#include <unistd.h>
/// Separator used for filesystem paths
#define SEPARATOR '/'
#endif

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include <libusb-1.0/libusb.h>
#include "latticeflash.h"
#include "cmd.h"
#include "util.h"

/// USB configuration of the programmer
#define CMD_USB_CONFIG              1
/// USB interface of the programmer
#define CMD_USB_INTERFACE           0
/// USB OUT endpoint of the programmer
#define CMD_USB_ENDPOINT_OUT        0x04
/// USB IN endpoint of the programmer
#define CMD_USB_ENDPOINT_IN         0x83
/// Endpoint length of the USB programmer
#define CMD_USB_ENDPOINT_LENGTH     64
/// USB programmer maximum transfer lenght
#define CMD_USB_MAX_TRANSFER_LEN    384
/// USB command timeout (milliseconds)
#define CMD_USB_TIMEOUT             3000

/// Path of the bitfile in the XCF XML file
#define CMD_XCF_XPATH  "/ispXCF/Chain/Device/File"

/// Number of supported mappers
static int n_mappers = 0;

/// Data of the supported mappers
static const struct CmdMapperData *md = NULL;

/// Copies the specified address to a byte array field
#define CMD_SET_ADDR(field, addr)	do{	\
    (field)[0] = (addr)>>16;			\
    (field)[1] = (addr)>>8;				\
    (field)[2] = (addr);				\
} while(0)

/// Copies the command length to the specified byte array field
#define CMD_SET_LEN(field, len)	do{	\
    (field)[0] = (len)>>8;			\
    (field)[1] = (len);				\
} while(0)

/// Device handle
static libusb_device_handle *husb = NULL;
/// Device pointer
static libusb_device *usb;

/************************************************************************//**
 * Obtains the complete path of the applicatin binary.
 *
 * \param[out] path Obtained path of the application.
 *
 * \return Last character of the application path, or NULL if it could not be
 * obtained.
 ****************************************************************************/
static char *AppPath(char *path)
{
    char *aux;

    // Build bitfile path name
#ifdef __OS_WIN
    wchar_t wc_path[MAX_PATH];

    GetModuleFileName(NULL, wc_path, MAX_PATH);
    wcstombs(path, wc_path, MAX_PATH);
#else
    readlink("/proc/self/exe", path, PATH_MAX);
#endif
    aux = strrchr(path, SEPARATOR);
    if (!aux) {
        NULL;
    }
    aux++;
    *aux = '\0';
    return aux;
}

/************************************************************************//**
 * Obtains the complete path of the bitfile corresponding to a mapper type.
 *
 * \param[in]  type    Mapper input type.
 * \param[in]  vMirror Vertical mirroring when true. Ignored if mapper does not
 *             support mirroring variants.
 * \param[out] path    Resulting path of the mapper bitfile.
 *
 * \return 0 if success, less than 0 if error.
 ****************************************************************************/
static int MapperBitfilePath(int type, int vMirror, char *path)
{
    char *aux;

    aux = AppPath(path);
    if (!aux) {
        *path = '\0';
        return -1;
    }
    aux = StrCopy(aux, "mappers");
    *aux++ = SEPARATOR;
    aux = StrCopy(aux, CmdMapperName(type));
    if (CmdMapperHasMirroringVariants(type)) {
        aux = StrCopy(aux, vMirror?"_v":"_h");
    }
    StrCopy(aux, ".jed");

    return 0;
}

/************************************************************************//**
 * Obtains the complete path of the XCF file used to burn mappers.
 *
 * \param[out] path Resulting path of the XCF file.
 *
 * \return 0 if success, less than 0 if error.
 ****************************************************************************/
static int MapperXcfPath(char *path)
{
    char *aux;

    aux = AppPath(path);
    if (!aux) {
        *path = '\0';
        return -1;
    }
    aux = StrCopy(aux, "mappers");
    *aux++ = SEPARATOR;
    StrCopy(aux, "burn.xcf");

    return 0;
}

/************************************************************************//**
 * Sets the path of the bitfile into the specified XCF file.
 *
 * \param[in] xcfFile Path of the XCF file.
 * \param[in] bitFile Path of the bitfile to set into the XCF file.
 *
 * \return 0 if success, less than 0 if error.
 ****************************************************************************/
static int SetXcfMapperPath(const char *xcfFile, const char *bitFile)
{
    xmlDocPtr doc;
    xmlXPathContextPtr xpathCtx; 
    xmlXPathObjectPtr xpathObj; 
    FILE *f;
    
    doc = xmlParseFile(xcfFile);
    if (!doc) {
    	PrintErr("Unable to parse XCF file %s\n", xcfFile);
    	return -1;
    }

    xpathCtx = xmlXPathNewContext(doc);
    if(!xpathCtx) {
        PrintErr("Unable to create new XPath context\n");
        xmlFreeDoc(doc); 
        return -1;
    }
    
    xpathObj = xmlXPathEvalExpression(BAD_CAST CMD_XCF_XPATH, xpathCtx);
    if(!xpathObj) {
        PrintErr("Unable to evaluate xpath expression %s\n", CMD_XCF_XPATH);
        xmlXPathFreeContext(xpathCtx); 
        xmlFreeDoc(doc); 
        return -1;
    }

	xmlNodeSetContent(xpathObj->nodesetval->nodeTab[0], BAD_CAST bitFile);

    
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx); 

    f = fopen(xcfFile, "w");

    xmlDocDump(f, doc);

    fclose(f);

    xmlFreeDoc(doc); 
    
    return 0;
}

int CmdInit(uint32_t deviceId, int num_mappers,
        const struct CmdMapperData *mapper)
{
    // Check we have at least a mapper
    if (num_mappers <= 0 || !mapper) {
        PrintErr("No mappers supported!\n");
        return CMD_ERROR;
    }
    // Check if already initialized
    if (husb && usb) return CMD_OK;
    // Init libusb
	if (libusb_init(NULL) < 0) {
        PrintErr("could not init libusb!\n" );
        return CMD_ERROR;
    }

	// Uncomment this to flood the screen with libusb debug information
	//libusb_set_debug(NULL, LIBUSB_LOG_LEVEL_DEBUG);

    // Detecting device
	husb = libusb_open_device_with_vid_pid(NULL, deviceId>>16,
            deviceId & 0xFFFF);

	if(!husb) {
		PrintErr("could not open device %04X:%04X!\n", deviceId>>16,
                deviceId & 0xFFFF);
		return CMD_ERROR;
	}

    usb = libusb_get_device(husb);
    if (!usb) {
        PrintErr("could not get device!\n");
        return CMD_ERROR;
    }


    // Set configuration
	
	if(libusb_set_configuration(husb, CMD_USB_CONFIG) < 0) {
        PrintErr("could not set configuration #%d!\n", CMD_USB_CONFIG);
        return CMD_ERROR;
	}

    // Claim interface
    if(libusb_claim_interface(husb, CMD_USB_INTERFACE) != LIBUSB_SUCCESS)
    {
        PrintErr( "could not claim interface #%d\n", CMD_USB_INTERFACE);
        return CMD_ERROR;
    }

    n_mappers = num_mappers;
    md = mapper;

	return CMD_OK;
}

/************************************************************************//**
 * Write data to the USB interface.
 *
 * \param[in] data   Data to write.
 * \param[in] length Length of data array in bytes.
 *
 * \return CMD_OK on success, CMD_ERROR on failure.
 ****************************************************************************/
static int CmdUsbWrite(const uint8_t *data, int length)
{
    int ret;
    int size;

    // Send data
    ret = libusb_bulk_transfer(husb, CMD_USB_ENDPOINT_OUT,
            (unsigned char*)data, length, &size, CMD_USB_TIMEOUT);

    if(ret != LIBUSB_SUCCESS || size != length) {
		PrintErr("USB write %d bytes failed: %s",  length,
		    libusb_error_name(ret));
		return CMD_ERROR;
	}

    return CMD_OK;
}

/************************************************************************//**
 * Read data from USB interface.
 *
 * \param[out] data       Array with the read data.
 * \param[in]  length     Maximum length to read.
 * \param[in]  timeout_ms Timeout in milliseconds of the read operation.
 *
 * \return 0 if success, less than 0 if error.
 ****************************************************************************/
static int CmdUsbRead(uint8_t *data, int length, int timeout_ms)
{
    int ret;
    int size;

    // Receive data
    ret = libusb_bulk_transfer(husb, CMD_USB_ENDPOINT_IN,
            data, length, &size, timeout_ms);

    if(ret != LIBUSB_SUCCESS) {
		PrintErr("USB read failed: %s!\n", libusb_error_name(ret));
		return CMD_ERROR;
	}

    return size;
}

int CmdSend(const Cmd *cmd, uint8_t cmdLen, CmdRep *rep, int timeout_ms)
{
    int recv;

    // Send command
    if (CmdUsbWrite(cmd->data, cmdLen) == CMD_ERROR) return CMD_ERROR;
    // Exception to the norm: BOOTLOADER command does not send a reply
    if (CMD_BOOTLOADER == cmd->command) return 0;
    // Get reply
    recv = CmdUsbRead(rep->data, CMD_USB_ENDPOINT_LENGTH, timeout_ms);
    if (CMD_ERROR == recv) return CMD_ERROR;

    if (CMD_REP_ERROR == rep->command) {
        PrintErr("programmer returned fail to command %d!\n", cmd->command);
        return CMD_ERROR;
    }

    return recv;
}

int CmdSendLongCmd(const Cmd *cmd, uint8_t cmdLen, const uint8_t *data,
				   int dataLen, CmdRep *rep, int timeout_ms)
{
    int ret;
    int size;

    // Send standard command and get reply
    if (CmdSend(cmd, cmdLen, rep, timeout_ms) == CMD_ERROR) {
        return CMD_ERROR;
    }

    // Send big data payload
    ret = libusb_bulk_transfer(husb, CMD_USB_ENDPOINT_OUT,
            (unsigned char*)data, dataLen, &size, CMD_USB_TIMEOUT);
    
    if ((ret != LIBUSB_SUCCESS) && (size != dataLen)) {
    	PrintErr("failed to send %d bytes payload: %s!\n", dataLen,
                libusb_error_name(ret));
        return CMD_ERROR;
    }

    return CMD_OK;
}

int CmdSendLongRep(const Cmd *cmd, uint8_t cmdLen, CmdRep *rep,
				   uint8_t *data, int recvLen, int timeout_ms)
{
    int ret;
    int size;
    int step;
    int recvd;

    // Send standard command and get reply
    if (CmdSend(cmd, cmdLen, rep, timeout_ms) == CMD_ERROR) {
        return CMD_ERROR;
    }

    // Receive big data payload
	for (recvd = 0; recvd < recvLen; recvd += step) {
		step = MIN(CMD_USB_MAX_TRANSFER_LEN, recvLen - recvd);
		ret = libusb_bulk_transfer(husb, CMD_USB_ENDPOINT_IN,
				data + recvd, step, &size, CMD_USB_TIMEOUT);
	
		if ((ret != LIBUSB_SUCCESS) || (size != step)) {
			PrintErr("failed to receive %d bytes payload: %s\n", recvLen, 
                    libusb_error_name(ret));
            return CMD_ERROR;
		}
	}
    
    return CMD_OK;
}

int CmdBootloader(void)
{
    Cmd cmd;
    CmdRep rep;

    cmd.command = CMD_BOOTLOADER;
    if (CmdSend(&cmd, 1, &rep, CMD_TOUT_MS) < 0) {
        return CMD_ERROR;
    }

    return CMD_OK;
}

int CmdFIdGet(CmdFlashId id[2])
{
    Cmd cmd;
    CmdRep rep;

    cmd.command = CMD_FLASH_ID;

    if (CmdSend(&cmd, 1, &rep, CMD_TOUT_MS) < 0) {
        return CMD_ERROR;
    }

    id[0] = rep.fId.chr;
    id[1] = rep.fId.prg;

    return CMD_OK;
}

int CmdFlashErase(enum CmdChip chip, uint32_t addr)
{
    Cmd cmd;
    CmdRep rep;

    cmd.rdWr.cmd = CMD_CHR_ERASE + chip;
    CMD_SET_ADDR(cmd.rdWr.addr, addr);

    if (CmdSend(&cmd, 4, &rep, addr == CMD_ERASE_FULL ? CMD_TOUT_LONG_MS :
                CMD_TOUT_MS) < 0) {
        return CMD_ERROR;
    }

    return CMD_OK;
}

int CmdRangeErase(enum CmdChip chip, uint32_t addr, uint32_t length)
{
    Cmd cmd;
    CmdRep rep;

    cmd.rdWr.cmd = CMD_CHR_RANGE_ERASE + chip;
    cmd.rangeErase.start[0] = addr>>16;
    cmd.rangeErase.start[1] = addr>>8;
    cmd.rangeErase.start[2] = addr;
    cmd.rangeErase.len[0] = length>>16;
    cmd.rangeErase.len[1] = length>>8;
    cmd.rangeErase.len[2] = length;

    if (CmdSend(&cmd, sizeof(cmd.rangeErase), &rep, CMD_TOUT_LONG_MS) < 0) {
        return CMD_ERROR;
    }
    return CMD_OK;
}

int CmdMapperNumToType(uint16_t mapper) {
    int i;

    for (i = 0; (i < n_mappers) && (mapper != md[i].number); i++);

    return i;
}

const char *CmdMapperName(int type) {
    if (type > n_mappers) {
        return NULL;
    }

    return md[type].name;
}

int CmdMapperCfg(int type) {
    Cmd cmd;
    CmdRep rep;

    if (type >= n_mappers) {
        return CMD_ERROR;
    }

    cmd.mapperSet.cmd = CMD_MAPPER_SET;
    cmd.mapperSet.mapper = md[type].number;
    cmd.mapperSet.pad = 0;

    if (CmdSend(&cmd, sizeof(CmdMapperSet), &rep, CMD_TOUT_MS) < 0) {
        return CMD_ERROR;
    }

    return CMD_OK;
}

int CmdWrite(enum CmdChip chip, const uint8_t *buf, uint32_t addr,
        uint16_t len)
{
    Cmd cmd;
    CmdRep rep;

    switch (chip) {
        case CMD_CHIP_CHR_FLASH:
            cmd.rdWr.cmd = CMD_CHR_WRITE;
            break;

        case CMD_CHIP_PRG_FLASH:
            cmd.rdWr.cmd = CMD_PRG_WRITE;
            break;

        case CMD_CHIP_PRG_RAM:
            cmd.rdWr.cmd = CMD_RAM_WRITE;
            break;

        default:
            return CMD_ERROR;

    }

    CMD_SET_ADDR(cmd.rdWr.addr, addr);
    CMD_SET_LEN(cmd.rdWr.len, len);
    if ((CmdSendLongCmd(&cmd, sizeof(CmdRdWrHdr), buf, len, &rep,
                    CMD_TOUT_MS) != CMD_OK) || (rep.command != CMD_OK)) {
        return CMD_ERROR;
    }

    return CMD_OK;
}

int CmdRead(enum CmdChip chip, uint8_t *buf, uint32_t addr, uint16_t len)
{
    Cmd cmd;
    CmdRep rep;

    switch (chip) {
        case CMD_CHIP_CHR_FLASH:
            cmd.rdWr.cmd = CMD_CHR_READ;
            break;

        case CMD_CHIP_PRG_FLASH:
            cmd.rdWr.cmd = CMD_PRG_READ;
            break;

        case CMD_CHIP_PRG_RAM:
            cmd.rdWr.cmd = CMD_RAM_READ;
            break;

        default:
            return CMD_ERROR;

    }

    CMD_SET_ADDR(cmd.rdWr.addr, addr);
    CMD_SET_LEN(cmd.rdWr.len, len);
    if ((CmdSendLongRep(&cmd, sizeof(CmdRdWrHdr), &rep, buf, len,
                    CMD_TOUT_MS) != CMD_OK) || (rep.command != CMD_REP_OK)) {
        return CMD_ERROR;
    }

    return CMD_OK;
}

int CmdFwGet(uint16_t *fwVer)
{
    Cmd cmd;
    CmdRep rep;

    cmd.command = CMD_FW_VER;

    if (CmdSend(&cmd, 1, &rep, CMD_TOUT_MS) < 0) {
        return -1;
    }
    *fwVer = (rep.fwVer.ver_major<<8) | (rep.fwVer.ver_minor);

    return 0;
}

int CmdMapperHasMirroringVariants(int type)
{
    if (type > n_mappers) {
        return -1;
    }

    return md[type].mirror_variant;
}

int CmdMapperProgram(int type, int vMirror)
{
    char bitFile[PATH_MAX];
    char xcfFile[PATH_MAX];
    int err;

    if (type > n_mappers) {
        return -1;
    }

    err = MapperXcfPath(xcfFile);
    if (!err) {
        err = MapperBitfilePath(type, vMirror, bitFile);
    }
    if (err) {
        return -1;
    }

    err = SetXcfMapperPath(xcfFile, bitFile);
    if (err) {
        return -1;
    }
    printf("Burning %s\n", bitFile);

    // Ignore return value because lattice programmer is bugged and segfaults
    // after finishing flashing
    LatticeFlash(xcfFile);

    return 0;
}

int CmdMapperNumMappers(void)
{
    return n_mappers;
}

void CmdClose(void)
{
    if (!husb) {
        return;
    }

    libusb_release_interface(husb, 0 );
    libusb_close(husb);
    libusb_exit(NULL);
}


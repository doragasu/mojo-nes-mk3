/************************************************************************//**
 * \file
 * \brief Flash specified xcf file to FPGA.
 *
 * \defgroup latticeflash latticeflash
 * \{
 * \brief Flash specified xcf file to FPGA.
 *
 * \author Jesus Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#ifndef _LATTICE_FLASH_H_
#define _LATTICE_FLASH_H_

#include "util.h"

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************//**
 * Module initialization.
 *
 * \param[in] path Complete path to lattice programmer.
 ****************************************************************************/
void LatticeFlashInit(const char *path);

/************************************************************************//**
 * Uses lattice prgcmd to burn xcf file to FPGA
 *
 * \param[in] xcf    XCF input file to burn to FPGA.
 *
 * \return -1 if spawning avrdude failed, -2 if avrdude didn't exit
 *         properly, -3 if any other error occurred,  or avrdude return
 *         code if otherwise.
 ****************************************************************************/
int LatticeFlash(const char *xcf);

#ifdef __cplusplus
}
#endif

#endif /*_LATTICE_FLASH_H_*/

/** \} */


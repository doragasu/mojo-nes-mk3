/************************************************************************//**
 * \file
 * \brief Flash specified xcf file to FPGA.
 *
 * \author Jesus Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#include "latticeflash.h"
#include "pspawn.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char *prgcmd = NULL;

void LatticeFlashInit(const char *path)
{
    prgcmd = path;
}

int LatticeFlash(const char *xcf)
{
    char *cmd = NULL;
	int retVal = -3;

    cmd = (char*)malloc(strlen(prgcmd) + strlen(xcf) + 10);
    if (!cmd) {
        goto out;
    }
    sprintf(cmd, "%s -infile %s", prgcmd, xcf);

	if ((retVal = system(cmd))) {
//		PrintErr("Lattice programmer failed!\n");
	}

out:
    if (cmd) free(cmd);
	// Return error code
    printf("Return: %d\n", retVal);
	return retVal;
}


/************************************************************************//**
 * \file
 * \brief General purpose utiities.
 *
 * \author Jesus Alonso (doragasu)
 * \date   2015
 ****************************************************************************/
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// Length of a line, used by the ModuleLoaded() function.
#define LINE_LEN    256

uint8_t *Alloc(MemImage *f)
{
    FILE *rom;
    uint8_t *writeBuf;

    if (!(rom = fopen(f->file, "rb"))) {
        perror(f->file);
        return NULL;
    }

    // Obtain length if not specified
    if (!f->len) {

        fseek(rom, 0, SEEK_END);
        f->len = ftell(rom);
        fseek(rom, 0, SEEK_SET);
    }

    writeBuf = (uint8_t*)malloc(f->len);
    if (!writeBuf) {
        perror("Allocating write buffer");
        fclose(rom);
        return NULL;
    }
    // Read the entire ROM and close file
    if (1 > fread(writeBuf, f->len, 1, rom)) {
        fclose(rom);
        free(writeBuf);
        PrintErr("Error reading ROM file!\n");
        return NULL;
    }
    fclose(rom);

    return writeBuf;
}

uint32_t MemCompare(const void *m1, const void *m2, uint32_t len)
{
    uint32_t i;
    uint8_t *p1 = (uint8_t*)m1;
    uint8_t *p2 = (uint8_t*)m2;

    for (i = 0; (i < len) && (p1[i] == p2[i]); i++);

    return i;
}

char *StrCopy(char *dst, const char *org)
{
    if (org) {
        while (*org != '\0') {
            *dst++ = *org++;
        }
    }
    *dst = '\0';

    return dst;
}

#ifndef __OS_WIN
int ModuleLoaded(const char *module_name)
{
    FILE *f = NULL;
    char line[LINE_LEN];

    f = fopen("/proc/modules", "r");
    if (!f) {
        return 0;
    }
    while (fgets(line, LINE_LEN, f)) {
        if (strstr(line, module_name) == line) {
            fclose(f);
            return 1;
        }
    }

    fclose(f);
    return 0;
}
#endif


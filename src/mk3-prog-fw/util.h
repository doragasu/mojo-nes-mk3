/************************************************************************//**
 * \file
 * \brief General purpose utility macros and definitions.
 *
 * \defgroup util util
 * \{
 * \brief General purpose utility macros and definitions.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#ifndef _UTIL_H_
#define _UTIL_H_

// Common TRUE/FALSE definitions
#ifndef TRUE
/// TRUE definition
#define TRUE 1
#endif
#ifndef FALSE
/// False definition
#define FALSE 0
#endif

#ifndef NULL
/// Definition of NULL value for the C language
#define NULL ((void*)0)
#endif

#ifndef MAX
/// Returns the bigger of two numbers
#define MAX(a, b)	((a)>(b)?(a):(b))
#endif
#ifndef MIN
/// Returns the smaller of two numbers
#define MIN(a, b)	((a)<(b)?(a):(b))
#endif

/// Printf function for debugging
#ifdef _DEBUG
#include <stdio.h>
#define dprintf(...)    printf(__VA_ARGS__)
#else
#define dprintf(...)
#endif

/// Obtains the number of elements of an array
#define ARRAY_LENGTH(array) (sizeof(array) / sizeof((array)[0]))

#endif //_UTIL_H_

/** \} */


/************************************************************************//**
 * \file
 * \brief System state machine. Receives system events and performs the
 *        corresponding actions.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2016
 ****************************************************************************/

#include <LUFA/Drivers/Board/LEDs.h>
#include "system.h"
#include "flash.h"
#include "mapper.h"
#include "bloader.h"

/// Macro to build an address for a command, with correct byte ordering.
#define SYS_RDWR_ADDR(addr)	(((uint32_t)(addr)[0]<<16) | \
        ((uint32_t)(addr)[1]<<8) | (uint32_t)(addr)[2])

/// Macro to build the length for a command, with correct byte ordering.
#define SYS_RDWR_LEN(len)	(((uint16_t)(len)[0]<<8) | \
        ((uint16_t)(len)[1]))

/// Instance of the system FSM
static SysInstance si;

/// Buffer for command reception and reply
static SysBuf buf;

// NOTE: Flash write functions will block waiting data until
// operation completes. Not elegant, but eases a lot the
// implementation.

void SysInit(void)
{
	dprintf("\n\n--= MOJO-NES MK3 PROGRAMMER =--\n\n");
    // Set default values
    memset(&si, 0, sizeof(SysInstance));
    // TODO: System timer initialization
    si.s = SYS_IDLE;
	dprintf("[STA] IDLE\n");
}

/************************************************************************//**
 * \brief Receive a complete endpoint data frame.
 *
 * \param[out] data Array containing the received data.
 ****************************************************************************/
static void SysDataRecv(SysBuf *buf)
{
    // We do not need to select endpoint, as it has been previously
    // selected to check if there is incoming data
    Endpoint_Read_Stream_LE(buf->data, VENDOR_O_EPSIZE, NULL);
    Endpoint_ClearOUT();
}

/************************************************************************//**
 * \brief Send a complete endpoint data frame.
 *
 * \param[in] data Array with the data to send.
 * \param[in] len  Number of bytes of data to send.
 ****************************************************************************/
static void SysDataSend(SysBuf *buf, uint16_t len)
{
    memset(buf->data+len, 0, VENDOR_I_EPSIZE-len);
    Endpoint_SelectEndpoint(VENDOR_IN_EPADDR);
	/// \todo Should we write len instead of endpoint length?
    Endpoint_Write_Stream_LE(buf->data, VENDOR_I_EPSIZE, NULL);
    Endpoint_ClearIN();
}

/************************************************************************//**
 * Receives data from the host, and programs it to a Flash chip.
 *
 * \param[in] chip Chip to program data to.
 * \param[in] addr Address to program data to.
 * \param[in] len  Number of bytes to program.
 *
 * \return SYS_OK if data was received and programmed successfully.
 *         SYS_ERROR otherwise.
 ****************************************************************************/
static int8_t SysFlashWrite(uint8_t chip, uint32_t addr, uint16_t len)
{
    uint16_t step;
    uint16_t i;
    uint8_t toWrite;
    uint8_t written;

    dprintf("%s write 0x%06lX:%04X\n", chip?"CHR":"PRG", addr, len);
    // Send first the reply to the command, and then receive and flash
    // the long data payload
    buf.rep.eRep.code = SYS_CMD_OK;
    SysDataSend(&buf, sizeof(SysRepEmpty));

    Endpoint_SelectEndpoint(VENDOR_OUT_EPADDR);
    while (len) {
#ifdef _DEBUG
		putchar('w');
#endif
        // Read data
        SysDataRecv(&buf);
        // Data received on endpoint
        step = MIN(len, VENDOR_O_EPSIZE);
        // Write data in blocks of max 32 bytes. The first
        // write takes care of avoiding crossing a 32-byte
        // write-buffer boundary. Following writes are
        // guaranteed not to cross it.
        toWrite = MIN(step, 32 - (addr & 0x1F));
        i = FlashWriteBuf(chip, addr, buf.data, toWrite);
        if (i != toWrite) return SYS_ERROR;
        addr += i;
        // First write is OK, write remaining data
        while (i < step) {
            toWrite = MIN(step - i, 32);
            written = FlashWriteBuf(chip, addr, buf.data + i, toWrite);
            i += written;
            addr += written;
            // Check for errors
            if (written != toWrite) return SYS_ERROR;
        }
        len -= i;
    }
    return SYS_OK;
}

/************************************************************************//**
 * Reads data from a Flash chip, and sends it to the host.
 *
 * \param[in] chip Chip to read from.
 * \param[in] addr Address to read from.
 * \param[in] len  Number of bytes to read.
 *
 * \return SYS_OK if data was read and sent successfully.
 *         SYS_ERROR otherwise.
 ****************************************************************************/
static int8_t SysFlashRead(uint8_t chip, uint32_t addr, uint16_t len)
{
    uint8_t readed;

    dprintf("%s read 0x%06lX:%04X\n", chip?"CHR":"PRG", addr, len);
    buf.rep.eRep.code = SYS_CMD_OK;
    SysDataSend(&buf, sizeof(SysRepEmpty));
    while (len) {
#ifdef _DEBUG
		putchar('r');
#endif
        readed = FlashReadBuf(chip, addr, buf.data, MIN(len, VENDOR_I_EPSIZE));
        SysDataSend(&buf, readed);
        if (readed > len) return SYS_ERROR;
        addr += readed;
        len -= readed;
    }
    return SYS_OK;
}

/************************************************************************//**
 * Reads data from the host, and writes it to the RAM chip.
 *
 * \param[in] addr Address to read from.
 * \param[in] len  Number of bytes to read.
 *
 * \return SYS_OK if data was read and sent successfully.
 *         SYS_ERROR otherwise.
 ****************************************************************************/
static int8_t SysRamWrite(uint32_t addr, uint16_t len)
{
    uint8_t toWrite;

    dprintf("RAM write 0x%06lX:%04X\n", addr, len);
    buf.rep.eRep.code = SYS_CMD_OK;
    SysDataSend(&buf, sizeof(SysRepEmpty));

    Endpoint_SelectEndpoint(VENDOR_OUT_EPADDR);
    while (len) {
#ifdef _DEBUG
		putchar('W');
#endif
        SysDataRecv(&buf);
        toWrite = MIN(len, VENDOR_I_EPSIZE);
        MapRamWrite(addr, toWrite, buf.data);
        addr += toWrite;
        len -= toWrite;
    }
    return SYS_OK;
}

/************************************************************************//**
 * Reads data from the RAM chip, and sends it to the host.
 *
 * \param[in] addr Address to read from.
 * \param[in] len  Number of bytes to read.
 *
 * \return SYS_OK if data was read and sent successfully.
 *         SYS_ERROR otherwise.
 ****************************************************************************/
static int8_t SysRamRead(uint32_t addr, uint16_t len)
{
    uint8_t toRead;

    dprintf("RAM read 0x%06lX:%04X\n", addr, len);
    buf.rep.eRep.code = SYS_CMD_OK;
    SysDataSend(&buf, sizeof(SysRepEmpty));
    while (len) {
#ifdef _DEBUG
		putchar('R');
#endif
        toRead = MIN(len, VENDOR_O_EPSIZE);
        if (MapRamRead(addr, toRead, buf.data) < 0) return SYS_ERROR;
        SysDataSend(&buf, toRead);
        addr += toRead;
        len -= toRead;
    }
    return SYS_OK;
}

/************************************************************************//**
 * Erase a flash chip sector, or the whole chip.
 *
 * \param[in] chip Flash chip in which the erase operation will be performed.
 * \param[in] addr If 0xFFFFFF, the whole chip is erased. Otherwise, the
 *                    sector of the pointed address is erased.
 ****************************************************************************/
static void SysFlashErase(uint8_t chip, uint32_t addr)
{
    if (0xFFFFFF == addr) {
        buf.rep.eRep.code = FlashChipErase(chip)?SYS_CMD_OK:SYS_CMD_ERROR;
    } else {
        buf.rep.eRep.code = FlashSectErase(chip, addr)?SYS_CMD_OK:SYS_CMD_ERROR;
    }
    SysDataSend(&buf, sizeof(SysRepEmpty));
}

/************************************************************************//**
 * Parse system command and perform requested action.
 *
 * \param[in] cmd Command to parse.
 *
 * \return 0
 *
 * \todo Warning, error control in this function is nonexistent
 ****************************************************************************/
static int SysCmdParse(SysCmd *cmd)
{
    uint32_t addr, len;
    uint8_t chip;

    switch(cmd->command) {
        case SYS_CMD_OK:
			dprintf("[CMD] OK\n");
            break;

        case SYS_CMD_FW_VER:
			dprintf("[CMD] FW_VER\n");
            buf.rep.fwVer.code = SYS_CMD_OK;
            buf.rep.fwVer.ver_major = SYS_FW_VER_MAJOR;
            buf.rep.fwVer.ver_minor = SYS_FW_VER_MINOR;
            SysDataSend(&buf, sizeof(SysRepFwVer));
            break;

        case SYS_CMD_BOOTLOADER:
			dprintf("[CMD] BOOTLOADER\n");
            JumpToBootloader();

        case SYS_CMD_CHR_WRITE:
            // Writes take at least two transfers: one for the header, and
            // one or more with the data payload (up to 65535 bytes).
        case SYS_CMD_PRG_WRITE:
			dprintf("[CMD] [CHR|PRG]_WRITE\n");
            chip = (cmd->command - SYS_CMD_CHR_WRITE) ^ 1;
            SysFlashWrite(chip, SYS_RDWR_ADDR(cmd->rdWr.addr),
                    SYS_RDWR_LEN(cmd->rdWr.len));
            break;

        case SYS_CMD_CHR_READ:
            // As happens with writes, reads take at least two response
            // frames, first an OK response, and then at least one frame
            // with the requested data.
        case SYS_CMD_PRG_READ:
			dprintf("[CMD] [CHR|PRG]_READ\n");
            chip = (cmd->command - SYS_CMD_CHR_READ) ^ 1;
            SysFlashRead(chip, SYS_RDWR_ADDR(cmd->rdWr.addr),
                    SYS_RDWR_LEN(cmd->rdWr.len));
            break;

        case SYS_CMD_CHR_ERASE:
        case SYS_CMD_PRG_ERASE:
			dprintf("[CMD] [CHR|PRG]_ERASE\n");
            chip = (cmd->command - SYS_CMD_CHR_ERASE) ^ 1;
            SysFlashErase(chip, SYS_RDWR_ADDR(cmd->rdWr.addr));
            break;

        case SYS_CMD_FLASH_ID:
			dprintf("[CMD] FLASH_ID\n");
            // Request IDs of both chips (leave space for reply)
            buf.rep.fId.code = SYS_CMD_OK;
            buf.rep.fId.prg.manId = FlashGetManId(CIF_CHIP_PRG);
            FlashGetDevId(CIF_CHIP_PRG, buf.rep.fId.prg.devId);
            buf.rep.fId.chr.manId = FlashGetManId(CIF_CHIP_CHR);
            FlashGetDevId(CIF_CHIP_CHR, buf.rep.fId.chr.devId);
            SysDataSend(&buf, sizeof(SysRepFlashId));
            break;

        case SYS_CMD_RAM_WRITE:
			dprintf("[CMD] RAM_WRITE\n");
            SysRamWrite(SYS_RDWR_ADDR(cmd->rdWr.addr), SYS_RDWR_LEN(
                        cmd->rdWr.len));
            break;

        case SYS_CMD_RAM_READ:
			dprintf("[CMD] RAM_READ\n");
            SysRamRead(SYS_RDWR_ADDR(cmd->rdWr.addr), SYS_RDWR_LEN(
                        cmd->rdWr.len));
            break;

        case SYS_CMD_MAPPER_SET:
			dprintf("[CMD] MAPPER_SET\n");
            if (MapInit(cmd->mapper.id)) {
                buf.rep.command = SYS_CMD_ERROR;
            } else {
                buf.rep.command = SYS_CMD_OK;
            }
            SysDataSend(&buf, 1);
            break;

        case SYS_CMD_CHR_RANGE_ERASE:
        case SYS_CMD_PRG_RANGE_ERASE:
			dprintf("[CMD] [CHR|PRG]_RANGE_ERASE\n");
            /// Range erase
            chip = (cmd->command - SYS_CMD_CHR_RANGE_ERASE) ^ 1;
            addr = (((uint32_t)cmd->rangeErase.start[0])<<16) |
                (((uint32_t)cmd->rangeErase.start[1])<<8)  |
                cmd->rangeErase.start[2];
            len =  (((uint32_t)cmd->rangeErase.len[0])<<16) |
                (((uint32_t)cmd->rangeErase.len[1])<<8)  |
                cmd->rangeErase.len[2];

            if (FlashRangeErase(chip, addr, len) == 0) {
                buf.rep.command = SYS_CMD_OK;
            } else {
                buf.rep.command = SYS_CMD_ERROR;
            }
            SysDataSend(&buf, 1);
            break;

        case SYS_CMD_ERROR:
			dprintf("[CMD] ERROR\n");
            buf.rep.command = SYS_CMD_ERROR;
            SysDataSend(&buf, 1);
            break;

        default:
			dprintf("[CMD] UNSUPPORTED\n");
            buf.rep.command = SYS_CMD_ERROR;
            SysDataSend(&buf, 1);
            break;
    }

    return 0;
}

/************************************************************************//**
 * \brief Resets cartridge and starts timer to wait for chip ready.
 ****************************************************************************/
static void SysCartInit(void)
{
    // Launch 100 ms timer to wait until chip is ready to accept commands
    Timer1Config(TimerMsToCount(100));
    Timer1Start();
    si.s = SYS_CART_INIT;
	dprintf("[STA] CART_INIT\n");
}

/************************************************************************//**
 * \brief Puts cart pins at their default (idle bus) state.
 ****************************************************************************/
static void SysCartRemove(void)
{
    FlashIdle();
    si.s = SYS_IDLE;
	dprintf("[STA] IDLE\n");
}

void SysFsmCycle(SysEvt evt)
{
    // Holds reply length
    uint16_t repLen = 0;

    // Process prioritary events (e.g. cart in/out) and events that can
    // generate more events (like data reception from host).
    // TODO: might be better removing cart events, and checking cart
    // status here.
    switch (evt) {
        case SYS_EVT_TIMER:
			dprintf("[EVT] TIMER\n");
            if (si.s == SYS_STAB_WAIT) {
                LEDs_TurnOffLEDs(LEDS_LED2);
                // Check if cart is finally inserted and USB ready
                if (si.f.cart_in && si.f.usb_ready) {
					SysCartInit();
				} else {
					SysCartRemove();
				}
            } else if (si.s == SYS_CART_INIT) {
                // Reset finished, cart should be ready to accept commands.
                // Obtain IDs.
                si.fc[0].manId = FlashGetManId(CIF_CHIP_PRG);
                FlashGetDevId(CIF_CHIP_PRG, si.fc[0].devId);
                si.fc[1].manId = FlashGetManId(CIF_CHIP_CHR);
                FlashGetDevId(CIF_CHIP_CHR, si.fc[1].devId);
				dprintf("[INF] PRG: %02X:%02X:%02X:%02X\n", si.fc[0].manId,
						si.fc[0].devId[0], si.fc[0].devId[1],
						si.fc[0].devId[2]);
				dprintf("[INF] CHR: %02X:%02X:%02X:%02X\n", si.fc[1].manId,
						si.fc[1].devId[0], si.fc[1].devId[1],
						si.fc[1].devId[2]);
                // Finally jump to ready state
                si.s = SYS_READY;
				dprintf("[STA] READY\n");
            }
            break;

        case SYS_EVT_CIN:			// Cartridge inserted
			dprintf("[EVT] CIN\n");
            si.f.cart_in = TRUE;
            if (si.s == SYS_IDLE) {
                si.s = SYS_STAB_WAIT;
				dprintf("[STA] STAB_WAIT\n");
                // Launch 1 s debounce timer
                Timer1Config(TimerMsToCount(3000));
                Timer1Start();
                LEDs_TurnOnLEDs(LEDS_LED2);
            }
            break;

        case SYS_EVT_COUT:			// Cartridge removed
			dprintf("[EVT] COUT\n");
            si.f.cart_in = FALSE;
            if (si.s != SYS_STAB_WAIT) {
                // Remove cart and return to IDLE state
                SysCartRemove();
            }
            break;

        case SYS_EVT_USB_ATT:		// USB attached and enumerated
			dprintf("[EVT] USB_ATT\n");
            si.f.usb_ready = TRUE;
            // Check if cart is inserted and we are IDLE.
            if (si.f.cart_in && si.s == SYS_IDLE) SysCartInit();
            break;

        case SYS_EVT_USB_DET:		// USB detached
        case SYS_EVT_USB_ERR:		// Error on USB interface
			dprintf("[EVT] USB_DET/ERR\n");
            si.f.usb_ready = FALSE; break;
            SysCartRemove();
            break;

        case SYS_EVT_DIN:
			dprintf("[EVT] DIN\n");
            // Get data from USB endpoint
            SysDataRecv(&buf);
            // If not in READY, commands allowed are restricted
            if ((SYS_READY == si.s) || (SYS_CMD_FW_VER == buf.cmd.command) ||
					(SYS_CMD_BOOTLOADER == buf.cmd.command)) {
                SysCmdParse(&buf.cmd);
            } else {
                buf.rep.command = SYS_CMD_ERROR;
                repLen = 1;
            }
            if (repLen) {
				SysDataSend(&buf, repLen);
			}
            break;

        case SYS_EVT_SW_PRESS:		// Button pressed event
			dprintf("[EVT] SW_PRESS\n");
            si.sw = SYS_SW_EVENT | SYS_SW_PRESSED;
            break;

        case SYS_EVT_SW_REL:		// Button released event
			dprintf("[EVT] SW_REL\n");
            si.sw = SYS_SW_EVENT;
            break;

        default:
            break;
    }
}


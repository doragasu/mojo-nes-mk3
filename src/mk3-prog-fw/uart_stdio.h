/************************************************************************//**
 * \file
 * \brief Allows using UART1 for the standard output.
 *
 * \defgroup uart_stdio uart_stdio
 * \{
 * \brief Allows using UART1 for the standard output.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2018
 ****************************************************************************/

#ifndef _UART_STDIO_H_
#define _UART_STDIO_H_

/// Desired UART baudrate. Note that achievable baudrates are integer values
/// obtained with the expression F_CPU / (N*16).
#define UART_STDIO_BAUD_RATE  500000

/************************************************************************//**
 * \brief This function sets up UART1 and configures the C library to use this
 * UART for the standard output.
 *
 * The UART is configured with the baudrate defined in UART_STDIO_BAUD_RATE,
 * with 8 data bits, 1 stop bits and no parity. UART is operated in polled
 * mode.
 ****************************************************************************/
void uart_stdio_init(void);

#endif /*_UART_STDIO_H_*/

/** \} */


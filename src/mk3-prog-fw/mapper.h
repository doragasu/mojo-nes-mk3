/************************************************************************//**
 * \file
 * \brief Handles mapper chips. This module acts as a wrapper to the
 *        supported mapper drivers.
 *
 * \defgroup mapper mapper
 * \{
 * \brief Handles mapper chips. This module acts as a wrapper to the
 *        supported mapper drivers.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2017
 ****************************************************************************/
#ifndef _MAPPER_H_
#define _MAPPER_H_

#include <stdint.h>

/// Base address of the RAM chip
#define MAP_RAM_BASE				0x6000

/// RAM chip length in bytes (8 KiB)
#define MAP_RAM_LEN				(8 * 1024)

#define MAP_TABLE(X_MACRO)                  \
    X_MACRO(NOROM,     norom,       0)      \
    X_MACRO(CNROM,     cnrom,       3)      \
    X_MACRO(MMC3,      mmc3,        4)      \
    X_MACRO(113,       113,       113)      \
    X_MACRO(NFROM,     nfrom,     404)

#define MAP_EXPAND_AS_ENUM(u_name, l_name, number)              \
    MAP_ ## u_name,

/// Supported mapper drivers, to use with MapInit().
typedef enum {
    MAP_TABLE(MAP_EXPAND_AS_ENUM)
    MAP_MAX
}  MapDriver;

/************************************************************************//**
 * Mapper initialization. Must be called, specifying one of the supported
 * chips, before calling other function modules.
 *
 * \param[in] drv Mapper to configure.
 *
 * \return 0 if success, -1 if mapper is not supported.
 ****************************************************************************/
int MapInit(uint16_t number);

/************************************************************************//**
 * Configure mapper to access specified chip (PRG/CHR ROM) and address.
 * Returns the CPU/PPU bus 16-bit address that must be used once this
 * function returns, to access the input address.
 *
 * \param[in] chip ROM chip (PRG/CHR) to access.
 * \param[in] addr Memory address to access.
 *
 * \return CPU/PPU address that must be used to access addr on chip.
 ****************************************************************************/
extern uint16_t (*MapPrepareAccess)(uint8_t chip, uint32_t addr);

/************************************************************************//**
 * Write data to RAM.
 *
 * \param[in] offset Offset in the data chip.
 * \param[in] len    Byte length of the data array to write to RAM.
 * \param[in] data   Byte array to write to RAM.
 *
 * \return 0 if success, -1 if range to write is not valid.
 *
 * \note offset is not the RAM address, but the offset inside the RAM bank.
 ****************************************************************************/
int MapRamWrite(uint16_t offset, uint16_t len, uint8_t data[]);

/************************************************************************//**
 * Read data from RAM.
 *
 * \param[in] offset Offset in the data chip.
 * \param[in] len    Byte length of the data array to read from RAM.
 * \param[out] data  Byte array to read from RAM.
 *
 * \return 0 if success, -1 if range to write is not valid.
 *
 * \note offset is not the RAM address, but the offset inside the RAM bank.
 ****************************************************************************/
int MapRamRead(uint16_t offset, uint16_t len, uint8_t data[]);

#endif /*_MAPPER_H_*/
/** \} */


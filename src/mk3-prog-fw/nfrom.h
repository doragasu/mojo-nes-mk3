/************************************************************************//**
 * \file
 * \brief This module allows writing to NFROM registers, e.g. to set bank
 *        addressing.
 *
 * \defgroup tkrom tkrom
 * \{
 * \brief This module allows writing to NFROM registers, e.g. to set bank
 *        addressing.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2017
 ****************************************************************************/
#ifndef _NFROM_H_
#define _NFROM_H_

#include <stdint.h>

/// Memory mapped registers supported by the mapper.
typedef enum {
	NFR_REG_CHR_LOW = 0,	///< CHR ROM bank, low byte.
	NFR_REG_CHR_HIGH,		///< CHR ROM bank, high bits.
	NFR_REG_PRG,			///< PRG ROM bank.
	NFR_REG_MIRR_RAM_PROT,	///< Mirroring and RAM protection configuration.
	NFR_REG_IRQ_LATCH,		///< IRQ counter latch.
	NFR_REG_IRQ_REL,		///< IRQ reload.
	NFR_REG_IRQ_DIS,		///< IRQ disable.
	NFR_REG_IRQ_ENA,		///< IRQ enable.
	NFR_NUM_REGS			///< Number of memory mapped registers.
} NfrReg;

/** \addtogroup NfrMirrRamCfg
 *  \brief Mirroring and RAM options for NFR_REG_MIRR_RAM_PROT register.
 *  \{ */
/// Vertical mirroring
#define NFR_REG_MIRR_RAM_PROT_VERTICAL			0x00
/// Horizontal mirroring
#define NFR_REG_MIRR_RAM_PROT_HORIZONTAL		0x01
/// Enable RAM chip
#define NFR_REG_MIRR_RAM_PROT_CHIP_ENABLE		0x80
/// Disable RAM chip
#define NFR_REG_MIRR_RAM_PROT_CHIP_DISABLE		0x00
/// Allow writes to RAM chip
#define NFR_REG_MIRR_RAM_PROT_ALLOW_WRITE		0x00
/// Prohibit writes to RAM chip
#define NFR_REG_MIRR_RAM_PROT_DENY_WRITE		0x40
/** \} */

/************************************************************************//**
 * Module initialization. Call before using other module functions.
 ****************************************************************************/
void NfrInit(void);

/************************************************************************//**
 * Prepare access to NFROM ROM/Flash chip address.
 *
 * \param[in] chip ROM/Flash chip to access.
 * \param[in] addr Address of the chip to access.
 *
 * \return CPU/PPU address to use for access to the requested chip/address.
 ****************************************************************************/
uint16_t NfrPrepareAccess(uint8_t chip, uint32_t addr);

/************************************************************************//**
 * Write data to specified mapper register.
 *
 * \param[in] reg  Register number to write to.
 * \param[in] data Data to write to specified register.
 ****************************************************************************/
void NfrMapperWrite(NfrReg reg, uint8_t data);

#endif /*_NFROM_H_*/
/** \} */



/************************************************************************//**
 * \file
 * \brief System state machine. Receives system events and performs the
 *        corresponding actions.
 *
 * \defgroup system sys_fsm 
 * \{
 * \brief System state machine. Receives system events and performs the
 *        corresponding actions.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2016
 ****************************************************************************/

#ifndef _SYS_FSM_H_
#define _SYS_FSM_H_

#include <stdint.h>
#include "Descriptors.h"
#include "util.h"
#include "timers.h"
#include "cart_if.h"

/** \addtogroup SysRevVals
 *  \brief Return values of some functions.
 *  \{ */
/// OK
#define SYS_OK		 0
/// Error
#define SYS_ERROR	-1
/** \} */

/** \addtogroup SysFwVer
 *  \brief Programmer firmware version.
 *  \{ */
/// Major version
#define SYS_FW_VER_MAJOR	0
/// Minor version
#define SYS_FW_VER_MINOR	3
/** \} */

/// Supported system events.
typedef enum {
    SYS_EVT_NONE = 0,	///< No event
    SYS_EVT_DIN,		///< Received data from host
    SYS_EVT_CIN,		///< Cartridge has been inserted
    SYS_EVT_COUT,		///< Cartridge has been removed
    SYS_EVT_TIMER,		///< Timer event
    SYS_EVT_USB_ATT,	///< USB attached
    SYS_EVT_USB_DET,	///< USB detached
    SYS_EVT_USB_ERR,	///< USB error
    SYS_EVT_SW_PRESS,	///< Switch press
    SYS_EVT_SW_REL,		///< Switch releasae
    SYS_EVT_NUM_EVENTS	///< Number of events
} SysEvt;

/// Supported system commands. Also includes OK and error codes.
typedef enum {
    SYS_CMD_OK = 0,				///< OK reply code
    SYS_CMD_FW_VER,				///< Get programmer firmware version
    SYS_CMD_BOOTLOADER,         ///< Enter bootloader mode
    SYS_CMD_CHR_WRITE,			///< Write to CHR flash
    SYS_CMD_PRG_WRITE,			///< Write to PRG flash
    SYS_CMD_CHR_READ,			///< Read from CHR flash
    SYS_CMD_PRG_READ,			///< Read from PRG flash
    SYS_CMD_CHR_ERASE,			///< Erase CHR flash (entire or sectors)
    SYS_CMD_PRG_ERASE,			///< Erase PRG flash (entire or sectors)
    SYS_CMD_FLASH_ID,			///< Get flash chips identifiers
    SYS_CMD_RAM_WRITE,  		///< Write data to cartridge SRAM
    SYS_CMD_RAM_READ,			///< Read data from cartridge SRAM
    SYS_CMD_MAPPER_SET, 		///< Configure cartridge mapper
    SYS_CMD_CHR_RANGE_ERASE,	///< Erase a memory range from CHR flash
    SYS_CMD_PRG_RANGE_ERASE,	///< Erase a memory range from PRG flash
    SYS_CMD_ERROR = 255			///< Error reply code
} SysCmdCode;

/// Allowed system states
typedef enum {
    SYS_IDLE,		///< Idle state, cartridge not inserted
    SYS_STAB_WAIT,	///< Wait until cart/USB stabilizes
    SYS_CART_INIT,	///< Initialize cartridge (obtain cart info)
    SYS_READY,		///< System ready to parse host commands
    SYS_ST_MAX
} SysStat;

enum {
    SYS_SW_PRESSED = 0x01,	///< If bit set, button is pressed.
    SYS_SW_EVENT   = 0x02	///< If bit set, a button event occurred.
};

/// Auxiliar flags to define system status
typedef union {
    uint8_t all;				///< Access to all flags
    struct {
        uint8_t cart_in:1;		///< Cartridge inserted
        uint8_t usb_ready:1;	///< USB attached and ready
    };
} SysFlags;

///  Data describing the cartridge flash chip.
typedef struct {
    uint8_t	manId;		///< Chip manufacturer ID
    uint8_t	devId[3];	///< Chip device ID
} SysFlashData;

/// Data about the currently running system instance.
typedef struct {
    SysStat s;			///< Current system status
    SysFlags f;			///< System status flags
    SysFlashData fc[2];	///< Flash chip data (for the 2 chips)
    uint8_t sw;			///< Switch (pushbutton) status
} SysInstance;

/// Command header for memory read and write
typedef struct {
    uint8_t cmd;		///< Command code
    uint8_t addr[3];	///< Address to read/write
    uint8_t len[2];		///< Length to read/write
} SysCmdRdWrHdr;

/// Command header for erase commands.
typedef struct {
    uint8_t cmd;			///< Command code
    uint8_t sectAddr[3];	///< Address to erase, Full chip if 0xFFFFFF
} SysCmdErase;

/// Command header for range erase commands
typedef struct {
    uint8_t cmd;        ///< Command code
    uint8_t start[3];   ///< Initial address
    uint8_t len[3];     ///< Memory range length
    uint8_t pad;        ///< Unused
} SysRangeErase;

/// Command header for mapper sec command
typedef struct {
    uint8_t cmd;
    uint8_t pad;
    uint16_t id;
} SysMapperSet;

/// Generic command request.
typedef union {
    uint8_t command;			///< Command code
    SysCmdRdWrHdr rdWr;			///< Read/write request
    SysCmdErase erase;			///< Erase request
    SysMapperSet mapper;		///< Set mapper
    SysRangeErase rangeErase;	///< Erase memory range
} SysCmd;

/// Flash chip identification information.
typedef struct {
    uint8_t manId;			///< Manufacturer ID
    uint8_t devId[3];		///< Device ID
} SysFlashId;

/// Empty command response.
typedef struct {
    uint8_t code;			///< Response code (OK/ERROR)
} SysRepEmpty;

/// Firmware version command response.
typedef struct {
    uint8_t code;			///< Response code (OK/ERROR)
    uint8_t ver_major;		///< Major version number
    uint8_t ver_minor;		///< Minor version number
} SysRepFwVer;

/// Flash ID command response.
typedef struct {
    uint8_t code;			///< Command code
    uint8_t pad;			///< Padding
    SysFlashId prg;			///< PRG Flash information
    SysFlashId chr;			///< CHR Flash information
} SysRepFlashId;

/// Generic reply to a command request.
typedef union {
    uint8_t command;		///< Command code
    SysRepEmpty eRep;		///< Empty command response
    SysRepFlashId fId;		///< Flash ID command response
    SysRepFwVer fwVer;		///< Firmware version command response
} SysRep;

/// Buffer definition for commands and replies
typedef union {
    ///< Raw data (up to 32 bytes)
    uint8_t data[MAX(VENDOR_O_EPSIZE, VENDOR_I_EPSIZE)];
    SysCmd cmd;
    SysRep rep;
} SysBuf;

/************************************************************************//**
 * \brief Module initialization. Must be called before using any other
 * function from this module.
 ****************************************************************************/
void SysInit(void);

/************************************************************************//**
 * \brief Takes an incoming event and executes a cycle of the system FSM
 *
 * \param[in] evt Incoming event to be processed.
 *
 * \note Lots of states have been removed, might be needed if problems arise
 * because USB_USBTask() needs to be serviced more often than it is with
 * the current implementation.
 ****************************************************************************/
void SysFsmCycle(SysEvt evt);

/// Returns TRUE if a SF_EVT_TIMER event must be noticed to the FSM
#define SysEvtTimerNotify()	Timer1Ovfw()

#endif /*_SYS_FSM_H_*/

/** \} */


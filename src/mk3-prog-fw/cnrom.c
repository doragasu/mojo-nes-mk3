#include "cnrom.h"

#include "cart_if.h"
#include "util.h"

/// Number of bits of each CHR bank
#define CNROM_CHR_BITS	13
/// Number of bits of each PRG bank
#define CNROM_PRG_BITS	15

/// Address mask for CHR banks
#define CNROM_CHR_ADDR_MASK	((1U<<CNROM_CHR_BITS) - 1)
/// Address mask for CHR banks
#define CNROM_PRG_ADDR_MASK	((1U<<CNROM_PRG_BITS) - 1)

static uint8_t shadow;

static void CnromWrite(uint8_t data) {
	shadow = data;

    dprintf("[CNROM] Write 0x%02X\n", data);
	CIF_ADDRL_PORT = (uint8_t)CNROM_REG_ADDR;
	CIF_ADDRH_PORT = (CIF_ADDRH_PORT & (~CIF_CPU_ADDRH_MASK)) |
					 (CNROM_REG_ADDR>>8);
	// Write data to bus
	CIF_DATA_PORT = data;
	CIF_DATA_DDR = 0xFF;
	// Chip is always selected, signal _W and generate clock
	CIF_PIN_SET(CPU_PHI2);
	CIF_PIN_CLR(CPU__PRG);
	CIF_PIN_CLR(CPU__W);
	
	// MMC3X write will be latched on clock falling edge
	CIF_PIN_CLR(CPU_PHI2);
	CIF_PIN_SET(CPU__W);
	CIF_PIN_SET(CPU__PRG);
	// Remove data from bus
	CIF_DATA_DDR  = 0;
	CIF_DATA_PORT = 0xFF;
}

void CnromInit(void)
{
    // Select bank 0 for CHR and PRG
    CnromWrite(0);
}

uint16_t CnromPrepareAccess(uint8_t chip, uint32_t addr)
{
    uint16_t chr_addr;
    uint8_t bank;

    // This mapper does not support PRG ROM bankswitching
    if (CIF_CHIP_PRG == chip) {
        return addr & CNROM_PRG_ADDR_MASK;
    }

    bank = addr>>CNROM_CHR_BITS;
    chr_addr = addr & CNROM_CHR_ADDR_MASK;

    if (bank != shadow) {
        CnromWrite(bank);
    }

    return chr_addr;
}

void CnromMapperWrite(uint8_t data)
{
    if (shadow != data) {
        CnromWrite(data);
    }
}



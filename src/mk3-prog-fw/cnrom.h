/************************************************************************//**
 * \file
 * \brief This module allows writing to cartridges implementing mapper 113
 *
 * \defgroup cnrom cnrom
 * \{
 * \brief This module allows writing to cartridges implementing mapper 113
 *
 * \author Jesús Alonso (doragasu)
 * \date   2018
 ****************************************************************************/
#ifndef _CNROM_H_
#define _CNROM_H_

#include <stdint.h>

/// Address of the mapper register, in range 0x8000 ~ 0xFFFF
#define CNROM_REG_ADDR 0x8000

/************************************************************************//**
 * Module initialization. Call before using other module functions.
 ****************************************************************************/
void CnromInit(void);

/************************************************************************//**
 * Prepare access to mapper 113 ROM/Flash chip address.
 *
 * \param[in] chip ROM/Flash chip to access.
 * \param[in] addr Address of the chip to access.
 *
 * \return CPU/PPU address to use for access to the requested chip/address.
 ****************************************************************************/
uint16_t CnromPrepareAccess(uint8_t chip, uint32_t addr);

/************************************************************************//**
 * Write data to the mapper register.
 *
 * \param[in] data Data to write to specified register.
 ****************************************************************************/
void CnromMapperWrite(uint8_t data);

#endif /*_CNROM_H_*/
/** \} */



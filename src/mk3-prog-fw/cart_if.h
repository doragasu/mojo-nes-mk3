/************************************************************************//**
 * \file
 * \brief Contains some definitions to use some special
 *  cart lines, not used by the Flash chip interface.
 *
 * \defgroup cart_if cart_if
 * \{
 * \brief Contains some definitions to use some special
 *  cart lines, not used by the Flash chip interface.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2016
 ****************************************************************************/

#ifndef _CART_IF_H_
#define _CART_IF_H_

#include <stdint.h>
#include <avr/io.h>
#include <avr/cpufunc.h>

/// 6502 PHI2 clock
#define CIF_CPU_PHI2(ACTION_MACRO)  ACTION_MACRO(E, 0)
/// Access to program ROM (active low)
#define CIF_CPU__PRG(ACTION_MACRO)  ACTION_MACRO(E, 6)
/// CPU write (active low)
#define CIF_CPU__W(ACTION_MACRO)    ACTION_MACRO(E, 7)
/// PRG Flash write enable
#define CIF_CPU__POWE(ACTION_MACRO) ACTION_MACRO(B, 6)
/// PPU write enable (active low)
#define CIF_PPU__W(ACTION_MACRO)    ACTION_MACRO(E, 3)
/// PPU read enable (active low)
#define CIF_PPU__R(ACTION_MACRO)    ACTION_MACRO(B, 0)
/// Cartridge detection (active low)
#define CIF__CDET(ACTION_MACRO)     ACTION_MACRO(E, 4)
/// Hardware version bit 2
#define CIF_VER2(ACTION_MACRO)      ACTION_MACRO(E, 2)
/// Hardware version bit 1
#define CIF_VER1(ACTION_MACRO)      ACTION_MACRO(E, 1)
/// Hardware version bit 0
#define CIF_VER0(ACTION_MACRO)      ACTION_MACRO(C, 7)

/// Set output by letter and number
#define CIF_SET(letter, number) do {            \
    PORT ## letter |=  (1<<number);} while(0)

/// Set output by pin name
#define CIF_PIN_SET(pin) CIF_ ## pin(CIF_SET)

/// Clear output by letter and number
#define CIF_CLR(letter, number) do {            \
    PORT ## letter &= ~(1<<number);} while(0)

/// Clear output by pin name
#define CIF_PIN_CLR(pin) CIF_ ## pin(CIF_CLR)

/// Read input by letter and number, do not shift pin
#define CIF_GET(letter, number)                 \
    (PIN ## letter & (1<<number))

/// Read input by pin name, do not shift pin
#define CIF_PIN_GET(pin) CIF_ ## pin(CIF_GET)

/// Read input by letter and number, shifted to bit 0
#define CIF_GET_BOOL(letter, number)            \
    ((PIN ## letter & (1<<number)) != 0)

/// Read input by pin name, shifted to bit 0
#define CIF_PIN_GET_BOOL(pin) CIF_ ## pin(CIF_GET_BOOL)

/// Configure pin as output, by letter and number
#define CIF_PIN_OUT(letter, number) do {        \
    DDR ## letter |=  (1<<number);} while(0)

/// Configure pin as input, by letter and number
#define CIF_PIN_IN(letter, number) do {         \
    DDR ## letter &= ~(1<<number);} while(0)

/// Enable input pull-up, by letter and number
#define CIF_PIN_PUEN(letter, number) do {       \
    PORT ## letter |=  (1<<number);} while(0)

/// Disable input pull-up, by letter and number
#define CIF_PIN_PUDIS(letter, number) do {      \
    PORT ## letter &= ~(1<<number);} while(0)

/// Chip selection to use for flash operations.
enum {
    CIF_CHIP_PRG = 0,
    CIF_CHIP_CHR
};

/// Letter used for the address bus, high (8~14) lines lines
#define CIF_ADDRH			C
/// Letter used for the address bus, lower (0~7) lines
#define CIF_ADDRL			A


/// Mask used for the CPU high addr lines
#define CIF_CPU_ADDRH_MASK	0x7F
/// Mask used for the PPU upper lines
#define CIF_PPU_ADDRH_MASK	0x3F
/// Only CPU address lines A0 ~ A12 are directly wired to flash
#define CIF_CPU_FLASH_ADDRH_MASK	0x1F
/// Only PPU address lines A0 ~ A9 are directly wired to flash
#define CIF_PPU_FLASH_ADDRH_MASK	0x03	

/// Letter used for the data bus
#define CIF_DATA			F


/*
 * DO NOT CUSTOMIZE ANYTHING BEYOND THIS POINT.
 */

/// Helper macro to construct port registers, second level
#define CIF_REG2(letter, reg)	(reg ## letter)
/// Helper macro to construct port registers
#define CIF_REG(letter, reg) CIF_REG2(letter, reg)

/// Address port, high bits
#define CIF_ADDRH_PORT	CIF_REG(CIF_ADDRH, PORT)
/// Address port, low bits
#define CIF_ADDRL_PORT	CIF_REG(CIF_ADDRL, PORT)

/// Address data directon register, high bits
#define CIF_ADDRH_DDR		CIF_REG(CIF_ADDRH, DDR)
/// Address data directon register, low bits
#define CIF_ADDRL_DDR		CIF_REG(CIF_ADDRL, DDR)

/// Address pin register, high bits
#define CIF_ADDRH_PIN		CIF_REG(CIF_ADDRH, PIN)
/// Address pin register, low bits
#define CIF_ADDRL_PIN		CIF_REG(CIF_ADDRL, PIN)

/// Data port
#define CIF_DATA_PORT		CIF_REG(CIF_DATA, PORT)

/// Data data direction register
#define CIF_DATA_DDR		CIF_REG(CIF_DATA, DDR)

/// Data pin register
#define CIF_DATA_PIN		CIF_REG(CIF_DATA, PIN)

/************************************************************************//**
 * \brief Initializes the module. Must be called before using any other
 * macro.
 ****************************************************************************/
static inline void CifInit(void) {
    uint8_t tmp;

	// Ensure JTAG interface is disabled to allow using PF[4~7] GPIO pins
    // Also make sure pull-ups are not disabled
    tmp = MCUCR | (1<<JTD);
    tmp &= ~(1<<PUD);
    MCUCR = tmp;	// JTAG disable sequence requieres writing the
    MCUCR = tmp;	// disable bit twice withing 4 clock cycles.

    // Configure control lines, and set outputs to inactive
    CIF_CPU__PRG(CIF_PIN_OUT);
    CIF_CPU_PHI2(CIF_PIN_OUT);
    CIF_CPU__POWE(CIF_PIN_OUT);
    CIF_CPU__W(CIF_PIN_OUT);
    CIF_PPU__W(CIF_PIN_OUT);
    CIF_PPU__R(CIF_PIN_OUT);
    CIF_PIN_SET(CPU__PRG);
    CIF_PIN_SET(CPU__POWE);
    CIF_PIN_SET(CPU__W);
    CIF_PIN_SET(PPU__W);
    CIF_PIN_SET(PPU__R);
    CIF_PIN_CLR(CPU_PHI2);

	// Configure address registers as output, and set to 0x7FFF
	CIF_ADDRL_DDR = 0xFF;
	CIF_ADDRH_DDR  |= CIF_CPU_ADDRH_MASK;
	CIF_ADDRL_PORT = 0xFF;
	CIF_ADDRH_PORT |= CIF_CPU_ADDRH_MASK;

	// Configure data register as input (with pull-ups enabled)
	CIF_DATA_DDR  = 0;
	CIF_DATA_PORT = 0xFF;

	// Enable internal pull-up for CIF__CDET
//    CIF__CDET(CIF_PIN_IN);
    CIF__CDET(CIF_PIN_PUEN);

	/// \todo Configure unused pins as inputs with pullups enabled
}

/************************************************************************//**
 * \brief Writes a byte to specified address.
 *
 * \param[in] chip The PRG/CHR chip identifier.
 * \param[in] addr Address to which data will be written.
 * \param[in] data Data to write to addr address.
 *
 * \note Do not mistake this function with the program ones.
 * \note CHR writes will not work unless A13 is 0.
 ****************************************************************************/
static inline void CifWrite(uint8_t chip, uint16_t addr, uint8_t data) {
	// Put address on the bus
	CIF_ADDRL_PORT = addr;
	CIF_ADDRH_PORT = (CIF_ADDRH_PORT & (~CIF_CPU_ADDRH_MASK)) |
					 ((addr>>8) & CIF_CPU_ADDRH_MASK);
	// Write data to bus
	CIF_DATA_PORT = data;
	CIF_DATA_DDR = 0xFF;
	// Chip is always selected, signal _W
	if (CIF_CHIP_PRG == chip) {
		// Set PHI2 for data transceivers to be enabled
        CIF_PIN_SET(CPU_PHI2);
		// #W is cleared to avoid Flash chip #OE being generated
        CIF_PIN_CLR(CPU__W);
        CIF_PIN_CLR(CPU__PRG);
        CIF_PIN_CLR(CPU__POWE);

        CIF_PIN_SET(CPU__POWE);
        CIF_PIN_SET(CPU__PRG);
        CIF_PIN_SET(CPU__W);
        CIF_PIN_CLR(CPU_PHI2);

	
	} else {
        // CPU__W must be enabled for the data transceivers to set the
        // right direction
        CIF_PIN_CLR(CPU__W);
        CIF_PIN_CLR(PPU__W);
    
        CIF_PIN_SET(PPU__W);
        CIF_PIN_SET(CPU__W);
	}
	// Remove data from bus
	CIF_DATA_DDR  = 0;
	CIF_DATA_PORT = 0xFF;
}

/************************************************************************//**
 * \brief Reads a byte from the specified address.
 *
 * \param[in] chip The PRG/CHR chip identifier.
 * \param[in] addr Address that will be read.
 *
 * \return Readed byte.
 * \note CHR reads will not work unless A13 is 0.
 ****************************************************************************/
static inline uint8_t CifRead(uint8_t chip, uint16_t addr) {
	uint8_t data;

	// Put address on the bus
	CIF_ADDRL_PORT = addr;
	CIF_ADDRH_PORT = (CIF_ADDRH_PORT & (~CIF_CPU_ADDRH_MASK)) |
					 ((addr>>8) & CIF_CPU_ADDRH_MASK);
	//CIF_DATA_PORT = 0x00;
	if (CIF_CHIP_PRG == chip) {
        CIF_PIN_SET(CPU_PHI2);
        CIF_PIN_CLR(CPU__PRG);
	}
	else {
        CIF_PIN_CLR(PPU__R);
	}
	// Read data
	_NOP();		// Insert NOPs to ensure the input sinchronizer gets the data
	_NOP();
	data = CIF_DATA_PIN;
	if (CIF_CHIP_PRG == chip) {
        CIF_PIN_SET(CPU__PRG);
        CIF_PIN_CLR(CPU_PHI2);
	}
	else {
        CIF_PIN_SET(PPU__R);
	}
	//CIF_DATA_PORT = 0xFF;
	
	return data;
}

/// Returns the 3-bit hardware revision identifier
#define CifVerGet()     ((CIF_PIN_GET_BOOL(VER2)<<2) | \
        (CIF_PIN_GET_BOOL(VER1)<<1) | CIF_PIN_GET_BOOL(VER0))

/// Returns the cart insertion status. TRUE if cart is inserted.
#define CifCartIn()		(CIF_PIN_GET(_CDET) == 0)

#endif //_CART_IF_H_

/** \} */


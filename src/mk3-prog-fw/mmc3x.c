/************************************************************************//**
 * \file
 * \brief This module allows writing to MMC3X registers, e.g. to set bank
 *        addressing.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#include "mmc3x.h"

/** \addtogroup mmc3x
 *  \brief CPU addresses of the memory mapped registers.
 *  \note Bit 15 is always one in these registers, and has been ommited on
 *        purpose, as write routine will lower #PRG pin.
 *  \{ */
/// Bank select register ($8000)
#define MMC3X_REG_BANK_SEL_ADDR		0x0000
/// Bank data register ($8001)
#define MMC3X_REG_BANK_DATA_ADDR	0x0001
/// Mirroring selection ($A000)
#define MMC3X_REG_MIRRORING_ADDR	0x2000
/// RAM protection register ($A001)
#define MMC3X_REG_RAM_PROT_ADDR		0x2001
/// IRQ latch register ($C000)
#define MMC3X_REG_IRQ_LATCH_ADDR	0x4000
/// IRQ reload ($C001)
#define MMC3X_REG_IRQ_REL_ADDR		0x4001
/// IRQ disable ($E000)
#define MMC3X_REG_IRQ_DIS_ADDR		0x6000
/// IRQ enable ($E001)
#define MMC3X_REG_IRQ_ENA_ADDR		0x6001
/** \} */

/// Macro to ease building the contents of the MMC3X_REG_BANK_SEL register.
#define MMC3X_REG_BANK_SEL_RMK(bankReg, romMode, a12Inv, bank)	\
		(((bankReg) & 7) | ((romMode)<<6) | ((a12Inv)<<7) |		\
		(((bank)>>5) & 0x38))

/// Computes MMC3X address depending on the chip to write (PRG/CHR ROM),
/// using R2 for the CHR ROM writes and R6 for the PRG ROM writes.
#define MMC3X_FLASH_ADDR(chip, addr) (								 	\
		(chip) == CIF_CHIP_PRG?((addr) & 0x1FFF) | 0x8000:				\
		((addr) & 0x03FF) | 0x1000										\
)

/// Obtain the flash bank length corresponding to input chip
#define MMC3X_FLASH_BANK(chip)	((chip)==CIF_CHIP_PRG?6:2)

/// Register CPU memory addresses array
const uint16_t mmc3xReg[MMC3X_REG_NUM_REGS] = {
	MMC3X_REG_BANK_SEL_ADDR,  MMC3X_REG_BANK_DATA_ADDR,
	MMC3X_REG_MIRRORING_ADDR, MMC3X_REG_RAM_PROT_ADDR,
	MMC3X_REG_IRQ_LATCH_ADDR, MMC3X_REG_IRQ_REL_ADDR,
	MMC3X_REG_IRQ_DIS_ADDR,   MMC3X_REG_IRQ_ENA_ADDR
};

/// Shadow copy of the MMC3X bank select registers
static uint16_t mmc3xBankShadow[8];

/************************************************************************//**
 * Write to specified MMC3X memory mapped register.
 *
 * \param[in] reg MMC3X memory mapped register that will be written.
 * \param[in] data 8-bit data to write to specified register.
 *
 * \note Function does NOT check reg is valid. Failing to provide a correct
 *       register number will cause writes to arbitrary memory addresses.
 *       Also does not use register shadow bank, it is recommended that the
 *       application uses Mmc3xBankConf() instead.
 ****************************************************************************/
static inline void Mmc3xWrite(Mmc3xReg reg, uint8_t data) {
	// Put address on the bus
	uint16_t addr = mmc3xReg[reg];

	CIF_ADDRL_PORT = addr;
	CIF_ADDRH_PORT = (CIF_ADDRH_PORT & (~CIF_CPU_ADDRH_MASK)) |
					 (addr>>8);
	// Write data to bus
	CIF_DATA_PORT = data;
	CIF_DATA_DDR = 0xFF;
	// Chip is always selected, signal _W and generate clock
	CIF_PIN_SET(CPU_PHI2);
	CIF_PIN_CLR(CPU__PRG);
	CIF_PIN_CLR(CPU__W);
	
	// MMC3X write will be latched on clock falling edge
	CIF_PIN_CLR(CPU_PHI2);
	CIF_PIN_SET(CPU__W);
	CIF_PIN_SET(CPU__PRG);
	// Remove data from bus
	CIF_DATA_DDR  = 0;
	CIF_DATA_PORT = 0xFF;
}

void Mmc3xInit(void) {
	uint8_t i;

	// Set bank select registers shadow copy to "impossible" value to
	// force a shadow copy register update on next write.
	/// \todo set sane default values at least for RAM read/write
	for (i = 0; i < 8; i++) mmc3xBankShadow[i] = 0xFFFF;
	// Configure mmc3x banks 0 and 1 to ease CHR flash programming (so bits
	// A10 and A11 applied to CHR flash match the ones of the supplied addr).
	Mmc3xBankConf(0, 0x0000);
	Mmc3xBankConf(1, 0x0800);

    // Enable RAM chip and disable RAM protection
    // NOTE: Maybe this should be done in RAM read/write routines, but I
    // think it does not hurt having it here
    Mmc3xWrite(MMC3X_REG_RAM_PROT, MMC3X_REG_RAM_PROT_CHIP_ENABLE |
            MMC3X_REG_RAM_PROT_ALLOW_WRITE);
}

void Mmc3xBankConf(uint8_t bankReg, uint32_t addr) {
	uint16_t bank;
	// Calculate register bank value
	bank = addr>>(bankReg<6?10:13);
	if (bank != mmc3xBankShadow[bankReg]) {
		// Set bank configuration register
		Mmc3xWrite(MMC3X_REG_BANK_SEL, MMC3X_REG_BANK_SEL_RMK(bankReg,
					MMC3X_ROM_MODE_LOW_SWAPPABLE, MMC3X_A12INV_OFF, bank));
		// Write Bank data register
		Mmc3xWrite(MMC3X_REG_BANK_DATA, bank);
		// Update register shadow copy
		mmc3xBankShadow[bankReg] = bank;
	}
}

uint16_t Mmc3xPrepareAccess(uint8_t chip, uint32_t addr) {
	Mmc3xBankConf(MMC3X_FLASH_BANK(chip), addr);
	return MMC3X_FLASH_ADDR(chip, addr);
}


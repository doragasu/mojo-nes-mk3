/************************************************************************//**
 * \file
 * \brief This module allows writing to cartridges implementing mapper 113
 *
 * \author Jesús Alonso (doragasu)
 * \date   2018
 ****************************************************************************/
#include "map113.h"
#include "cart_if.h"
#include "util.h"

/// Number of bits of each CHR bank
#define MAP113_CHR_BITS	13
/// Number of bits of each PRG bank
#define MAP113_PRG_BITS	15

/// Address mask for CHR banks
#define MAP113_CHR_ADDR_MASK	((1U<<MAP113_CHR_BITS) - 1)
/// Address mask for PRG banks
#define MAP113_PRG_ADDR_MASK	((1U<<MAP113_PRG_BITS) - 1)
static uint8_t shadow;

static void Map113Write(uint8_t data) {
	shadow = data;

    dprintf("[MAP113] Write 0x%02X\n", data);
	CIF_ADDRL_PORT = (uint8_t)MAP113_REG_ADDR;
	CIF_ADDRH_PORT = (CIF_ADDRH_PORT & (~CIF_CPU_ADDRH_MASK)) |
					 (MAP113_REG_ADDR>>8);
	// Write data to bus
	CIF_DATA_PORT = data;
	CIF_DATA_DDR = 0xFF;
	// Chip is always selected, signal _W and generate clock
	CIF_PIN_SET(CPU_PHI2);
	CIF_PIN_SET(CPU__PRG);
	CIF_PIN_CLR(CPU__W);
	
	// MMC3X write will be latched on clock falling edge
	CIF_PIN_CLR(CPU_PHI2);
	CIF_PIN_SET(CPU__W);
	CIF_PIN_CLR(CPU__PRG);
	// Remove data from bus
	CIF_DATA_DDR  = 0;
	CIF_DATA_PORT = 0xFF;
	CIF_PIN_SET(CPU__PRG);
}

void Map113Init(void)
{
    // Select bank 0 for CHR and PRG
    Map113Write(0);
}

uint16_t Map113PrepareAccess(uint8_t chip, uint32_t addr)
{
    uint16_t cpu_addr;
    uint8_t bank;
    uint8_t reg = shadow;

    // Register format: MCPP PCCC
	if (CIF_CHIP_CHR == chip) {
        bank = (addr>>MAP113_CHR_BITS) & 0xF;
        reg &= 0x38;
        reg |= (bank & 0x8)<<3;
        reg |= (bank & 0x7);
        cpu_addr = addr & MAP113_CHR_ADDR_MASK;
	} else {
        bank = (addr>>MAP113_PRG_BITS) & 0x7;
        reg &= 0x47;
        reg |= bank<<3;
        cpu_addr = addr & MAP113_PRG_ADDR_MASK;
	}

    if (reg != shadow) {
        Map113Write(reg);
        dprintf("[MAP113] chip %d, addr 0x%06lX\n", chip, addr);
        dprintf("[MAP113] bank %d, cpu_addr %04X\n", bank, cpu_addr);
        dprintf("[MAP113] W 0x%02X\n", reg);
    }

    return cpu_addr;
}

void Map113MapperWrite(uint8_t data)
{
    if (shadow != data) {
        Map113Write(data);
    }
}


/************************************************************************//**
 * \file
 * \brief This module allows to manage (mainly read and write) from flash
 *        memory chips such as S29GL064.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2016
 ****************************************************************************/

#include "flash.h"
#include "util.h"
#include "mapper.h"

/// Obtains a sector address from an address. Current chip does not actually
//require any calculations to get SA.
/// \todo Support several flash chips
#define FLASH_SA_GET(addr)		(addr)

/// Number of shifts for addresses of saddr array
#define FLASH_SADDR_SHIFT	8

/// Top address of the Flash chip, plus 1, shifted FLASH_SADDR_SHIFT times.
#define FLASH_SADDR_MAX		(FLASH_CHIP_LENGTH>>FLASH_SADDR_SHIFT)

/// Sector word addresses, shifted FLASH_SADDR_SHIFTS times to the right
/// Note not all the sectors are the same length (depending on top boot
/// or bottom boot flash configuration).
/// \warning This is not adapted to 64 MiB chips!!!!!
const static uint16_t saddr[] = {
    // 8 MiB, Bottom Boot (S29GL064N04)
    0x0000, 0x0020, 0x0040, 0x0060, 0x0080, 0x00A0, 0x00C0, 0x00E0,
    0x0100, 0x0200, 0x0300, 0x0400, 0x0500, 0x0600, 0x0700, 0x0800,
    0x0900, 0x0A00, 0x0B00, 0x0C00, 0x0D00, 0x0E00, 0x0F00, 0x1000,
    0x1100, 0x1200, 0x1300, 0x1400, 0x1500, 0x1600, 0x1700, 0x1800,
    0x1900, 0x1A00, 0x1B00, 0x1C00, 0x1D00, 0x1E00, 0x1F00, 0x2000,
    0x2100, 0x2200, 0x2300, 0x2400, 0x2500, 0x2600, 0x2700, 0x2800,
    0x2900, 0x2A00, 0x2B00, 0x2C00, 0x2D00, 0x2E00, 0x2F00, 0x3000,
    0x3100, 0x3200, 0x3300, 0x3400, 0x3500, 0x3600, 0x3700, 0x3800,
    0x3900, 0x3A00, 0x3B00, 0x3C00, 0x3D00, 0x3E00, 0x3F00, 0x4000,
    0x4100, 0x4200, 0x4300, 0x4400, 0x4500, 0x4600, 0x4700, 0x4800,
    0x4900, 0x4A00, 0x4B00, 0x4C00, 0x4D00, 0x4E00, 0x4F00, 0x5000,
    0x5100, 0x5200, 0x5300, 0x5400, 0x5500, 0x5600, 0x5700, 0x5800,
    0x5900, 0x5A00, 0x5B00, 0x5C00, 0x5D00, 0x5E00, 0x5F00, 0x6000,
    0x6100, 0x6200, 0x6300, 0x6400, 0x6500, 0x6600, 0x6700, 0x6800,
    0x6900, 0x6A00, 0x6B00, 0x6C00, 0x6D00, 0x6E00, 0x6F00, 0x7000,
    0x7100, 0x7200, 0x7300, 0x7400, 0x7500, 0x7600, 0x7700, 0x7800,
    0x7900, 0x7A00, 0x7B00, 0x7C00, 0x7D00, 0x7E00, 0x7F00
    // 4 MiB, Top Boot
//	0x0000, 0x0100, 0x0200, 0x0300, 0x0400, 0x0500, 0x0600, 0x0700,
//	0x0800, 0x0900, 0x0A00, 0x0B00, 0x0C00, 0x0D00, 0x0E00, 0x0F00,
//	0x1000, 0x1100, 0x1200, 0x1300, 0x1400, 0x1500, 0x1600, 0x1700,
//	0x1800, 0x1900, 0x1A00, 0x1B00, 0x1C00, 0x1D00, 0x1E00, 0x1F00,
//	0x2000, 0x2100, 0x2200, 0x2300, 0x2400, 0x2500, 0x2600, 0x2700,
//	0x2800, 0x2900, 0x2A00, 0x2B00, 0x2C00, 0x2D00, 0x2E00, 0x2F00,
//	0x3000, 0x3100, 0x3200, 0x3300, 0x3400, 0x3500, 0x3600, 0x3700,
//	0x3800, 0x3900, 0x3A00, 0x3B00, 0x3C00, 0x3D00, 0x3E00, 0x3F00,
//	0x3F20, 0x3F40, 0x3F60, 0x3F80, 0x3FA0, 0x3FC0, 0x3FE0
};

/// Returns the sector number corresponding to address input
#define FLASH_NSECT		ARRAY_LENGTH(saddr)

uint8_t FlashDataPoll(uint8_t chip, uint16_t addr, uint8_t data) {
	uint16_t read;

	// Poll while DQ7 != data(7) and DQ5 == 0 and DQ1 == 0
	do {
		read = CifRead(chip, addr);
	} while (((data ^ read) & 0x80) && ((read & 0x22) == 0));

	// DQ7 must be tested after another read, according to datasheet
//	if (((data ^ read) & 0x80) == 0) return 0;
	read = CifRead(chip, addr);
	if (((data ^ read) & 0x80) == 0) return 0;
	// Data has not been programmed, return with error. If DQ5 is set, also
	// issue a reset command to return to array read mode
	if (read & 0x20) {
		FlashReset(chip);
	}
	// If DQ1 is set, issue the write-to-buffer-abort-reset command.
	if (read & 0x02) {
		FlashUnlock(chip);
		FlashReset(chip);
	}
	return 1;
}

uint8_t FlashErasePoll(uint8_t chip, uint16_t addr) {
	uint16_t read;

	// Wait until DQ7 or DQ5 are set
	do {
		read = CifRead(chip, addr);
	} while (!(read & 0xA0));


	// If DQ5 is set, an error has occurred. Also a reset command needs to
	// be sent to return to array read mode.
	if (!(read & 0x80)) return 0;

	FlashReset(chip);
	return 1;
	//return (read & 0x80) != 0;
}

void FlashInit(void){
	// Configure cartridge interface
	CifInit();
	MapInit(MAP_MMC3);	// TODO: Select proper mapper
}

void FlashIdle(void) {
	// Control signals
	CIF_PIN_SET(CPU_PHI2);
	CIF_PIN_SET(CPU__PRG);
	CIF_PIN_SET(CPU__W);
	CIF_PIN_SET(PPU__W);
	CIF_PIN_SET(PPU__R);
	// Data port (input with pullups enabled)
	CIF_DATA_DDR  = 0;
	CIF_DATA_PORT = 0xFF;
	// Addresses
	CIF_ADDRL_PORT = 0xFF;
	CIF_ADDRH_PORT |= CIF_CPU_ADDRH_MASK;
}

uint8_t FlashGetManId(uint8_t chip) {
	uint16_t retVal;

	// Obtain manufacturer ID and reset interface to return to array read.
	FlashAutoselect(chip);
	retVal = CifRead(chip, FLASH_MANID_RD[0]);
	FlashReset(chip);

	return retVal;
}

void FlashGetDevId(uint8_t chip, uint8_t devId[3]) {
	// Obtain device ID and reset interface to return to array read.
	FlashAutoselect(chip);
	devId[0] = CifRead(chip, FLASH_DEVID_RD[0]);
	devId[1] = CifRead(chip, FLASH_DEVID_RD[1]);
	devId[2] = CifRead(chip, FLASH_DEVID_RD[2]);
	FlashReset(chip);
}

void FlashProg(uint8_t chip, uint32_t addr, uint8_t data) {
	uint8_t i;
	uint16_t cpuAddr;

	// Prepare mapper for write
	cpuAddr = MapPrepareAccess(chip, addr);
	// Unlock and write program command
	FlashUnlock(chip);
	FLASH_WRITE_CMD(chip, FLASH_PROG, i);
	// Write data
	CifWrite(chip, cpuAddr, data);
}

uint8_t FlashWriteBuf(uint8_t chip, uint32_t addr,
					  uint8_t data[], uint8_t bLen) {
	// Sector address
	uint16_t sa;
	// CPU address for writing using the mapper
	uint16_t cpuAddr;
	// Number of bytes to write
	uint8_t bc;
	// Index
	uint8_t i;

	// Check maximum write length
	if (bLen > 32) return 0;

	// Prepare mapper for write
	cpuAddr = MapPrepareAccess(chip, addr);

	// Obtain the sector address (CPU address, using mapper)
	sa = cpuAddr;
	// Compute the number of bytes to write minus 1. Maximum number is 31,
	// but without crossing a write-buffer page
	bc = MIN(bLen, 32 - (addr & 0x1F)) - 1;
	// Unlock and send Write to Buffer command
	FlashUnlock(chip);
	CifWrite(chip, sa, FLASH_WR_BUF[0]);
	// Write word count - 1
	CifWrite(chip, sa, bc);

	// Write data to bufffer
	for (i = 0; i <= bc; i++, cpuAddr++) CifWrite(chip, cpuAddr, data[i]);
	// Write program buffer command
	CifWrite(chip, sa, FLASH_PRG_BUF[0]);
	// Poll until programming is complete
	if (FlashDataPoll(chip, cpuAddr - 1, data[i - 1])) return 0;

	// Return number of elements (bytes) written
	return i;
}

uint8_t FlashReadBuf(uint8_t chip, uint32_t addr,
					 uint8_t data[], uint8_t bLen) {
	// CPU bus address
	uint16_t cpuAddr;
	// Loop control variable
	uint8_t i;

	// Prepare mapper for read
	cpuAddr = MapPrepareAccess(chip, addr);

	for (i = 0; i < bLen; i++, cpuAddr++) {
		data[i] = CifRead(chip, cpuAddr);
	}
	return i;
}

void FlashUnlockBypass(uint8_t chip) {
	uint8_t i;

	FlashUnlock(chip);
	FLASH_WRITE_CMD(chip, FLASH_UL_BYP, i);
}

void FlashUnlockBypassReset(uint8_t chip) {
	// Write reset command. Addresses are don't care
	CifWrite(chip, 0, FLASH_UL_BYP_RST[0]);
	CifWrite(chip, 0, FLASH_UL_BYP_RST[1]);
}

uint8_t FlashChipErase(uint8_t chip) {
	uint8_t i;

	// Unlock and write chip erase sequence
	FlashUnlock(chip);
	FLASH_WRITE_CMD(chip, FLASH_CHIP_ERASE, i);
	// Poll until erase complete.
	return FlashErasePoll(chip, 0);
}

uint8_t FlashSectErase(uint8_t chip, uint32_t addr) {
	// Sector address (CPU relative, using mapper)
	uint16_t sa;
	// Index
	uint8_t i;
	
	// Prepare mapper for erase
	sa = MapPrepareAccess(chip, addr);

	// Unlock and write sector address erase sequence
	FlashUnlock(chip);
	FLASH_WRITE_CMD(chip, FLASH_SEC_ERASE, i);
	// Write sector address 
	CifWrite(chip, sa, FLASH_SEC_ERASE_WR[0]);
	// Wait until erase starts (polling DQ3)
	while (!(CifRead(chip, sa) & 0x08));
	// Poll until erase complete
	return FlashErasePoll(chip, sa);
}

uint8_t FlashRangeErase(uint8_t chip, uint32_t addr, uint32_t len) {
	// Index
	uint8_t i, j;
	// Shifted address to compare with 
	uint16_t caddr = addr>>FLASH_SADDR_SHIFT;
	uint16_t clen = (len - 1)>>FLASH_SADDR_SHIFT;

	if (!len) return 0;
	if ((addr + len) > (FLASH_SADDR_MAX<<FLASH_SADDR_SHIFT)) {
        dprintf("[FLASH] Invalid erase range 0x%06lX:%06lX\n",
                addr, len);
        return 1;
    }

	// Find sector containing the initial address
	for (i = FLASH_NSECT - 1; caddr < saddr[i]; i--);
	// Find sector containing the end address
	for (j = FLASH_NSECT - 1; (caddr + clen) < saddr[j]; j--);

	for (; i <= j; i++) {
		if (!FlashSectErase(chip, ((uint32_t)(saddr[i]))<<FLASH_SADDR_SHIFT)) {
            dprintf("[FLASH] Erase failed\n");
			return 2;
		}
	}

	return 0;
}


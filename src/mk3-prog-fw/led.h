/************************************************************************//**
 * \file
 * \brief LED control module.
 *
 * \defgroup led led
 * \{
 * \brief LED control module.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#ifndef _LED_H_
#define _LED_H_

/// PORT register used for the LED
#define LED_REG		PORTE
/// DDR register used for the LED
#define LED_DDR		DDRE
/// Pin used for the led
#define LED_PIN		PE1	

/// Turns on LED
#define LedOn()		do{LED_REG &= ~(1<<LED_PIN);}while(0)
/// Turns off LED
#define LedOff()	do{LED_REG |= (1<<LED_PIN);}while(0)
/// Toggles LED status
#define LedToggle()	do{LED_REG ^= (1<<LED_PIN);}while(0)
/// Module initialization, must be called before anything else
#define LedInit()	do{LED_DDR |= (1<<LED_PIN);LedOff();}while(0)

#endif //_LED_H_

/** \} */


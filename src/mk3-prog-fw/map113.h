/************************************************************************//**
 * \file
 * \brief This module allows writing to cartridges implementing mapper 113
 *
 * \defgroup map113 map113
 * \{
 * \brief This module allows writing to cartridges implementing mapper 113
 *
 * \author Jesús Alonso (doragasu)
 * \date   2018
 ****************************************************************************/
#ifndef _MAP113_H_
#define _MAP113_H_

#include <stdint.h>

/// Address of the mapper register
#define MAP113_REG_ADDR 0x4100

/// Horizontal mirroring mask
#define MAP113_MIRROR_HORIZONTAL    0x00
/// Vertical mirroring mask
#define MAP113_MIRROR_VERTICAL      0x80


/************************************************************************//**
 * Module initialization. Call before using other module functions.
 ****************************************************************************/
void Map113Init(void);

/************************************************************************//**
 * Prepare access to mapper 113 ROM/Flash chip address.
 *
 * \param[in] chip ROM/Flash chip to access.
 * \param[in] addr Address of the chip to access.
 *
 * \return CPU/PPU address to use for access to the requested chip/address.
 ****************************************************************************/
uint16_t Map113PrepareAccess(uint8_t chip, uint32_t addr);

/************************************************************************//**
 * Write data to the mapper register.
 *
 * \param[in] data Data to write to specified register.
 ****************************************************************************/
void Map113MapperWrite(uint8_t data);

#endif /*_MAP113_H_*/
/** \} */



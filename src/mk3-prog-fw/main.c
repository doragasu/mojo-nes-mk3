/************************************************************************//**
 * \file
 * \brief mmc3-prog-fw: Firmware implementation for the AWESOME MOJO-NES
 * MKIII PROGRAMMER by doragasu.
 *
 * \defgroup main main
 * \{
 * \brief mmc3-prog-fw: Firmware implementation for the AWESOME MOJO-NES
 * MKIII PROGRAMMER by doragasu.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#include "board.h"
#include <avr/signature.h>
#include <string.h>
#include <util/delay.h>
#include <avr/io.h>
#include <stdio.h>

#include "main.h"
#include "bloader.h"

#include "flash.h"
#include "system.h"

#include "uart_stdio.h"

/// \brief Holds information about if the cartridge is or not inserted, and if
/// there has been a cartridge status change since last time.
typedef union {
	uint8_t all;				///< Access to all fields simultaneously
	struct {
		uint8_t statChange:1;	///< Status changed if TRUE
		uint8_t inserted:1;		///< Cartridge inserted if TRUE
	};
} CartStatus;

/** \} */
/// \brief Returns cartridge status.
CartStatus CheckCartStatus(void)
{
	// Status the last time this function was called.
	static uint8_t lastStatus;
	// Cartridge status
	CartStatus s;

	s.inserted = CifCartIn();
	if (s.inserted != lastStatus) {
		lastStatus = s.inserted;
		s.statChange = TRUE;
	} else {
		s.statChange = FALSE;
	}
	return s;
}


int main(void)
{
	uint8_t buttonStat, prevButtonStat;
    CartStatus cs;

	// Init LUFA related stuff
	SetupHardware();
	// If button pressed at startup, enter bootloader
	if (Buttons_GetStatus()) {
		JumpToBootloader();	
	}

	// Initialize system controller
	SysInit();

	dprintf("[INF] HW ver = %hd\n", CifVerGet());

	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
	prevButtonStat = Buttons_GetStatus();
	GlobalInterruptEnable();

	// Generate an initial cart in/out event
	cs = CheckCartStatus();
	if (cs.inserted) {
		SysFsmCycle(SYS_EVT_CIN);
	} else {
		SysFsmCycle(SYS_EVT_COUT);
	}

	for (;;) {
		USB_USBTask();
		// If button changed status, send event
		if ((buttonStat = Buttons_GetStatus()) != prevButtonStat) {
			SysFsmCycle(buttonStat?SYS_EVT_SW_PRESS:SYS_EVT_SW_REL);
			prevButtonStat = buttonStat;
		}


		// Check if there has been a change on cart status
		cs = CheckCartStatus();
		if (cs.statChange) SysFsmCycle(cs.inserted?
				SYS_EVT_CIN:SYS_EVT_COUT);
		// Check if we must generate a time event
		if (SysEvtTimerNotify()) SysFsmCycle(SYS_EVT_TIMER);

		// Check if there is input data from USB
		Endpoint_SelectEndpoint(VENDOR_OUT_EPADDR);
		if (Endpoint_IsOUTReceived()) {
			LEDs_TurnOnLEDs(LEDMASK_USB_BUSY);
			SysFsmCycle(SYS_EVT_DIN);
			LEDs_TurnOffLEDs(LEDMASK_USB_BUSY);
		}
	}
}

/** \brief Configures the board hardware and chip peripherals. */
void SetupHardware(void)
{
	/* Disable watchdog if enabled by bootloader/fuses */
	MCUSR &= ~(1 << WDRF);
	wdt_disable();

	/* Disable clock division */
	clock_prescale_set(clock_div_1);

	/* Hardware Initialization */
#ifdef _DEBUG
	uart_stdio_init();
#endif
	LEDs_Init();
	Buttons_Init();
	FlashInit();
	USB_Init();
}

/** \brief Event handler for the USB_Connect event. This indicates that the device is enumerating via the status LEDs. */
void EVENT_USB_Device_Connect(void)
{
	/* Indicate USB enumerating */
	LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** \brief Event handler for the USB_Disconnect event. This indicates that the device is no longer connected to a host via
 *  the status LEDs.
 */
void EVENT_USB_Device_Disconnect(void)
{
	/* Indicate USB not ready */
	LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
	SysFsmCycle(SYS_EVT_USB_DET);
}

/** \brief Event handler for the USB_ConfigurationChanged event. This is fired when the host set the current configuration
 *  of the USB device after enumeration - the device endpoints are configured.
 */
void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool configSuccess = true;

	/* Setup Vendor Data Endpoints */
	configSuccess &= Endpoint_ConfigureEndpoint(VENDOR_IN_EPADDR,  EP_TYPE_BULK, VENDOR_I_EPSIZE, 2);
	configSuccess &= Endpoint_ConfigureEndpoint(VENDOR_OUT_EPADDR, EP_TYPE_BULK, VENDOR_O_EPSIZE, 2);

	// Set LEDs and generate FSM events according to result
	if (configSuccess) {
		LEDs_SetAllLEDs(LEDMASK_USB_READY);
		SysFsmCycle(SYS_EVT_USB_ATT);
	} else {
		LEDs_SetAllLEDs(LEDMASK_USB_ERROR);
		SysFsmCycle(SYS_EVT_USB_ERR);
	}
}

/** Event handler for the USB_ControlRequest event. This is used to catch and process control requests sent to
 *  the device from the USB host before passing along unhandled control requests to the library for processing
 *  internally.
 */
void EVENT_USB_Device_ControlRequest(void)
{
	// Process vendor specific control requests here
}

/** \} */


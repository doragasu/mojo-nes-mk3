/************************************************************************//**
 * \file
 * \brief Hardware board related definitions.
 *
 * \defgroup board board
 * \{
 * \brief Hardware board related definitions.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#ifndef _BOARD_H_
#define _BOARD_H_

#ifndef F_CPU
/// CPU frequency
#define F_CPU			8000000LU
#endif

/// Set a bit from a register
#define SET_BIT(reg, bit)	do{(reg) |=  (1u << (bit));}while(0)
/// Clear a bit from a register
#define CLR_BIT(reg, bit)	do{(reg) &= ~(1u << (bit));}while(0)

#endif //_BOARD_H_

/** \} */


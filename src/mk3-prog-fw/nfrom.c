/************************************************************************//**
 * \file
 * \brief This module allows writing to NFROM registers, e.g. to set bank
 *        addressing.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2017
 ****************************************************************************/
#include "nfrom.h"
#include "cart_if.h"

/// Number of bits of each CHR bank
#define NFR_CHR_BITS	13
/// Number of bits of each PRG bank
#define NFR_PRG_BITS	15

/// Address mask for CHR banks
#define NFR_CHR_ADDR_MASK	((1U<<NFR_CHR_BITS) - 1)
/// Address mask for PRG banks
#define NFR_PRG_ADDR_MASK	((1U<<NFR_PRG_BITS) - 1)

/** \addtogroup tkrom
 *  \brief CPU addresses of the memory mapped registers.
 *  \note Bit 15 is always one in these registers, and has been ommited on
 *        purpose, as write routine will lower #PRG pin.
 *  \{ */
/// CHR bank select register, low byte ($8000)
#define NFR_REG_CHR_LOW_ADDR		0x0000
/// CHR bank select register, high bits ($8001)
#define NFR_REG_CHR_HIGH_ADDR		0x0001
/// PRG bank select register ($A000)
#define NFR_REG_PRG_ADDR			0x2000
/// Mirroring and RAM protection register ($A001)
#define NFR_REG_MIRR_RAM_PROT_ADDR	0x2001
/// IRQ latch register ($C000)
#define NFR_REG_IRQ_LATCH_ADDR		0x4000
/// IRQ reload ($C001)
#define NFR_REG_IRQ_REL_ADDR		0x4001
/// IRQ disable ($E000)
#define NFR_REG_IRQ_DIS_ADDR		0x6000
/// IRQ enable ($E001)
#define NFR_REG_IRQ_ENA_ADDR		0x6001
/** \} */

/// Number of TKR related registers
#define NFR_REG_NUM_REGS			8

/// Register CPU memory addresses array
const uint16_t tkrRegAddr[NFR_REG_NUM_REGS] = {
	NFR_REG_CHR_LOW_ADDR,   NFR_REG_CHR_HIGH_ADDR,
	NFR_REG_PRG_ADDR,       NFR_REG_MIRR_RAM_PROT_ADDR,
	NFR_REG_IRQ_LATCH_ADDR, NFR_REG_IRQ_REL_ADDR,
	NFR_REG_IRQ_DIS_ADDR,   NFR_REG_IRQ_ENA_ADDR
};

/// Number of shadow registers
#define NFR_SHADOW_REGS		4

/// Shadow of the mapper registers
static uint8_t tkrShadow[NFR_SHADOW_REGS];

static void NfrWrite(NfrReg reg, uint16_t data);

void NfrInit(void) {
	int8_t i;

	// Enable RAM and RAM writes
	NfrWrite(NFR_REG_MIRR_RAM_PROT, NFR_REG_MIRR_RAM_PROT_CHIP_ENABLE |
            NFR_REG_MIRR_RAM_PROT_ALLOW_WRITE);
	tkrShadow[3] = NFR_REG_MIRR_RAM_PROT_CHIP_ENABLE |
        NFR_REG_MIRR_RAM_PROT_ALLOW_WRITE;
	// Set banks to 0
	for (i = 2; i >= 0; i--) {
		NfrWrite(i, 0);
		tkrShadow[i] = 0;
	}
}

/************************************************************************//**
 * Write data to specified mapper register. This function differs with
 * NfrMapperWrite in the fact that it does not check the shadow registers.
 *
 * \param[in] reg  Register number to write to.
 * \param[in] data Data to write to specified register.
 ****************************************************************************/
static void NfrWrite(NfrReg reg, uint16_t data) {
	uint16_t addr = tkrRegAddr[reg];

	CIF_ADDRL_PORT = addr;
	CIF_ADDRH_PORT = (CIF_ADDRH_PORT & (~CIF_CPU_ADDRH_MASK)) |
					 (addr>>8);
	// Write data to bus
	CIF_DATA_PORT = data;
	CIF_DATA_DDR = 0xFF;
	// Chip is always selected, signal _W and generate clock
	CIF_PIN_SET(CPU_PHI2);
	CIF_PIN_CLR(CPU__PRG);
	CIF_PIN_CLR(CPU__W);
	
	// MMC3X write will be latched on clock falling edge
	CIF_PIN_CLR(CPU_PHI2);
	CIF_PIN_SET(CPU__W);
	CIF_PIN_SET(CPU__PRG);
	// Remove data from bus
	CIF_DATA_DDR  = 0;
	CIF_DATA_PORT = 0xFF;
}

void NfrMapperWrite(NfrReg reg, uint8_t data) {
	// If register has shadow copy, check if value changed, and update
	// if not changed. Else perform register write directly.
	if (reg < NFR_SHADOW_REGS) {
		if (tkrShadow[reg] != data) {
			NfrWrite(reg, data);
			tkrShadow[reg] = data;
		}
	} else NfrWrite(reg, data);
}

uint16_t NfrPrepareAccess(uint8_t chip, uint32_t addr) {
	uint16_t cpuAddr;
	uint16_t bank;

	if (CIF_CHIP_CHR == chip) {
		cpuAddr = addr & NFR_CHR_ADDR_MASK;
		bank = addr>>NFR_CHR_BITS;
		NfrMapperWrite(NFR_REG_CHR_LOW,  bank);
		NfrMapperWrite(NFR_REG_CHR_HIGH, bank>>8);
	} else {
		cpuAddr = addr & NFR_PRG_ADDR_MASK;
		bank = addr>>NFR_PRG_BITS;
		NfrMapperWrite(NFR_REG_PRG, bank);
	}

	return cpuAddr;
}


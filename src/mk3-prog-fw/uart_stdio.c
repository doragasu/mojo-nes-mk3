#include "board.h"
#include <avr/io.h>
#include <stdio.h>
#include "uart_stdio.h"

#define UART_UBRR  (((F_CPU / (UART_STDIO_BAUD_RATE * 16UL))) - 1)

static int uart_putchar(char c, FILE *stream)
{
    if (c == '\n') {
        uart_putchar('\r', stream);
    }

    loop_until_bit_is_set(UCSR1A, UDRE1);
    UDR1 = c;

    return 0;
}

static FILE uart_stdio = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_RW);

void uart_stdio_init(void)
{
    // Set clock divisors
    UBRR1H = UART_UBRR>>8;
    UBRR1L = UART_UBRR;

    // Asyncrhonous operation, 8 bit, no parity, 1 bit stop
    UCSR1C = (1<<UCSZ11) | (1<<UCSZ10);
    // Enable receiver and transmitter
    UCSR1B = (1<<RXEN1)  | (1<<TXEN1);

    // Set stdout pointing to our putchar function
    stdout = &uart_stdio;
}

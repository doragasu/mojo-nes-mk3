/************************************************************************//**
 * \file
 * \brief This module allows writing to MMC3X registers, e.g. to set bank
 *        addressing.
 *
 * \defgroup mmc3x mmc3x
 * \{
 * \brief This module allows writing to MMC3X registers, e.g. to set bank
 *        addressing.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2016
 ****************************************************************************/
#ifndef _MMC3X_H_
#define _MMC3X_H_

#include "cart_if.h"

///Definitions of MMC3X memory mapped registers.
/// Tip: build with gcc's '-fshort-enums' for it to use a single byte.
typedef enum {
	MMC3X_REG_BANK_SEL = 0,	///< Bank select
	MMC3X_REG_BANK_DATA,	///< Bank data
	MMC3X_REG_MIRRORING,	///< Mirroring control
	MMC3X_REG_RAM_PROT,		///< PRG RAM protect
	MMC3X_REG_IRQ_LATCH,	///< IRQ latch
	MMC3X_REG_IRQ_REL,		///< IRQ reload
	MMC3X_REG_IRQ_DIS,		///< IRQ disable
	MMC3X_REG_IRQ_ENA,		///< IRQ enable
	MMC3X_REG_NUM_REGS		///< Number of memory mapped registers
} Mmc3xReg;


/** \addtogroup Mmc3xMirrorModes
 *  \brief Allowed mirroring modes.
 *  \{ */
/// Vertical mirroring
#define MMC3X_REG_MIRRORING_VERTICAL	0
/// Horizontal mirroring
#define MMC3X_REG_MIRRORING_HORIZONTAL	1
/** \} */

/** \addtogroup Mmc3xSramConfig
 *  \brief Allowed SRAM configuration values.
 *  \{ */
/// Enable RAM chip
#define MMC3X_REG_RAM_PROT_CHIP_ENABLE		0x80
/// Disable RAM chip
#define MMC3X_REG_RAM_PROT_CHIP_DISABLE		0x00
/// Allow writes to RAM chip
#define MMC3X_REG_RAM_PROT_ALLOW_WRITE		0x00
/// Prohibit writes to RAM chip
#define MMC3X_REG_RAM_PROT_DENY_WRITE		0x40
/** \} */

/// Register CPU memory addresses array
extern const uint16_t mmc3xReg[MMC3X_REG_NUM_REGS];

/** \addtogroup Mmc3xBankSwCfg
 *  \brief MMC3 bankswitching options used when writing
 *   to Bank Select register, with macro MMC3X_REG_BANK_SEL_RMK().
 *  \{ */
/// Lower banks can be swapped (default)
#define MMC3X_ROM_MODE_LOW_SWAPPABLE	0
/// Lower banks are fixed
#define MMC3X_ROM_MODE_LOW_FIXED		1
/// A12 bit inversion disabled (default)
#define MMC3X_A12INV_OFF				0
/// A12 bit inversion enabled
#define MMC3X_A12INV_ON					1
/** \} */

/************************************************************************//**
 * Module initialization. Call before using Mmc3xBankConf() function.
 ****************************************************************************/
void Mmc3xInit(void);

/************************************************************************//**
 * Prepare access to MMC3X ROM/Flash chip address.
 *
 * \param[in] chip Chip to access (MMC3X_CHIP_PRG/MMC3X_CHIP_CHR).
 * \param[in] addr Address of the chip to access.
 *
 * \return CPU/PPU address to use for access to the requested chip/address.
 ****************************************************************************/
uint16_t Mmc3xPrepareAccess(uint8_t chip, uint32_t addr);

/************************************************************************//**
 * Configures a bank address register, to allow accessing the specified
 * address.
 *
 * \param[in] bankReg (0~7) to set.
 * \param[in] addr Address that will be accessed using bankReg register.
 ****************************************************************************/
void Mmc3xBankConf(uint8_t bankReg, uint32_t addr);

#endif /*_MMC3X_H_*/
/** \} */


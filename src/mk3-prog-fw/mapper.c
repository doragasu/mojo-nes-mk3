/************************************************************************//**
 * \file
 * \brief Handles mapper chips. This module acts as a wrapper to the
 *        supported mapper drivers.
 *
 * \author Jesús Alonso (doragasu)
 * \date   2017
 ****************************************************************************/
#include "mapper.h"
#include "cnrom.h"
#include "mmc3x.h"
#include "nfrom.h"
#include "map113.h"

#define MAP_EXPAND_AS_NUMBER(u_name, l_name, number)    number,

uint16_t (*MapPrepareAccess)(uint8_t chip, uint32_t addr);

const uint16_t map_number[MAP_MAX] = {
    MAP_TABLE(MAP_EXPAND_AS_NUMBER)
};

/// No mapper. Just return the input address.
uint16_t MapPrepNull(uint8_t chip, uint32_t addr) {
    (void)chip;
    return addr;
}

int MapInit(uint16_t number) {
    int i;
    // Initialize the cart interface buses
    //	CifInit(); Done in the flash module

    for (i = 0; (i < MAP_MAX) && (map_number[i] != number); i++);
    if (MAP_MAX == i) {
        // Unsupported mapper
        return -1;
    }

    switch (i) {
        default:
            // If not supported mapper requested, silently use NOROM
        case MAP_NOROM:
            MapPrepareAccess = MapPrepNull;
            break;

        case MAP_CNROM:
            CnromInit();
            MapPrepareAccess = CnromPrepareAccess;
            break;

        case MAP_MMC3:
            // Initialize MMC3X driver
            Mmc3xInit();
            MapPrepareAccess = Mmc3xPrepareAccess;
            break;

        case MAP_113:
            Map113Init();
            MapPrepareAccess = Map113PrepareAccess;
            break;

        case MAP_NFROM:
            NfrInit();
            MapPrepareAccess = NfrPrepareAccess;
            break;
    }

    return 0;
}

/************************************************************************//**
 * Write one byte to a synchronous element connected to the bus.
 *
 * \param[in] addr Address to write the byte to.
 * \param[in] data Data byte to write.
 ****************************************************************************/
static inline void MapSyncWrite(uint16_t addr, uint8_t data) {
    CIF_ADDRL_PORT = addr;
    CIF_ADDRH_PORT = (CIF_ADDRH_PORT & (~CIF_CPU_ADDRH_MASK)) |
        ((addr>>8) & CIF_CPU_ADDRH_MASK);
    // Write data to bus
    CIF_DATA_PORT = data;
    CIF_DATA_DDR = 0xFF;

    // Depending on A15, set or clear #PRG
    if (addr & 0x8000) CIF_PIN_CLR(CPU__PRG);
    else CIF_PIN_SET(CPU__PRG);
    CIF_PIN_CLR(CPU__W);
    CIF_PIN_SET(CPU_PHI2);
    // Write is done now

    // Remove data from port
    CIF_PIN_CLR(CPU_PHI2);
    CIF_PIN_SET(CPU__W);
    CIF_DATA_DDR = 0;
    CIF_DATA_PORT = 0xFF;
}

/************************************************************************//**
 * Read one byte from a synchronous element connected to the bus.
 *
 * \param[in] addr Address to read from the bus.
 *
 * \return The readed byte.
 ****************************************************************************/
static inline uint8_t MapSyncRead(uint16_t addr) {
    CIF_ADDRL_PORT = addr;
    CIF_ADDRH_PORT = (CIF_ADDRH_PORT & (~CIF_CPU_ADDRH_MASK)) |
        ((addr>>8) & CIF_CPU_ADDRH_MASK);

    // Depending on A15, set or clear #PRG
    if (addr & 0x8000) CIF_PIN_CLR(CPU__PRG);
    else CIF_PIN_SET(CPU__PRG);
    CIF_PIN_SET(CPU_PHI2);
    // Read is done now
    _NOP();	// Ensure data traverses input synchronizer
    CIF_PIN_CLR(CPU_PHI2);
    return CIF_DATA_PIN;
}

int MapRamWrite(uint16_t offset, uint16_t len, uint8_t data[]) {
    uint16_t i;

    // Check block fits in the available RAM
    if (offset + len > MAP_RAM_LEN) return -1;

    for (i = 0, offset = offset + MAP_RAM_BASE; i < len; i++, offset++) {
        MapSyncWrite(offset, data[i]);
    }
    return 0;
}

int MapRamRead(uint16_t offset, uint16_t len, uint8_t data[]) {
    uint16_t i;

    // Check block fits in the available RAM
    if (offset + len > MAP_RAM_LEN) return -1;

    for (i = 0, offset = offset + MAP_RAM_BASE; i < len; i++, offset++) {
        data[i] = MapSyncRead(offset);
    }

    return 0;
}

